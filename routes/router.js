const auth = require('../middlewares/auth')
const multipart = require('../middlewares/multipart')
const common = require('../controllers/common')
const home = require('../controllers/home')
const people = require('../controllers/people')
const company = require('../controllers/company')
const trainingProvider = require('../controllers/training-provider.js')
const backoffice = require('../controllers/backoffice')

const router = app => {
  // Health check
  app.get('/', async (request, response) => response.sendStatus(200))

  // Landing, login, register, verify
  app.get('/home/landing', home.landing)
  app.get('/home/landing_courses_jobs', home.landing_courses_jobs)
  app.post('/home/login', home.login)
  app.post('/home/register', home.register)
  app.post('/home/resend_mail', home.sendVerifyAccountEmail)
  app.post('/home/google-login', home.googleLogin)
  app.get('/home/verify-user/:token', home.verify)
  app.post('/home/recover-password-email', home.recoverPasswordEmail)
  app.post('/home/recover-password/:token', home.recoverPassword)
  app.get('/home/set-role/:role', auth.jwtCheck, home.setRole)

  // Common
  app.post('/common/search-skills-by-name', common.searchSkillsByName)
  app.post('/common/search-competences-by-name', common.searchCompetencesByName)
  app.post('/common/search-experiences-by-name', common.searchExperiencesByName)
  app.post('/common/search-challenges-by-name', common.searchChallengesByName)
  app.post('/common/search-courses-by-name', common.searchCoursesByName)

  // resources
  app.post('/common/add-resource',  multipart.upload.single('file'), auth.jwtCheck,common.addExternalResource)
  app.post('/common/get-resource',multipart.upload.single(), auth.jwtCheck, common.getExternalResource)
  app.post('/common/delete-resource', multipart.upload.single(),auth.jwtCheck, common.deleteExternalResource)

  app.get('/images/:folder/:image', common.getImage)
  app.get('/cvs/:cv', common.getCv)
  app.get('/common/countries', common.getCountries)
  app.get('/common/search-courses', auth.jwtCheck, common.getSearchCourses)
  app.post('/common/search-courses', auth.jwtCheck, common.searchCourses)
  app.post('/common/delete-fav-courses', auth.jwtCheck, common.deleteFavCourses)
  app.post('/common/add-fav-courses', auth.jwtCheck, common.addFavCourses)
  app.get('/common/get-rate-courses', auth.jwtCheck, common.getRateCourses)
  app.post('/common/rate-course', auth.jwtCheck, common.rateCourse)

  app.get('/common/courses/:course_id', auth.jwtCheck, common.getCourse)
  app.get('/visitor/courses/:course_id', common.getCourseNoUID)

  app.post('/common/delete-fav-jobs', auth.jwtCheck, common.deleteFavJobs)
  app.post('/common/add-fav-jobs', auth.jwtCheck, common.addFavJobs)
  app.put('/common/update-password', auth.jwtCheck, common.updatePassword)
  app.delete('/common/delete-account', auth.jwtCheck, common.deleteAccount)
  app.post('/common/upload-profile-image', multipart.upload.single('image'), auth.jwtCheck, common.uploadProfileImage)
  app.get('/common/search-jobs', auth.jwtCheck, common.getSearchJobs)
  app.post('/common/search-jobs',auth.jwtCheck,  common.getJobsByOptions)

  app.get('/common/jobs/:job_id',  common.getJobById)

  app.post('/common/search-candidates', auth.jwtCheck, common.searchCandidates)
  app.post('/common/addCandidates', auth.jwtCheck, company.postFavCandidate)

  // People
  app.get('/people/user-profile/', auth.jwtCheck, people.getUserProfile)
  app.put('/people/user-profile/personal-information', auth.jwtCheck, people.updatePersonalInformation)
  app.put('/people/user-profile/user-skills', auth.jwtCheck, people.updateUserSkills)
  app.put('/people/user-profile/user-competences', auth.jwtCheck, people.updateUserCompetences)
  app.put('/people/user-profile/user-experiences', auth.jwtCheck, people.updateUserExperiences)
  app.put('/people/user-profile/user-challenges', auth.jwtCheck, people.updateUserChallenges)
  app.put('/people/user-profile/user-educations', auth.jwtCheck, people.updateUserEducations)
  app.get('/people/learning-paths', auth.jwtCheck, people.getLearningPaths)
  app.post('/people/upload-cv', multipart.upload.single('cv'), auth.jwtCheck, people.uploadCv)
  app.delete('/people/delete-cv', auth.jwtCheck, people.deleteCv)
  app.get('/people/home', auth.jwtCheck, people.getHome)

  // Company
  app.get('/company/user-profile', auth.jwtCheck, company.getUserProfile)
  app.put('/company/user-profile/personal-information', auth.jwtCheck, company.updatePersonalInformation)
  app.get('/company/offer-jobs', auth.jwtCheck, company.getOfferJobs)
  app.put('/company/update-jobs', auth.jwtCheck, company.updateJobs)
  app.put('/company/update-job', auth.jwtCheck, company.updateJob)
  app.delete('/company/delete-job', auth.jwtCheck, company.deleteJob)
  app.post('/company/upload-job-image', multipart.upload.single('image'), auth.jwtCheck, company.uploadJobImage)
  app.get('/company/home', auth.jwtCheck, company.getHome)
  app.post('/company/addCandidates', auth.jwtCheck, company.postFavCandidate)
  app.post('/company/deleteCandidates', auth.jwtCheck, company.deleteFavCandidate)
  app.get('/company/candidates', auth.jwtCheck, company.getCandidates)

  // Training provider
  app.get('/training-provider/user-profile', auth.jwtCheck, trainingProvider.getUserProfile)
  app.put('/training-provider/user-profile/personal-information', auth.jwtCheck, trainingProvider.updatePersonalInformation)
  app.get('/training-provider/demanded-skills', auth.jwtCheck, trainingProvider.getDemandedSkills)
  app.get('/training-provider/validate-courses', auth.jwtCheck, trainingProvider.getValidateCourses)
  app.put('/training-provider/accept-rate', auth.jwtCheck, trainingProvider.acceptRate)
  app.put('/training-provider/reject-rate', auth.jwtCheck, trainingProvider.rejectRate)
  app.get('/training-provider/publish-training-courses', auth.jwtCheck, trainingProvider.getPublishTrainingCourses)
  app.post('/training-provider/upload-course-image', multipart.upload.single('image'), auth.jwtCheck, trainingProvider.uploadCourseImage)
  app.put('/training-provider/update-courses', auth.jwtCheck, trainingProvider.updateCourses)
  app.delete('/training-provider/delete-courses/:course_id', auth.jwtCheck, trainingProvider.deleteCourses)
  app.get('/training-provider/home', auth.jwtCheck, trainingProvider.getHome)
  app.post('/training-provider/addCandidates', auth.jwtCheck, company.postFavCandidate)
  app.post('/training-provider/deleteCandidates', auth.jwtCheck, company.deleteFavCandidate)
  app.get('/training-provider/candidates', auth.jwtCheck, company.getCandidates)

  // Backoffice
  app.get('/backoffice/back-home', auth.jwtCheck, backoffice.backHome)
  app.post('/backoffice/update-user', auth.jwtCheck, backoffice.updatePersonalInformation)
  app.post('/backoffice/delete-user', auth.jwtCheck, backoffice.deleteUser)
  app.get('/backoffice/jobs/', auth.jwtCheck, backoffice.getAllJobs)
  app.get('/backoffice/users/', auth.jwtCheck, backoffice.getAllUsers)
  app.get('/backoffice/courses/', auth.jwtCheck, backoffice.getAllCourses)
  app.post('/backoffice/delete-courses', auth.jwtCheck, backoffice.deleteCourse)
  app.post('/backoffice/delete-jobs', auth.jwtCheck, backoffice.deleteJob)
  app.post('/backoffice/update-job', auth.jwtCheck, backoffice.updateJobInformation)
  app.post('/backoffice/update-course', auth.jwtCheck, backoffice.updateCourseInformation)
  app.post('/backoffice/get-course-ratings', auth.jwtCheck, backoffice.getCourseRatings)
  app.post('/backoffice/remove-course-rating', auth.jwtCheck, backoffice.removeCourseRating)
  app.post('/backoffice/update-rate', auth.jwtCheck, backoffice.updateRate)

}

module.exports = router
