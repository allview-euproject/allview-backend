const db = require('../../db')

const getUserProfile = async (userId) => {
  const data = {}

  // User personal information
  // const personalInformation = await db.query('SELECT u.user_id, u.email, u.full_name, u.description, u.company_identification, u.street, ' +
  //   'u.city, u.state, u.country, u.website, u.phone, u.contact_person, u.contact_mail, u.sector_id, u.sector_name, o.occupation_name, u.image FROM users u ' +
  //   'LEFT JOIN occupations o ON u.sector_id = o.occupation_id WHERE u.user_id = ?', userId)
  const personalInformation = await db.query('SELECT u.user_id, u.email, u.full_name, u.description, u.company_identification, u.street, ' +
     'u.city, u.state, u.country, u.website, u.phone, u.contact_person, u.contact_mail, u.sector_id, u.sector_name, u.image FROM users u ' +
     'WHERE u.user_id = ?', userId)
  data.personalInformation = personalInformation[0]

  // Sectors are occupations level 2
  const sectors = await db.query('SELECT occupation_name FROM occupations WHERE occupation_level = 2')
  data.sectors = []
  for (const sector of sectors) {
    data.sectors.push(sector.occupation_name)
  }

  const user_sectors = await db.query('SELECT DISTINCT occupation_name FROM users_occupations WHERE user_id = ' + userId)
  data.user_sectors = []
  for (const sector of user_sectors) {
    data.user_sectors.push(sector.occupation_name)
  }

  return data
}

const updatePersonalInformation = async (userId, fullName, city, state, country, phone, description, companyIdentification, street, contactPerson, contactMail, website, sector) => {
  if (fullName) {
    try {
      await db.query('UPDATE users SET full_name = ? WHERE user_id = ?', [fullName, userId])
    } catch (error) {
      throw new Error('Error updating full name: ' + error.message)
    }
  }
  if (city) {
    try {
      await db.query('UPDATE users SET city = ? WHERE user_id = ?', [city, userId])
    } catch (error) {
      throw new Error('Error updating city: ' + error.message)
    }
  }
  if (state) {
    try {
      await db.query('UPDATE users SET state = ? WHERE user_id = ?', [state, userId])
    } catch (error) {
      throw new Error('Error updating state: ' + error.message)
    }
  }
  if (country) {
    try {
      await db.query('UPDATE users SET country = ? WHERE user_id = ?', [country, userId])
    } catch (error) {
      throw new Error('Error updating country: ' + error.message)
    }
  }
  if (phone) {
    // A phone number should have 12 digits max
    if (phone.match(/\d/g).length > 12) throw new Error('Error updating phone: the number is too long')
    // A phone number should have 8 digits min
    if (phone.match(/\d/g).length < 8) throw new Error('Error updating phone: the number is too short')
    try {
      await db.query('UPDATE users SET phone = ? WHERE user_id = ?', [phone, userId])
    } catch (error) {
      throw new Error('Error updating phone: ' + error.message)
    }
  }
  if (description) {
    try {
      await db.query('UPDATE users SET description = ? WHERE user_id = ?', [description, userId])
    } catch (error) {
      throw new Error('Error updating description: ' + error.message)
    }
  }
  if (companyIdentification) {
    try {
      await db.query('UPDATE users SET company_identification = ? WHERE user_id = ?', [companyIdentification, userId])
    } catch (error) {
      throw new Error('Error updating company identification: ' + error.message)
    }
  }
  if (street) {
    try {
      await db.query('UPDATE users SET street = ? WHERE user_id = ?', [street, userId])
    } catch (error) {
      throw new Error('Error updating street: ' + error.message)
    }
  }
  if (contactPerson) {
    try {
      await db.query('UPDATE users SET contact_person = ? WHERE user_id = ?', [contactPerson, userId])
    } catch (error) {
      throw new Error('Error updating contact person: ' + error.message)
    }
  }
  if (contactMail) {
    try {
      await db.query('UPDATE users SET contact_mail = ? WHERE user_id = ?', [contactMail, userId])
    } catch (error) {
      throw new Error('Error updating contact person: ' + error.message)
    }
  }
  if (website) {
    try {
      await db.query('UPDATE users SET website = ? WHERE user_id = ?', [website, userId])
    } catch (error) {
      throw new Error('Error updating website: ' + error.message)
    }
  }

  if (sector) {
    console.log("EL SECTOR ES: " + sector)
    await db.query('DELETE FROM users_occupations WHERE user_id = ?', [userId])
    try {
      sector.forEach(async (sec) => {
        await db.query('INSERT INTO users_occupations (occupation_name, user_id) VALUES (?, ?)', [sec, userId]) 
        console.log('INSERT INTO users_occupations (occupation_name, user_id) VALUES (?, ?)', [sec, userId])
      })
    } catch (error) {
      throw new Error('Error updating sector: ' + error.message)
    }
  }
}

module.exports = {
  getUserProfile,
  updatePersonalInformation
}
