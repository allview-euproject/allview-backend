const db = require('../../db')
const utils = require('../../utils')
const fs = require('fs')
const path = require('path')

const getOfferJobs = async (userId) => {
  const data = {}

  const sectors = await db.query('SELECT occupation_name FROM occupations WHERE occupation_level = 2')
  data.sectors = []
  for (const sector of sectors) {
    data.sectors.push(sector.occupation_name)
  }

  const languages = await db.query('SELECT language FROM jobs_languages')
  data.languages = []
  for (const language of languages) {
    data.languages.push(language.language)
  }

  const contracts = await db.query('SELECT contract FROM jobs_contracts')
  data.contracts = []
  for (const contract of contracts) {
    data.contracts.push(contract.contract)
  }

  const jobs = await db.query('SELECT * FROM jobs WHERE deleted_at IS NULL AND company_id = ?', userId)

  data.jobs = []
  for (const job of jobs) {
    job.experiencesSelected = ""
    job.experiencesSelected = job.job_requirements

    if(!job.languages && job.language_id){
      job.language = (await db.query('SELECT language FROM jobs_languages WHERE job_language_id = ?', job.language_id))[0].language
      if(!job.languages){
        job.languages = JSON.stringify([job.language])
      }
    }
    if(job.contract_id) {
      job.contract = (await db.query('SELECT contract FROM jobs_contracts WHERE job_contract_id = ?', job.contract_id))[0].contract
    }
      //const sectors = await db.query('SELECT occupation_name FROM jobs_occupations WHERE job_id = ?', job.job_id)
    const sectors = await db.query('SELECT IFNULL(t2.occupation_name, t1.occupation_name) AS occupation_name\n' +
        'FROM jobs_occupations AS t1\n' +
        'LEFT JOIN occupations AS t2 ON t1.occupation_id = t2.occupation_id \n' +
        'WHERE t1.job_id = ?', job.job_id);

    job.sector = []
    for (const sector of sectors) {
      job.sector.push(sector.occupation_name)
    }

    const essentialSkillsSearch = await db.query('SELECT skill_name FROM jobs_skills WHERE job_id = ? AND essential = 1', job.job_id)
    job.essentialSkillsSelected = []
    for (const essentialSkill of essentialSkillsSearch) {
      job.essentialSkillsSelected.push(essentialSkill.skill_name)
    }
    job.essentialSkillsSelectedAutocomplete = []

    const optionalSkillsSearch = await db.query('SELECT skill_name FROM jobs_skills WHERE job_id = ? AND essential = 0', job.job_id)
    job.optionalSkillsSelected = []
    for (const optionalSkill of optionalSkillsSearch) {
      job.optionalSkillsSelected.push(optionalSkill.skill_name)
    }
    job.optionalSkillsSelectedAutocomplete = []

    //const experiencesSSearch = await db.query('SELECT occupation_name FROM jobs_occupations WHERE job_id = ?', job.job_id)
    // job.experiencesSelected = []
    // for (const experience of experiencesSSearch) {
    //   job.experiencesSelected.push(experience.occupation_name)
    // }
    
    job.experiencesSelectedAutocomplete = []

    data.jobs.push(job)
  }

  return data
}

// TODO: This function works but it probably requires a full rewrite
// It is complex since it can create, update and delete courses in the same call
// The training provider methods has separated functions for deleting and updating courses,
// making the code longer but much easier to read
const updateJobs = async (userId, jobs) => {
  // This array contains the jobs id that the user wants to update. If there are more jobs saved, it means that some have been deleted
  const savedJobs = []
  // Iterate every job sent from the frontend and update it
  for (const job of jobs) {
    //console.log("EL JOB ES: " + JSON.stringify(job))
    // It makes no sense to set the job duration value for an undefined job contract
    //if (job.contract === 'Undefined') job.duration = null //COMO LA DURACIÓN YA NO ES OBLIGATORIA MODIFICAMOS ESTO.
    // Check if all the required fields are present
    //if (!job.name || job.salary === undefined || !job.contact_details || !job.language || !job.sector || !job.contract || !job.contract ) {
    if (!job.name || !job.contact_details || !job.language || !job.sector || !job.contract ) {
      throw new Error('Missing required fields (name, contact_details, language, sector, type of contract)')
    }

    // Check if the skills are included
    // if ((!job.essentialSkillsSelectedAutocomplete || job.essentialSkillsSelectedAutocomplete.length === 0) && (!job.essentialSkillsSelected || job.essentialSkillsSelected.length === 0)) {
    //   throw new Error('Missing required essential skills')
    // }

    // Check if the skills are included
    if ((!job.essentialSkillsSelected || job.essentialSkillsSelected.length === 0)) {
      throw new Error('Missing required essential skills')
    }

    // Check if the salary is a number
    // if (!utils.isNumeric(job.salary)) {
    //   throw new Error('The salary is not a valid number')
    // }
    if(job.image && job.image.indexOf("http") !== -1){
      job.image = job.image.substring(job.image.lastIndexOf("/")+1);
    }
    let jobId = job.job_id
    let image = job.image
    // console.log("EL VALOR DE LA IMAGEN ES: " + image)
    // If the job has just been created, it has no id
    if (jobId) {
      // Deleted the saved job data
      await db.query('DELETE FROM jobs_occupations WHERE job_id = ?', jobId)
      await db.query('DELETE FROM jobs_skills WHERE job_id = ?', jobId)
      // Get image for later use, it does not came in the job object so we treat it separately
      const imageSearch = (await db.query('SELECT image FROM jobs WHERE job_id = ?', jobId))[0]
      if (imageSearch && image == null){
        image = imageSearch.image
        // console.log("EL VALOR DE LA IMAGEN2 ES: " + image)
      } 

      // Delete saved job
      //await db.query('DELETE FROM jobs WHERE job_id = ?', jobId)
      // Insert the job with the old id
      //await db.query('INSERT INTO jobs (job_id, name, contact_details, company_id, contract_id, language_id) VALUES(' + jobId + ',"' + job.name + '","' + job.contact_details + '",' + job.company_id + ',(SELECT job_contract_id FROM jobs_contracts WHERE contract="' + job.contract + '"),(SELECT job_language_id FROM jobs_languages WHERE language ="' + job.language + '"))')
      await await db.query('UPDATE jobs SET manual_publish = ? , publish_ini = ?, publish_end = ?, name = ?,contact_details = ?, company_id = ?, contract_id = (SELECT job_contract_id FROM jobs_contracts WHERE contract= ?), language_id = (SELECT job_language_id FROM jobs_languages WHERE language = ?) WHERE jom_id = ?', [job.manual_publish,job.publish_ini,job.publish_end,job.name, job.contact_details,job.company_id, job.contract,job.language,jobId])

    } else {
      // Insert the job with a new id
      //console.log('INSERT INTO jobs (name, contact_details, company_id, contract_id, language_id, sector_id) VALUES("' + job.name + '", "' + job.contact_details + '",' + userId + ',(SELECT job_contract_id FROM jobs_contracts WHERE contract="' + job.contract + '"),(SELECT job_language_id FROM jobs_languages WHERE language ="' + job.language + '"),(SELECT occupation_id FROM occupations WHERE occupation_name="' + job.sector + '"))')
      //const response = await db.query('INSERT INTO jobs (name, contact_details, company_id, contract_id, language_id) VALUES("' + job.name + '", "' + job.contact_details + '",' + userId + ',(SELECT job_contract_id FROM jobs_contracts WHERE contract="' + job.contract + '"),(SELECT job_language_id FROM jobs_languages WHERE language ="' + job.language + '"))')
      const response = await db.query('INSERT INTO jobs (manual_publish,publish_ini,publish_end,name, contact_details, company_id, contract_id, language_id) VALUES(?,?,?,(SELECT job_contract_id FROM jobs_contracts WHERE contract= ? ),(SELECT job_language_id FROM jobs_languages WHERE language = ?))',[job.manual_publish,job.publish_ini,job.publish_end,job.name,job.contact_details,userId,job.contract,job.language])
      // Save the new id
      jobId = response.insertId
    }

    if (job.description != null) {
      await db.query('UPDATE jobs SET description= ? WHERE job_id= ?',[job.description, jobId])
    }


    if (job.receive_candidates_recommendations != null) {
      await db.query('UPDATE jobs SET receive_candidates_recommendations = ? WHERE job_id = ?', [job.receive_candidates_recommendations, jobId]);
    }

    if (job.salary){
      await db.query('UPDATE jobs SET salary= ? WHERE job_id= ?',[job.salary, jobId]);
    }


    //console.log("LA DURACION ES: " + job.duration)
    if (job.duration){
      //console.log('UPDATE jobs SET duration=' + job.duration + ' WHERE job_id=' + jobId)
      await db.query('UPDATE jobs SET duration= ? WHERE job_id= ? ',[job.description, jobId])
    }

    // These lines inserts the new skills/experiences. This data comes from the xxxxSelected and xxxxSelectedAutocomplete arrays
    // First, the arrays are concatenated into a single array and the duplicated are removed
    // Then, each skill is inserted into the db
    // Both arrays may come empty so thats why there are a check and a default value

    // if (job.essentialSkillsSelectedAutocomplete != null) {
    //   //if (!job.essentialSkillsSelected) job.essentialSkillsSelected = []
    //   //const essentialSkills = job.essentialSkillsSelectedAutocomplete.concat(job.essentialSkillsSelected.filter((item) => job.essentialSkillsSelectedAutocomplete.indexOf(item) < 0))
    //   for (const skill of job.essentialSkillsSelectedAutocomplete) {
    //     await db.query('INSERT INTO jobs_skills (job_id, essential, skill_name) VALUES(' + jobId + ',1, "' + skill + '")')
    //   }
    // }

    if (job.essentialSkillsSelected != null) {
      //if (!job.essentialSkillsSelected) job.essentialSkillsSelected = []
      //const essentialSkills = job.essentialSkillsSelectedAutocomplete.concat(job.essentialSkillsSelected.filter((item) => job.essentialSkillsSelectedAutocomplete.indexOf(item) < 0))
      for (const skill of job.essentialSkillsSelected) {
        await db.query('INSERT INTO jobs_skills (job_id, essential, skill_name) VALUES(?,1,?)',[jobId,skill])
      }
    }

    // if (job.optionalSkillsSelectedAutocomplete != null) {
    //   //if (!job.optionalSkillsSelected) job.optionalSkillsSelected = []
    //   //const optionalSkills = job.optionalSkillsSelectedAutocomplete.concat(job.optionalSkillsSelected.filter((item) => job.optionalSkillsSelectedAutocomplete.indexOf(item) < 0))
    //   for (const skill of job.optionalSkillsSelectedAutocomplete) {
    //     await db.query('INSERT INTO jobs_skills (job_id, essential, skill_name) VALUES(' + jobId + ',0, "' + skill + '")')
    //   }
    // }

    if (job.optionalSkillsSelected != null) {
      //if (!job.optionalSkillsSelected) job.optionalSkillsSelected = []
      //const optionalSkills = job.optionalSkillsSelectedAutocomplete.concat(job.optionalSkillsSelected.filter((item) => job.optionalSkillsSelectedAutocomplete.indexOf(item) < 0))
      for (const skill of job.optionalSkillsSelected) {
        //await db.query('INSERT INTO jobs_skills (job_id, essential, skill_name) VALUES(' + jobId + ',0, "' + skill + '")')
        await db.query('INSERT INTO jobs_skills (job_id, essential, skill_name) VALUES(?,0,?)',[jobId,skill])
      }
    }

    if (job.sector != null) {
      //if (!job.optionalSkillsSelected) job.optionalSkillsSelected = []
      //const optionalSkills = job.optionalSkillsSelectedAutocomplete.concat(job.optionalSkillsSelected.filter((item) => job.optionalSkillsSelectedAutocomplete.indexOf(item) < 0))
      for (const sector of job.sector) {
        await db.query('INSERT INTO jobs_occupations (job_id, occupation_name) VALUES(?,?)',[jobId,skill])
      }
    }

    // if (job.experiencesSelectedAutocomplete != null) {
    //   if (!job.experiencesSelected) job.experiencesSelected = []
    //   const experiences = job.experiencesSelectedAutocomplete.concat(job.experiencesSelected.filter((item) => job.experiencesSelectedAutocomplete.indexOf(item) < 0))
    //   for (const experience of experiences) {
    //     await db.query('INSERT INTO jobs_occupations (job_id, occupation_name) VALUES (' + jobId + ', "' + experience + '")')
    //   }
    // }

    savedJobs.push(jobId)
  }
  // Deleted jobs not present in the update
  const jobsInDb = await db.query('SELECT job_id FROM jobs WHERE company_id = ?', userId)
  for (const jobInDb of jobsInDb) {
    let jobPresentInResponse = false
    for (const savedJob of savedJobs) {
      if (jobInDb.job_id === savedJob) jobPresentInResponse = true
    }
    if (!jobPresentInResponse) {
      await db.query('UPDATE jobs SET deleted_at = NOW() WHERE job_id = ?', jobInDb.job_id)
    }
  }
}

const updateJob = async (userId, job) => {
  if(!job.place)
    job.place = null
  if(!job.init_date)
    job.init_date = null
  if(!job.end_date)
    job.end_date = null

  if (job.expired === undefined){
    job.expired = false;
  }
  if (job.manual_publish === undefined){
    job.manual_publish = false;
  }
  if (job.publish_ini === undefined){
    job.publish_ini = null;
  }
  if (job.publish_end === undefined){
    job.publish_end = null;
  }
//  console.log,("EL JOB ES: " + JSON.stringify(job))
  
  // It makes no sense to set the job duration value for an undefined job contract  
  if (!job.name || !job.contact_details || !job.languages || !job.sector || !job.contract_types ) {
    throw new Error('Missing required fields (name, contact_details, languages, sector, type of contract)')
  }

  // Check if the skills are included
  if ((!job.essentialSkillsSelected || job.essentialSkillsSelected.length === 0)) {
    throw new Error('Missing required essential skills')
  }

  // Check if the salary is a number
  let jobId = job.job_id
  let image = ""
  
  // console.log("EL VALOR DE LA IMAGEN ES: " + image)
  // If the job has just been created, it has no id
  if (jobId) {

    // Get image for later use, it does not came in the job object so we treat it separately
    const imageSearch = (await db.query('SELECT image FROM jobs WHERE job_id = ?', jobId))[0]
      // if (imageSearch && image == null){
      //   image = imageSearch.image
      //   console.log("EL VALOR DE LA IMAGEN2 ES: " + image)
      // }
      if (imageSearch){
         image = imageSearch.image
         console.log("EL VALOR DE LA IMAGEN2 ES: " + image)
      }
    
    //console.log('INSERT INTO jobs (place,init_date,end_date,job_id, name, contact_details, company_id, language_id, image, job_requirements) VALUES ('+ job.place + '", "' + job.init_date + '", "' +job.end_date + '", ' + jobId + ',"' + job.name + '","' + job.contact_details + '",' + job.company_id + ',(SELECT job_language_id FROM jobs_languages WHERE language ="' + job.language + '"), "' + image + '",  \'' + JSON.stringify(job.experiencesSelected) + '\')')
    // Delete saved job
    //await db.query('DELETE FROM jobs WHERE job_id = ?', jobId)
    // Insert the job with the old id 
    //await db.query('INSERT INTO jobs (job_id,place,init_date,end_date, name, contact_details, company_id, contract_id, language_id, image, job_requirements) VALUES ('+ job.place + '", "' + job.init_date + '", "' +job.end_date + '", "' + jobId + ',"' + job.name + '","' + job.contact_details + '",' + job.company_id + ', (SELECT job_contract_id FROM jobs_contracts WHERE contract="' + job.contract + '"),(SELECT job_language_id FROM jobs_languages WHERE language ="' + job.language + '"), "' + image + '",  \'' + JSON.stringify(job.experiencesSelected) + '\')')

    //let contract  = await db.query('SELECT job_contract_id as id FROM jobs_contracts WHERE contract = ?', job.contract);
    //const contract_id = contract[0].id
    //const language_id = await db.query('SELECT job_language_id FROM jobs_languages WHERE language = "' + job.language + '"');

   // console.log("U contract_id",contract_id)
/*
    const insertResponse = await db.query("INSERT INTO jobs (job_id,place,init_date,end_date, name, contact_details, company_id, contract_id, languages, image, job_requirements) VALUES (?,?,?,?,?,?,?,?,?,?,?)",
        [jobId, job.place, job.init_date, job.end_date, job.name, job.contact_details, job.company_id, contract_id,JSON.stringify(job.languages), image, JSON.stringify(job.experiencesSelected)])
*/
    //const updateResponse = await db.query("UPDATE jobs SET place = ?,init_date = ?, end_date = ?, name = ?, contact_details = ?, company_id = ?, contract_id = ?, languages = ?, image = ?, job_requirements = ? WHERE job_id = ?",
    //    [job.place, job.init_date, job.end_date, job.name, job.contact_details, job.company_id, contract_id,JSON.stringify(job.languages), image, JSON.stringify(job.experiencesSelected),jobId])

    const updateResponse = await db.query("UPDATE jobs SET image = ?,expired = ?, manual_publish = ?, publish_ini = ?, publish_end = ?,place = ?,init_date = ?, end_date = ?, name = ?, contact_details = ?, company_id = ?, contract_types = ?, languages = ?, image = ?, job_requirements = ? WHERE job_id = ?",
        [job.image,job.expired,job.manual_publish,job.publish_ini,job.publish_end,job.place, job.init_date, job.end_date, job.name, job.contact_details, job.company_id, JSON.stringify(job.contract_types),JSON.stringify(job.languages), image, JSON.stringify(job.experiencesSelected),jobId])


    // Deleted the saved job data
    await db.query('DELETE FROM jobs_occupations WHERE job_id = ?', jobId)
    await db.query('DELETE FROM jobs_skills WHERE job_id = ?', jobId)

  } else {
    // Insert the job with a new id
    //console.log('INSERT INTO jobs (place,init_date,end_date,name, contact_details, company_id, language_id, image, job_requirements) VALUES("' + job.place + '", "'+ job.init_date + '", "' +job.end_date + '", "' + job.name + '", "' + job.contact_details + '",' + userId + ',(SELECT job_language_id FROM jobs_languages WHERE language = "' + job.language + '"), "' + image + '", \'' + JSON.stringify(job.experiencesSelected) + '\')')
    //const response = await db.query('INSERT INTO jobs (place,init_date,end_date,name, contact_details, company_id, contract_id, language_id, image, job_requirements) VALUES("' + job.place + '", "'+ job.init_date + '", "' +job.end_date + '", "' + job.name + '", "' + job.contact_details + '",' + userId + ',(SELECT job_contract_id FROM jobs_contracts WHERE contract="' + job.contract + '"),(SELECT job_language_id FROM jobs_languages WHERE language = "' + job.language + '"), "' + image + '", \'' + JSON.stringify(job.experiencesSelected) + '\')')

    //let contract = await db.query('SELECT job_contract_id as id FROM jobs_contracts WHERE contract = ?', job.contract);
    //const contract_id = contract[0].id
    const response = await db.query("INSERT INTO jobs (expired,manual_publish , publish_ini, publish_end,place,init_date,end_date, name, contact_details, company_id,contract_id, contract_types, languages, image, job_requirements) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
        [ job.expired,job.manual_publish,job.publish_ini,job.publish_end,job.place, job.init_date, job.end_date, job.name, job.contact_details, userId, null,JSON.stringify(job.contract_types),JSON.stringify(job.languages), image, JSON.stringify(job.experiencesSelected)])

    // Save the new id
    jobId = response.insertId
    job.job_id = jobId;
  }

  if (job.description != null) {
    //await db.query('UPDATE jobs SET description="' + job.description + '" WHERE job_id=' + jobId)
    await db.query('UPDATE jobs SET description= ? WHERE job_id = ?',[job.description,jobId])
  }
  if (job.receive_candidates_recommendations != null) {
    await db.query('UPDATE jobs SET receive_candidates_recommendations = ? WHERE job_id = ?', [job.receive_candidates_recommendations, jobId])
  }
  if (job.salary){
    //await db.query('UPDATE jobs SET salary="' + job.salary + '" WHERE job_id=' + jobId)
    await db.query('UPDATE jobs SET salary = ? WHERE job_id = ?',[job.salary,jobId])
  }
  //console.log("LA DURACION ES: " + job.duration)
  if (job.duration){
    //console.log('UPDATE jobs SET duration=' + job.duration + ' WHERE job_id=' + jobId)
    //await db.query('UPDATE jobs SET duration=' + job.duration + ' WHERE job_id=' + jobId)
    await db.query('UPDATE jobs SET duration = ? WHERE job_id= ?',[job.duration,jobId])
  }

  if (job.related_file != null) {
    await db.query('UPDATE jobs SET related_file= ? WHERE job_id= ?',[job.related_file, jobId])
  }
  if (job.related_external_url != null) {
    await db.query('UPDATE jobs SET related_external_url= ? WHERE job_id= ?',[job.related_external_url, jobId])
  }

  if (job.essentialSkillsSelected != null) {
    //if (!job.essentialSkillsSelected) job.essentialSkillsSelected = []
    //const essentialSkills = job.essentialSkillsSelectedAutocomplete.concat(job.essentialSkillsSelected.filter((item) => job.essentialSkillsSelectedAutocomplete.indexOf(item) < 0))
    for (const skill of job.essentialSkillsSelected) {
      //await db.query('INSERT INTO jobs_skills (job_id, essential, skill_name) VALUES(' + jobId + ',1, "' + skill + '")')
      await db.query('INSERT INTO jobs_skills (job_id, essential, skill_name) VALUES(?,1,?)',[jobId,skill])
    }
  }
  if (job.optionalSkillsSelected != null) {
    //if (!job.optionalSkillsSelected) job.optionalSkillsSelected = []
    //const optionalSkills = job.optionalSkillsSelectedAutocomplete.concat(job.optionalSkillsSelected.filter((item) => job.optionalSkillsSelectedAutocomplete.indexOf(item) < 0))
    for (const skill of job.optionalSkillsSelected) {
      //await db.query('INSERT INTO jobs_skills (job_id, essential, skill_name) VALUES(' + jobId + ',0, "' + skill + '")',[jobId,skill])
      await db.query('INSERT INTO jobs_skills (job_id, essential, skill_name) VALUES(?,0,?)',[jobId,skill])
    }
  }
  if (job.sector != null) {
    //if (!job.optionalSkillsSelected) job.optionalSkillsSelected = []
    //const optionalSkills = job.optionalSkillsSelectedAutocomplete.concat(job.optionalSkillsSelected.filter((item) => job.optionalSkillsSelectedAutocomplete.indexOf(item) < 0))
    for (const sector of job.sector) {
      await db.query('INSERT INTO jobs_occupations (job_id, occupation_name) VALUES(?,?)',[jobId,sector])
    }
  }

  return job
}


const deleteJob = async (userId, job) => {

  if(job.company_id != userId){
    throw new Error('Only the company of the job can delete the job' )
  }

  let jobId = job.job_id
  let image = ""

  // console.log("EL VALOR DE LA IMAGEN ES: " + image)
  // If the job has just been created, it has no id
  if (jobId) {

    // // Deleted the saved job data
    // await db.query('DELETE FROM jobs_occupations WHERE job_id = ?', jobId)
    // await db.query('DELETE FROM jobs_skills WHERE job_id = ?', jobId)
    // // Delete saved job
    // await db.query('DELETE FROM jobs WHERE job_id = ?', jobId)

    // soft delete
    await db.query('UPDATE jobs SET deleted_at = NOW() WHERE job_id = ?', jobId)
  } else {
    throw new Error('No Job Id Provided job can\'t delete' )
  }
}

const uploadJobImage = async (filename, jobId) => {
  const randomImageName = utils.randomRame(10)
  try {
    if (filename){
      let imageExtension = filename.split('.')
      imageExtension = imageExtension[imageExtension.length - 1]

      const oldPath = path.join(__dirname, '../../public/tmp/' + filename)
      const newPath = path.join(__dirname, '../../public/images/jobs/' + randomImageName + '.' + imageExtension)

      fs.rename(oldPath, newPath, function (error) {
        if (error) console.log(error)
        console.log('Image moved and renamed')
      })
      
      await db.query('UPDATE jobs SET image = ? WHERE job_id = ?', [randomImageName + '.' + imageExtension, jobId])
      const data = {};
      data.filename = randomImageName + '.' + imageExtension;
      return data;
    }else{
      /// esta borrando
      const images = await db.query('SELECT image FROM jobs WHERE job_id = ?',[jobId])
      for (const image of images) {
        const newPath = path.join(__dirname, '../../public/images/jobs/' + image.image)
        console.log("Eliminar imagen ",newPath)
        if(fs.existsSync(newPath)) {
          fs.unlinkSync(newPath);
        }else{
          console.log("No existe imagen ",newPath)
        }
      }
      await db.query('UPDATE jobs SET image = ? WHERE job_id = ?', [null, jobId])
      const data = {
        image_deleted: true
      };
      return data;
    }
  } catch (error) {
    throw new Error('Error updating image: ' + error.message)
  }
}

module.exports = {
  updateJobs,
  updateJob,
  deleteJob,
  getOfferJobs,
  uploadJobImage
}
