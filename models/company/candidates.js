const db = require('../../db')
const utils = require('../../utils')
const fs = require('fs')
const path = require('path')
const userProfile = require('../people/user-profile')

const doFavCandidate = async (userId, candidateId) => {
    
    try{
        const response = await db.query("INSERT INTO users_relationship (id_company, id_favocandidate) VALUES(" + userId +  ", "
     + candidateId + ") ON DUPLICATE KEY UPDATE id_company = " + userId + ", id_favocandidate = " + candidateId)
        
        operation_id = response.insertId
    }catch(error){
        throw new Error('Error relationing users: ' + error.message)
    }
    
}

const deleteFavCandidate = async (userId, candidateId) => {
    
    try{
        const response = await db.query("DELETE FROM users_relationship WHERE id_company = " + userId + " AND id_favocandidate = " + candidateId )
            
        operation_id = response.insertId
    }catch(error){
        throw new Error('Error deleting users relationship: ' + error.message)
    }
    
}

const getCandidates = async(userId) => {

    const data = {}
    const candidates = await db.query("SELECT * FROM users WHERE user_id IN (SELECT id_favocandidate FROM users_relationship WHERE id_company = " + userId + ") ORDER BY full_name")
    
    for (const i in candidates) {
        candidates[i].userProfile = await userProfile.getUserProfile(candidates[i].user_id)
    }

    return candidates

}

module.exports = {
    doFavCandidate,
    deleteFavCandidate,
    getCandidates
  }