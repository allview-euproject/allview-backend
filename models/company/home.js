const db = require('../../db')

const getHome = async (userId) => {
  const data = {}

  // Using the IMDB weighted average method, wich is a true Bayesian estimate
  /*
   weighted rating (WR) = (v ÷ (v+m)) × R + (m ÷ (v+m)) × C
   R = average rating
   v = number of votes for the movie = (votes)
   m = minimum votes required to be listed in the Top
   C = the mean vote across the whole data
  */
  const m = 1 // We can increase the value if the number of ratings increase
  const ratingGlobalAvgData = await db.query('SELECT AVG(rating) AS rating_global_avg FROM users_courses WHERE users_courses.deleted_at IS NULL')
  const C = ratingGlobalAvgData[0].rating_global_avg

  // Get the rating average and vote count for each course
  const ratedCoursesAgg = await db.query('SELECT course_id, AVG(rating) AS rating_avg, COUNT(course_id) AS rated_times ' +
   'FROM users_courses WHERE users_courses.deleted_at IS NULL GROUP BY course_id')

  const weightedRating = []
  for (const ratedCourseAgg of ratedCoursesAgg) {
    // Skip courses with less than m votes
    if (ratedCourseAgg.rated_times < m) continue
    const R = ratedCourseAgg.rating_avg
    const v = ratedCourseAgg.rated_times
    const WR = (v / (v + m)) * R + (m / (v + m)) * C

    // Store all the valid weighted ratings
    weightedRating.push({
      course_id: ratedCourseAgg.course_id,
      weighted_rating: WR
    })
  }

  // Sort the top courses by weighted rating
  weightedRating.sort((a, b) => b.weighted_rating - a.weighted_rating)

  // Save the top four courses in the data array
  data.topRatedCourses = []
  let counter = 0
  for (const topCourse of weightedRating) {
    const course = await db.query('SELECT course_id, title, partner, image FROM courses ' +
      'INNER JOIN courses_partners ON courses.partner_id = courses_partners.course_partner_id WHERE course_id = ?', topCourse.course_id)
    // const course = await db.query('SELECT * FROM courses ' +
    //    'INNER JOIN courses_partners ON courses.partner_id = courses_partners.course_partner_id WHERE course_id = ?', topCourse.course_id)
    data.topRatedCourses.push(course[0])
    counter++
    if (counter >= 4) break
  }

  data.recommendedCourses = await db.query('SELECT * FROM (SELECT courses.course_id, courses.title, courses.image, courses_partners.partner FROM users_courses_recommendations ' +
    'INNER JOIN courses ON courses.course_id = users_courses_recommendations.course_id ' +
    'INNER JOIN courses_partners ON courses.partner_id = courses_partners.course_partner_id ' +
    'WHERE users_courses_recommendations.user_id = ? AND predicted_rating > 2.5 ' +
    'AND courses.course_id NOT IN (SELECT course_id FROM users_courses WHERE users_courses.deleted_at IS NULL AND user_id = ?) ' +
    'ORDER BY predicted_rating DESC) A GROUP BY course_id LIMIT 4', [userId, userId])

  // data.recommendedCourses = await db.query('SELECT * FROM (SELECT * FROM users_courses_recommendations ' +
  // 'INNER JOIN courses ON courses.course_id = users_courses_recommendations.course_id ' +
  // 'INNER JOIN courses_partners ON courses.partner_id = courses_partners.course_partner_id ' +
  // 'WHERE users_courses_recommendations.user_id = ? AND predicted_rating > 2.5 ' +
  // 'AND courses.course_id NOT IN (SELECT course_id FROM users_courses WHERE user_id = ?) ' +
  // 'ORDER BY predicted_rating DESC) A GROUP BY course_id LIMIT 4', [userId, userId])

  // If there are less than 4 recommended corses, recommend courses from the most demanded skills from the jobs published in the platform
  // If the user_id and candidate_id are 0, these are the general recommendations
  if (data.recommendedCourses.length < 4) {
    const generalRecommendedCourses = await db.query('SELECT courses.course_id, courses.title, courses.image, courses_partners.partner FROM candidates_recommendations ' +
    'INNER JOIN courses ON courses.course_id = candidates_recommendations.course_id ' +
    'INNER JOIN courses_partners ON courses.partner_id = courses_partners.course_partner_id ' +
    'WHERE candidates_recommendations.user_id = 0 AND candidates_recommendations.candidate_id = 0 ' +
    'AND courses.course_id NOT IN (SELECT course_id FROM users_courses WHERE users_courses.deleted_at IS NULL AND user_id = ?) ' +
    'ORDER BY score DESC LIMIT 4', userId)

    // const generalRecommendedCourses = await db.query('SELECT * FROM candidates_recommendations ' +
    //   'INNER JOIN courses ON courses.course_id = candidates_recommendations.course_id ' +
    //   'INNER JOIN courses_partners ON courses.partner_id = courses_partners.course_partner_id ' +
    //   'WHERE candidates_recommendations.user_id = 0 AND candidates_recommendations.candidate_id = 0 ' +
    //   'AND courses.course_id NOT IN (SELECT course_id FROM users_courses WHERE user_id = ?) ' +
    //   'ORDER BY score DESC LIMIT 4', userId)
    // Concat arrays
    data.recommendedCourses = data.recommendedCourses.concat(generalRecommendedCourses)
  }

  // If there are still less than 4 courses available, get random courses
  if (data.recommendedCourses.length < 4) {
    const randomCourses = await db.query('SELECT courses.course_id, courses.title, courses.image, courses_partners.partner FROM courses ' +
      'INNER JOIN courses_partners ON courses.partner_id = courses_partners.course_partner_id ' +
      'AND courses.course_id NOT IN (SELECT course_id FROM users_courses WHERE users_courses.deleted_at IS NULL AND user_id = ?) ' +
      'ORDER BY RAND() LIMIT 4', userId)

    // const randomCourses = await db.query('SELECT * FROM courses ' +
    //   'INNER JOIN courses_partners ON courses.partner_id = courses_partners.course_partner_id ' +
    //   'AND courses.course_id NOT IN (SELECT course_id FROM users_courses WHERE user_id = ?) ' +
    //   'ORDER BY RAND() LIMIT 4', userId)
    // Concat arrays
    data.recommendedCourses = data.recommendedCourses.concat(randomCourses)
  }

  // Get just the first four courses
  data.recommendedCourses = data.recommendedCourses.slice(0, 4)

  data.recommendedCandidates = await db.query('SELECT user_job_recommendation_id, users.*, jobs.name AS job_name FROM users_jobs_recommendations ' +
    'INNER JOIN jobs ON jobs.job_id = users_jobs_recommendations.job_id ' +
    'INNER JOIN users ON users.user_id = users_jobs_recommendations.user_id ' +
    'WHERE users_jobs_recommendations.job_id IN (SELECT job_id FROM jobs WHERE deleted_at IS NULL AND company_id = ?) ' +
    'AND jobs.receive_candidates_recommendations = 1 ' +
    'ORDER BY score DESC LIMIT 4', userId)

  return data
}

module.exports = {
  getHome
}
