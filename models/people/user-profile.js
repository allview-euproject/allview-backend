const db = require('../../db')
const fs = require('fs')
const path = require('path')
const utils = require('../../utils')

const getProfileImage = async (email) => {
  const personalInformation = await db.query('SELECT image FROM users WHERE email = ?', email)
  return personalInformation[0]
}

const getUserProfile = async (userId) => {
  const data = {}

  // User personal information
  // const personalInformation = await db.query('SELECT u.cv, u.image, u.user_id, u.email, u.full_name, u.description, u.company_identification, u.street, ' +
  //   'u.city, u.state, u.country, u.website, u.phone, u.contact_person, o.occupation_name FROM users u LEFT JOIN occupations o ON u.sector_id = o.occupation_id WHERE u.user_id = ?', userId)
  const personalInformation = await db.query('SELECT u.cv, u.image, u.user_id, u.email, u.full_name, u.description, u.company_identification, u.street, ' +
     'u.city, u.state, u.country, u.website, u.phone, u.contact_person, u.contact_mail FROM users u WHERE u.user_id = ?', userId)
  data.personalInformation = personalInformation[0]

  // User skills
  // TODO: las skills sin code son ultimo level?
  const skills = await db.query('SELECT * FROM users_skills INNER JOIN skills ON users_skills.skill_id = skills.skill_id WHERE user_id = ?', userId)
  data.skills = []
  for (const skill of skills) {
    data.skills.push(skill.skill_name)
  }

  // User competences
  const competences = await db.query('SELECT * FROM users_competences INNER JOIN skills ON users_competences.skill_id = skills.skill_id WHERE user_id = ?', userId)
  data.competences = []
  for (const competence of competences) {
    data.competences.push(competence.skill_name)
  }

  // User experiences
  const experiences = await db.query('SELECT * FROM users_experiences INNER JOIN occupations ON users_experiences.occupation_id = occupations.occupation_id WHERE user_id = ?', userId)
  data.experiences = []
  for (const experience of experiences) {
    data.experiences.push(experience.occupation_name)
  }

  // User education
  const educations = await db.query('SELECT * FROM users_educations WHERE user_id = ?', userId)
  // Convert the start and end year to an array
  for (const i in educations) {
    educations[i].startAndEndYear = [educations[i].start_year, educations[i].end_year]
  }
  data.educations = educations

  // User challenges
  // const challenges = await db.query('SELECT * FROM users_occupations INNER JOIN occupations ON users_occupations.occupation_id = occupations.occupation_id WHERE user_id = ?', userId)
  const challenges = await db.query('SELECT * FROM users_occupations WHERE user_id = ?', userId)
  data.challenges = []
  for (const challenge of challenges) {
    data.challenges.push(challenge.occupation_name)
  }

  data.missingProfile = false
  let missingProfileCheckSkills = await db.query('SELECT user_id FROM users_skills WHERE user_id = ?', userId)
  let missingProfileCheckChallenges = await db.query('SELECT user_id FROM users_occupations WHERE user_id = ?', userId)
  let missingProfileCheckCompetences = await db.query('SELECT user_id FROM users_competences WHERE user_id = ?', userId)
  let missingProfileCheckEducations = await db.query('SELECT user_id FROM users_educations WHERE user_id = ?', userId)
  let missingProfileCheckExperiences = await db.query('SELECT user_id FROM users_experiences WHERE user_id = ?', userId)
  if (missingProfileCheckSkills.length === 0 && missingProfileCheckChallenges.length === 0 && missingProfileCheckCompetences.length === 0 
    && missingProfileCheckEducations.length === 0 && missingProfileCheckExperiences.length === 0) {
    data.missingProfile = true
  }
  
  return data
}

const updatePersonalInformation = async (userId, fullName, city, state, country, phone, description, companyIdentification, street, contactPerson, website, mainProduct, sector) => {
  if (fullName) {
    try {
      await db.query('UPDATE users SET full_name = ? WHERE user_id = ?', [fullName, userId])
    } catch (error) {
      throw new Error('Error updating full name: ' + error.message)
    }
  }
  if (city) {
    try {
      await db.query('UPDATE users SET city = ? WHERE user_id = ?', [city, userId])
    } catch (error) {
      throw new Error('Error updating city: ' + error.message)
    }
  }
  if (state) {
    try {
      await db.query('UPDATE users SET state = ? WHERE user_id = ?', [state, userId])
    } catch (error) {
      throw new Error('Error updating state: ' + error.message)
    }
  }
  if (country) {
    try {
      await db.query('UPDATE users SET country = ? WHERE user_id = ?', [country, userId])
    } catch (error) {
      throw new Error('Error updating country: ' + error.message)
    }
  }
  if (phone) {
    // A phone number should have 12 digits max
    if (phone.match(/\d/g).length > 12) throw new Error('Error updating phone: the number is too long')
    // A phone number should have 8 digits min
    if (phone.match(/\d/g).length < 8) throw new Error('Error updating phone: the number is too short')
    try {
      await db.query('UPDATE users SET phone = ? WHERE user_id = ?', [phone, userId])
    } catch (error) {
      throw new Error('Error updating phone: ' + error.message)
    }
  }
  if (description) {
    try {
      await db.query('UPDATE users SET description = ? WHERE user_id = ?', [description, userId])
    } catch (error) {
      throw new Error('Error updating description: ' + error.message)
    }
  }
  if (companyIdentification) {
    try {
      await db.query('UPDATE users SET company_identification = ? WHERE user_id = ?', [companyIdentification, userId])
    } catch (error) {
      throw new Error('Error updating company identification: ' + error.message)
    }
  }
  if (street) {
    try {
      await db.query('UPDATE users SET street = ? WHERE user_id = ?', [street, userId])
    } catch (error) {
      throw new Error('Error updating street: ' + error.message)
    }
  }
  if (contactPerson) {
    try {
      await db.query('UPDATE users SET contact_person = ? WHERE user_id = ?', [contactPerson, userId])
    } catch (error) {
      throw new Error('Error updating contact person: ' + error.message)
    }
  }
  if (website) {
    try {
      await db.query('UPDATE users SET website = ? WHERE user_id = ?', [website, userId])
    } catch (error) {
      throw new Error('Error updating website: ' + error.message)
    }
  }
  if (mainProduct) {
    try {
      await db.query('UPDATE users SET main_product = ? WHERE user_id = ?', [mainProduct, userId])
    } catch (error) {
      throw new Error('Error updating main product: ' + error.message)
    }
  }
  if (sector) {
    try {
      await db.query('UPDATE users SET sector_id = (SELECT occupation_id FROM occupations WHERE occupation_level=2 AND occupation_name= ? ) WHERE user_id = ?', [sector, userId])
    } catch (error) {
      throw new Error('Error updating sector: ' + error.message)
    }
  }
}

const updateUserEducations = async (userId, educations) => {
  // Delete the current saved educations
  await db.query('DELETE FROM users_educations WHERE user_id = ?', userId)

  // Insert the updated educations
  for (const education of educations) {
    // Check if all the fields are present
    if (!education.university || !education.level || !education.name || !education.startAndEndYear[0] || !education.startAndEndYear[1]) {
      throw new Error('Required field is missing')
    }
    await db.query('INSERT INTO users_educations (user_id, university, level, name, start_year, end_year) VALUES(?,?,?,?,?,?)',
      [userId, education.university, education.level, education.name, education.startAndEndYear[0], education.startAndEndYear[1]])
  }
}

const updateUserSkills = async (userId, skills) => {
  // Delete the current saved skills
  await db.query('DELETE FROM users_skills WHERE user_id = ?', userId)

  // Insert the updated skills
  for (const skillName of skills) {
    const skillIdSearch = await db.query('SELECT skill_id FROM skills WHERE skill_name = ? LIMIT 1', skillName)
    await db.query('INSERT INTO users_skills (user_id, skill_id) VALUES(?,?)', [userId, skillIdSearch[0].skill_id])
  }
}

const updateUserCompetences = async (userId, competences) => {
  // Delete the current saved competences
  await db.query('DELETE FROM users_competences WHERE user_id = ?', userId)

  // Insert the updated competences
  for (const competenceName of competences) {
    const competenceIdSearch = await db.query('SELECT skill_id FROM skills WHERE skill_name = ? LIMIT 1', competenceName)
    await db.query('INSERT INTO users_competences (user_id, skill_id) VALUES(?,?)', [userId, competenceIdSearch[0].skill_id])
  }
}

const updateUserChallenges = async (userId, challenges) => {
  console.log("usewrid",userId);
  console.log("challenges",challenges);
  // Delete the current saved occupations
  await db.query('DELETE FROM users_occupations WHERE user_id = ?', userId)
console.log("challenges",challenges);
  // Insert the updated occupations
  for (let challengeName of challenges) {
    challengeName = challengeName.trim();
    const challengeIdSearch = await db.query('SELECT occupation_id FROM occupations WHERE occupation_name LIKE ? LIMIT 1', "%"+challengeName+"%")
    console.log("challengeIdSearch",challengeIdSearch  );
    await db.query('INSERT INTO users_occupations (user_id, occupation_id, occupation_name) VALUES (?,?,?)', [userId, challengeIdSearch[0].occupation_id, challengeName])
  }
}

const updateUserExperiences = async (userId, experiences) => {
  // Delete the current saved occupations
  await db.query('DELETE FROM users_experiences WHERE user_id = ?', userId)

  // Insert the updated occupations
  for (const experienceName of experiences) {
    const experienceIdSearch = await db.query('SELECT occupation_id FROM occupations WHERE occupation_name = ? LIMIT 1', experienceName)
    await db.query('INSERT INTO users_experiences (user_id, occupation_id) VALUES(?,?)', [userId, experienceIdSearch[0].occupation_id])
  }
}

const uploadCv = async (filename, userId) => {
  const randomCvName = utils.randomRame(10)
  try {
    let cvExtension = filename.split('.')
    cvExtension = cvExtension[cvExtension.length - 1]
    const newCvName = randomCvName + '.' + cvExtension

    const oldPath = path.join(__dirname, '../../public/tmp/' + filename)
    const newPath = path.join(__dirname, '../../public/cvs/' + newCvName)

    fs.rename(oldPath, newPath, function (error) {
      if (error) console.log(error)
      console.log('Cv moved and renamed')
    })

    await db.query('UPDATE users SET cv = ? WHERE user_id = ?', [newCvName, userId])
  } catch (error) {
    throw new Error('Error updating cv: ' + error.message)
  }
}

const deleteCv = async (userId) => {
  try {
    await db.query('UPDATE users SET cv = null WHERE user_id = ?', userId)
  } catch (error) {
    throw new Error(error.message)
  }
}

module.exports = {
  getProfileImage,
  getUserProfile,
  updatePersonalInformation,
  updateUserSkills,
  updateUserCompetences,
  updateUserEducations,
  updateUserExperiences,
  updateUserChallenges,
  uploadCv,
  deleteCv
}
