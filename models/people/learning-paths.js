const utils = require('../../utils')
const db = require('../../db')

const getLearningPaths = async (userId) => {
  // User challenges
  const challenges = await db.query('SELECT * FROM users_occupations INNER JOIN occupations ON users_occupations.occupation_id = occupations.occupation_id WHERE user_id = ?', userId)
  for (const i in challenges) {
    challenges[i].name = ((challenges[i].occupation_code) || '') + ' ' + challenges[i].occupation_name
    challenges[i].id = challenges[i].occupation_id
    // The occupation has childrens only if it is an occupation with a code
    const aux = Number.parseInt(challenges[i].occupation_code, 10)
    if (challenges[i].occupation_code - aux === 0) {
      // This empty array is used by Vuetify to detect that it has childrens
      challenges[i].children = []
    }
  }

  const userChallenges = {}
  for (const challenge of challenges) {
    // Get essential and optional skills per job challenge
    const essentialSkills = await db.query('SELECT skills.skill_name, skills.skill_description FROM skills_occupations_essential_relations INNER JOIN skills ' +
      'ON skills_occupations_essential_relations.skill_id = skills.skill_id WHERE occupation_id = ?', challenge.occupation_id)
    const optionalSkills = await db.query('SELECT skills.skill_name, skills.skill_description FROM skills_occupations_optional_relations INNER JOIN skills ON ' +
      'skills_occupations_optional_relations.skill_id = skills.skill_id WHERE occupation_id = ?', challenge.occupation_id)

    // TODO capitalize the rest of the name
    const occupationName = utils.capitalize(challenge.occupation_name)

    userChallenges[occupationName] = {}
    userChallenges[occupationName].essential = essentialSkills
    userChallenges[occupationName].optional = optionalSkills
  }

  // User skills
  const userSkills = await db.query('SELECT * FROM users_skills INNER JOIN skills ON users_skills.skill_id = skills.skill_id WHERE user_id = ?', userId)

  return { userChallenges: userChallenges, userSkills: userSkills }
}

module.exports = {
  getLearningPaths
}
