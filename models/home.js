require('dotenv')
const jwt = require('jwt-simple')
const bcrypt = require('bcrypt')
const utils = require('../utils')
const db = require('../db')
const nodemailer = require('nodemailer')
const OAuth2Client = require('google-auth-library')

// nodemailer config
const EMAIL_CONFIG = {
  host: process.env.EMAIL_HOST,
  port: process.env.EMAIL_PORT,
  secure: true,
  auth: {
    type: 'login',
    user: process.env.EMAIL_AUTH_USER,
    pass: process.env.EMAIL_AUTH_PASS
  }
}

const landing = async () => {
  const users = (await db.query('SELECT COUNT(user_id) as total_users FROM users'))[0].total_users
  const jobs = (await db.query('SELECT COUNT(job_id) as total_jobs FROM jobs WHERE deleted_at IS NULL'))[0].total_jobs
 // const deleted_jobs = (await db.query('SELECT COUNT(job_id) as total_jobs FROM jobs WHERE deleted_at IS NOT NULL'))[0].total_jobs
  const courses = (await db.query('SELECT COUNT(course_id) as total_courses FROM courses WHERE deleted_at IS NULL AND (expired = false AND manual_publish = TRUE ' +
      'OR ( ' +
      '    (publish_ini IS NULL OR CURRENT_TIMESTAMP > publish_ini) \n' +
      '    AND \n' +
      '    (publish_end IS NULL OR CURRENT_TIMESTAMP < publish_end) \n' +
      '    AND \n' +
      '    (end_date IS NULL OR CURRENT_TIMESTAMP < end_date) \n' +
      '))'))[0].total_courses

  return {
    users: users,
    jobs: jobs,
 //   deleted_jobs:deleted_jobs,
    courses: courses
  }
}


const landing_courses_jobs = async () => {
console.log("landing_courses_jobs")
  const data = {}

  // Using the IMDB weighted average method, wich is a true Bayesian estimate
  /*
   weighted rating (WR) = (v ÷ (v+m)) × R + (m ÷ (v+m)) × C
   R = average rating
   v = number of votes for the movie = (votes)
   m = minimum votes required to be listed in the Top
   C = the mean vote across the whole data
  */
  const m = 1 // We can increase the value if the number of ratings increase
  const ratingGlobalAvgData = await db.query('SELECT AVG(rating) AS rating_global_avg FROM users_courses WHERE users_courses.deleted_at IS NULL')
  const C = ratingGlobalAvgData[0].rating_global_avg

  // Get the rating average and vote count for each course
  const ratedCoursesAgg = await db.query('SELECT course_id, AVG(rating) AS rating_avg, COUNT(course_id) AS rated_times ' +
      'FROM users_courses WHERE users_courses.deleted_at IS NULL GROUP BY course_id')

  const weightedRating = []
  for (const ratedCourseAgg of ratedCoursesAgg) {
    // Skip courses with less than m votes
    if (ratedCourseAgg.rated_times < m) continue
    const R = ratedCourseAgg.rating_avg
    const v = ratedCourseAgg.rated_times
    const WR = (v / (v + m)) * R + (m / (v + m)) * C

    // Store all the valid weighted ratings
    weightedRating.push({
      course_id: ratedCourseAgg.course_id,
      weighted_rating: WR
    })
  }

  // Sort the top courses by weighted rating
  weightedRating.sort((a, b) => b.weighted_rating - a.weighted_rating)

  // Save the top four courses in the data array
  data.topRatedCourses = []
  let counter = 0
  console.log("weightedRating",JSON.stringify(weightedRating))
  for (const topCourse of weightedRating) {

    const course = await db.query('SELECT course_id, title, partner, image FROM courses ' +
        'LEFT JOIN courses_partners ON courses.partner_id = courses_partners.course_partner_id ' +
        ' WHERE course_id = ? AND deleted_at is null AND (expired = FALSE AND manual_publish = TRUE ' +
            'OR ( ' +
            '    (publish_ini IS NULL OR CURRENT_TIMESTAMP > publish_ini) \n' +
            '    AND \n' +
            '    (publish_end IS NULL OR CURRENT_TIMESTAMP < publish_end) \n' +
            '    AND \n' +
            '    (end_date IS NULL OR CURRENT_TIMESTAMP < end_date) \n' +
            ')) order by created_at DESC LIMIT 1', topCourse.course_id)

    // const course = await db.query('SELECT * FROM courses ' +
    //    'INNER JOIN courses_partners ON courses.partner_id = courses_partners.course_partner_id WHERE course_id = ?', topCourse.course_id)
    if(course[0] != null) {
      data.topRatedCourses.push(course[0])
      counter++
    }
    if (counter >= 4) break
  }
  console.log("data.topRatedCourses",JSON.stringify(data.topRatedCourses))
  /*
  SELECT course_id, title, partner, image FROM courses
        LEFT JOIN courses_partners ON courses.partner_id = courses_partners.course_partner_id
         WHERE course_id = ? AND deleted_at is null AND (expired = FALSE AND manual_publish = TRUE
            OR (
                (publish_ini IS NULL OR CURRENT_TIMESTAMP > publish_ini)
                AND
                (publish_end IS NULL OR CURRENT_TIMESTAMP < publish_end)
                AND
                (end_date IS NULL OR CURRENT_TIMESTAMP < end_date)
            )) order by created_at DESC LIMIT 4
  * */

  let query = '' +
      'SELECT * FROM jobs WHERE deleted_at is null AND (expired = false AND manual_publish = TRUE ' +
      'OR ( ' +
      '    (publish_ini IS NULL OR CURRENT_TIMESTAMP > publish_ini) \n' +
      '    AND \n' +
      '    (publish_end IS NULL OR CURRENT_TIMESTAMP < publish_end) \n' +
      '    AND \n' +
      '    (end_date IS NULL OR CURRENT_TIMESTAMP < end_date) \n' +
      ')) order by created_at DESC LIMIT 4';

  console.log("job>",query)
  data.jobs = (await db.query(query))


  return data
}


const login = async (email, password) => {
  // Check if all the fields are present
  if (!email || !password) {
    throw new Error('Missing fields')
  }

  // Check if the email is valid
  if (!utils.validateEmail(email)) {
    throw new Error('Invalid email address')
  }

  // Check if the db has this email and matches the hash of the password
  const userSearch = await db.query('SELECT user_id, email, password_hash, role_id, verified FROM users WHERE email = ?', email)
  if (userSearch.length === 0) {
    throw new Error('The email is not registered in the system')
  }

  // Check if the user was verified
  if (userSearch[0].verified === 0) {
    throw new Error('Account not verified. Please verify your account by clicking on the link sent to your email. Please check your mail spam Inbox.')
  }

  // Check if the email was registered with a google account and it does not have a password yet
  const googleSearch = await db.query('SELECT password_hash FROM users WHERE email = ?', email)
  if (!googleSearch[0].password_hash) {
    throw new Error('This email was registered using Google Login, please use it to login.')
  }

  // Compares the saved hash with the password
  if (!await bcrypt.compare(password, userSearch[0].password_hash)) {
    throw new Error('Invalid password')
  }

  // Gets the role ID of the user
  const roleId = userSearch[0].role_id

  return {
    roleId: roleId,
    token: createToken(userSearch[0].user_id)
  }
}

const register = async (name, email, role, password, passwordCheck) => {
  // Check if all the fields are present
  if (!name || !email || !role || !password) {
    throw new Error('Missing fields')
  }

  // Check if both passwords are the same
  if (password !== passwordCheck) {
    throw new Error('Passwords do not match')
  }

  // Check if the email is valid
  if (!utils.validateEmail(email)) {
    throw new Error('Invalid email address')
  }

  // Check if the password has at least 8 characters with numbers and letters
  utils.isPasswordSecure(password)

  // Check if the email was already used
  const emailSearch = await db.query('SELECT email,verified FROM users WHERE email = ?', email)
  if (emailSearch.length > 0) {
    if(emailSearch[0].verified === 1)
      throw new Error('Email was already used')
    else
      throw new Error('Email was already used, Account not verified. Please verify your account by clicking on the link sent to your email. Please check your mail spam Inbox.')
  }

  // Process the input role string
  role = role.toLowerCase().replace(' ', '-')
  // Get the role id
  const roleSearch = await db.query('SELECT role_id FROM roles WHERE role = ?', role)
  if (roleSearch.length === 0) {
    throw new Error('The role is not valid')
  }
  const roleId = roleSearch[0].role_id

  // Hash with the Bcrypt algorithm using a bcrypt cost of 12 (2^12 salting rounds)
  // Bcrypt cost should be modified to take around 250ms
  console.time('hash')
  const passwordHash = await bcrypt.hash(password, 12)
  console.timeEnd('hash')

  // Insert the new user
  await db.query('INSERT INTO users (full_name, email, contact_mail, password_hash, role_id) VALUES(?,?,?,?,?)', [name, email, email, passwordHash, roleId])
  await sendVerifyAccountEmail(email)
}

// Google Login with backend authentication
// https://developers.google.com/identity/sign-in/web/backend-auth
const googleLogin = async (googleToken) => {
  const client = new OAuth2Client.OAuth2Client(process.env.GOOGLE_CLIENT_ID)
  const ticket = await client.verifyIdToken({
    idToken: googleToken,
    audience: process.env.GOOGLE_CLIENT_ID
  })
  const payload = ticket.getPayload()
  const email = payload.email
  const googleSub = payload.sub
  // May be useful in the future: const image = payload.picture
  const name = payload.name || payload.given_name

  // Check if the user was already registered
  const emailSearch = await db.query('SELECT user_id, email FROM users WHERE email = ?', email)
  if (emailSearch.length === 0) {
    // Insert the new user (there is no need to verify the email)
    await db.query('INSERT INTO users (email, google_sub, full_name) VALUES(?,?,?)', [email, googleSub, name])
    // If the account was registered using the normal login, add the Google login identifier
  } else {
    await db.query('UPDATE users SET google_sub = ? WHERE email = ?', [googleSub, email])
  }

  const userSearch = await db.query('SELECT user_id, role_id FROM users WHERE email = ?', email)
  // Gets the role ID of the user
  const roleId = userSearch[0].role_id

  return {
    roleId: roleId,
    token: createToken(userSearch[0].user_id)
  }
}

// Verify a new account by sending an email with a verify token
const sendVerifyAccountEmail = async (email) => {
  // comprobar si está pendiente de verificar, sino error
  const emailSearch = await db.query('SELECT email,verified FROM users WHERE email = ?', email)
  if (emailSearch.length > 0) {
    if(emailSearch[0].verified === 1)
      throw new Error('Email was already verified')
    else{
      const token = await createVerifyToken()
      const content = {
        from: process.env.EMAIL_SENDER_ADDRESS,
        to: email,
        subject: 'Verify your account on the AllVIEW Platform',
        html: `
      <div> 
      <h4>Hello and welcome to the AllVIEW Platform.</h4> 
      <p>Please click here: <a href="` + process.env.EMAIL_BODY_PLATFORM_URL + 'verify?token=' + token + '" target="_blank">' + process.env.EMAIL_BODY_PLATFORM_URL + 'verify?token=' + token + `</a> to verify your account</p>
      <p>This link will only be available if this is your last confirmation mail. Please register again or try to login if your want a new link.</p>
  `
      }
      // Send the email and check if it has been sent successfully
      const transporter = nodemailer.createTransport(EMAIL_CONFIG)
      transporter.sendMail(content, async (error, info) => {
        if (!error) {
          console.log('Email sent to', email)
          await db.query('UPDATE users SET verify_token = ? WHERE email = ?', [token, email])
          // TODO: develop an alternative that does not depend if the server keeps running to delete the token
          setTimeout(() => expireVerifyToken(email), 600000)
        } else {
          console.log(error)
          throw new Error(error.message)
        }
      })
    }
  }

}

// Recover account by email
const recoverPasswordEmail = async (email) => {
  const verifiedCheck = await db.query('SELECT verified FROM users WHERE email = ? AND verified = 1', email)
  // If the email exists and has been verified
  if (verifiedCheck.length > 0) {
    // Create a token to change password
    const token = await createVerifyToken()
    const content = {
      from: process.env.EMAIL_SENDER_ADDRESS,
      to: email,
      subject: 'Password recovery request on AllVIEW Platform',
      html: `
      <div> 
      <h4>Hello,</h4> 
      <p>A request to recover the password in our platform has been requested using this email, if it weren't you just ignore this message. Otherwise, click here: <a href=" ` + process.env.EMAIL_BODY_PLATFORM_URL + 'recover-password?token=' + token + '" target="_blank">' + process.env.EMAIL_BODY_PLATFORM_URL + 'recover-password?token=' + token + `</a> to restore your password.</p>
      <p>This link will only be available for 1 hour, after that time your request will become invalid.</p>
  `
    }
    // Send the email and check if it has been sent successfully
    const transporter = nodemailer.createTransport(EMAIL_CONFIG)
    transporter.sendMail(content, async (error, info) => {
      if (!error) {
        console.log('Email sent to', email)
        await db.query('UPDATE users SET verify_token="' + token + '" WHERE email="' + email + '"')
        // TODO: develop an alternative that does not depend if the server keeps running to delete the token
        setTimeout(() => expireVerifyToken(email), 600000)
      } else {
        console.log(error)
        throw new Error(error.message)
      }
    })
  } else {
    throw new Error('The given email has not been verified or does not exist')
  }
}

const recoverPassword = async (token, password, passwordCheck) => {
  if (!password) {
    throw new Error('Missing fields')
  }

  // Check if both passwords are the same
  if (password !== passwordCheck) {
    throw new Error('Passwords do not match')
  }

  // Check if the password has at least 8 characters with numbers and letters
  utils.isPasswordSecure(password)

  // Hash with the Bcrypt algorithm using a bcrypt cost of 12 (2^12 salting rounds)
  // Bcrypt cost should be modified to take around 250ms
  console.time('hash')
  const passwordHash = await bcrypt.hash(password, 12)
  console.timeEnd('hash')
  const response = await db.query('UPDATE users SET password_hash = ?, verify_token = null WHERE verify_token = ?', [passwordHash, token])
  return response
}

// Set the user as verified and delete the user if it is not verified
const expireVerifyToken = async (email) => {
  await db.query('UPDATE users SET verify_token = null WHERE email = ?', email)
  //await db.query('DELETE FROM users WHERE email = ? AND verified = 0', email)
}

// Verify users with the register code sent by email
const verify = async (token) => {
  const verifyResult = await db.query('UPDATE users SET verified = 1, verify_token = null WHERE verify_token = ?', token)
  if (verifyResult.changedRows === 1) {
    return true
  } else {
    throw new Error('Verify token not found or expired')
  }
}

const setRole = async (userId, role) => {
  // Process the input role string
  role = role.toLowerCase().replace(' ', '-')
  // Get the role id
  const roleSearch = await db.query('SELECT role_id FROM roles WHERE role = ?', role)
  if (roleSearch.length === 0) {
    throw new Error('The role is not valid')
  }
  const roleId = roleSearch[0].role_id
  // Set the user role id
  await db.query('UPDATE users SET role_id = ? WHERE user_id = ?', [roleId, userId])
  return { roleId: roleId }
}

// Helper functions

// Creates a token and checks if it is not already being used in the DB
const createVerifyToken = async () => {
  let token
  while (true) {
    // A token is a random number between 0 and 10000000000000000
    token = Math.floor(Math.random() * 10000000000000000)
    const tokenSearch = await db.query('SELECT verify_token FROM users WHERE verify_token = ?', token)
    // If the token is not being used, exit loop and return the token
    if (tokenSearch.length === 0) break
  }
  return token
}

// Creates a JWT token with the email as the payload
const createToken = (userId) => {
  const now = new Date()
  const payload = {
    userId: userId,
    createdAt: now,
    expiresAt: now.setDate(now.getDate() + 365) // Authentication lasts 1 year
  }
  return jwt.encode(payload, process.env.JWT_TOKEN, 'HS512') // HMAC SHA512
}

module.exports = {
  login,
  register,
  verify,
  recoverPasswordEmail,
  recoverPassword,
  googleLogin,
  setRole,
  landing,
  landing_courses_jobs,
  sendVerifyAccountEmail
}
