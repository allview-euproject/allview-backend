const db = require('../../db')

const getValidateCourses = async (userId) => {
  const data = {}
  data.pendingCourses = await db.query('SELECT users_courses.*, users.email, users.full_name, courses.title FROM users_courses INNER JOIN courses ON users_courses.course_id = courses.course_id ' +
    'INNER JOIN users ON users_courses.user_id = users.user_id ' +
    'WHERE users_courses.deleted_at IS NULL AND courses.user_id = ? AND status_id = 1', userId)
  data.rejectedCourses = await db.query('SELECT users_courses.*, users.email, users.full_name, courses.title FROM users_courses LEFT JOIN courses ON users_courses.course_id = courses.course_id ' +
    'JOIN users ON users_courses.user_id = users.user_id WHERE users_courses.deleted_at IS NULL AND status_id = 3 AND courses.user_id = ?', userId)
  data.acceptedCourses = await db.query('SELECT users_courses.*, users.email, users.full_name, courses.title FROM users_courses LEFT JOIN courses ON users_courses.course_id = courses.course_id ' +
    'JOIN users ON users_courses.user_id = users.user_id WHERE users_courses.deleted_at IS NULL AND status_id = 2 AND courses.user_id = ?', userId)

  // Status 1: pending
  // Status 2: accepted
  // Status 3: rejected
  return data
}

const acceptRate = async (rateid) => {
  const coursesRatesAccepted = await db.query('UPDATE users_courses SET status_id = 2 WHERE users_courses_id = ?', rateid)
  return coursesRatesAccepted
}
const rejectRate = async (rateid) => {
  const coursesRatesAccepted = await db.query('UPDATE users_courses SET status_id = 3 WHERE users_courses_id = ?', rateid)
  return coursesRatesAccepted
}

module.exports = {
  getValidateCourses, acceptRate, rejectRate
}
