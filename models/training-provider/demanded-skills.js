const db = require('../../db')

// Gets the top 50 demanded skills
const getDemandedSkills = async () => {
  // const skills = await db.query('SELECT jobs_skills.skill_id, count(*) AS count, skill_name FROM jobs_skills INNER JOIN skills ' +
  //   'ON jobs_skills.skill_id = skills.skill_id GROUP BY jobs_skills.skill_id ORDER BY count DESC LIMIT 50')
  const skills = await db.query('SELECT count(job_id) as count, skill_name FROM jobs_skills WHERE skill_name <> "-" GROUP BY skill_name')
  return skills
}

const getDemandedProfesionalExperience = async () => {
  // const skills = await db.query('SELECT jobs_skills.skill_id, count(*) AS count, skill_name FROM jobs_skills INNER JOIN skills ' +
  //   'ON jobs_skills.skill_id = skills.skill_id GROUP BY jobs_skills.skill_id ORDER BY count DESC LIMIT 50')
  const skills = await db.query('SELECT count(job_id) as count, skill_name FROM jobs_skills WHERE skill_name <> "-" GROUP BY skill_name')
  return skills
}

const getDemandedCompetences = async () => {
  // const skills = await db.query('SELECT jobs_skills.skill_id, count(*) AS count, skill_name FROM jobs_skills INNER JOIN skills ' +
  //   'ON jobs_skills.skill_id = skills.skill_id GROUP BY jobs_skills.skill_id ORDER BY count DESC LIMIT 50')
  const skills = await db.query('SELECT count(job_id) as count, skill_name FROM jobs_skills WHERE skill_name <> "-" GROUP BY skill_name')
  return skills
}

module.exports = {
  getDemandedSkills,
  getDemandedProfesionalExperience,
  getDemandedCompetences
}
