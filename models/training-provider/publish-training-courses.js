const utils = require('../../utils')
const db = require('../../db')
const fs = require('fs')
const path = require('path')
const { countReset } = require('console')

const getPublishTrainingCourses = async (userId) => {
  const data = {}

  // Data used in the selectors
  const partners = await db.query('SELECT partner FROM courses_partners')
  data.partners = []
  for (const partner of partners) {
    data.partners.push(partner.partner)
  }

  const topics = await db.query('SELECT topic FROM topics')
  data.topics = []
  for (const topic of topics) {
    data.topics.push(topic.topic)
  }

  const types = await db.query('SELECT type FROM courses_types')
  data.types = []
  for (const type of types) {
    data.types.push(type.type)
  }

  const languages = await db.query('SELECT language FROM courses_languages')
  data.languages = []
  for (const language of languages) {
    data.languages.push(language.language)
  }

  const licenses = await db.query('SELECT license FROM courses_licenses')
  data.licenses = []
  for (const license of licenses) {
    data.licenses.push(license.license)
  }

  const categories = await db.query('SELECT category FROM courses_categories')
  data.categories = []
  for (const category of categories) {
    data.categories.push(category.category)
  }

  const relevances = await db.query('SELECT relevance FROM courses_relevances')
  data.relevances = []
  for (const relevance of relevances) {
    data.relevances.push(relevance.relevance)
  }

  const skills = await db.query('SELECT distinct(skill_name) FROM skills WHERE skill_type_id = 1 ORDER BY skill_name ')
  data.skills = []
  for (const skill of skills) {
    data.skills.push(skill.skill_name)
  }

  const competences = await db.query('SELECT distinct(skill_name) FROM skills WHERE skill_type_id = 2 ORDER BY skill_name')
  data.competences = []
  for (const competence of competences) {
    data.competences.push(competence.skill_name)
  }


  // Saved course data
  data.courses = await db.query('SELECT * FROM courses ' +
    'LEFT JOIN courses_languages ON language_id = course_language_id ' +
    'LEFT JOIN courses_categories ON course_category_id = category_id ' +
    'LEFT JOIN courses_partners ON partner_id = course_partner_id ' +
    'LEFT JOIN courses_licenses ON license_id = course_license_id ' +
    'LEFT JOIN courses_types ON type_id = course_type_id ' +
    'WHERE user_id = ?', userId)

  //'LEFT JOIN courses_relevances ON relevance_id = course_relevance_id ' +


  for (const i in data.courses) {
    data.courses[i].topicsSelected = []
    const topicsSelected = await db.query('SELECT * FROM courses_topics INNER JOIN topics ON topics.topic_id = courses_topics.topic_id WHERE course_id = ?', data.courses[i].course_id)
    for (const topic of topicsSelected) {
      data.courses[i].topicsSelected.push(topic.topic)
    }
  }

  
  for (const i in data.courses) {
    data.courses[i].competences = []
    //console.log("SELECT skill_name FROM skills WHERE skill_id = (SELECT courses_competences.skill_id FROM courses_competences WHERE course_id = " + data.courses[i].course_id + ") ")
    const competencesSelected = await db.query("SELECT skill_name FROM skills WHERE skill_id IN (SELECT courses_competences.skill_id FROM courses_competences WHERE course_id = " + data.courses[i].course_id + ") ORDER BY skill_name")
    console.log(competencesSelected)
    data.courses[i].competences = []
    for (const compentence of competencesSelected) {
      data.courses[i].competences.push(compentence.skill_name)
    }

  }

  for (const i in data.courses) {
    data.courses[i].skills = []
    console.log("SELECT skill_name FROM skills WHERE skill_id = (SELECT courses_skills.skill_id FROM courses_skills WHERE course_id = " + data.courses[i].course_id + ")")
    const skillsSelected = await db.query("SELECT skill_name FROM skills WHERE skill_id IN (SELECT courses_skills.skill_id FROM courses_skills WHERE course_id = " + data.courses[i].course_id + ") ORDER BY skill_name")
    data.courses[i].skills = []
    for (const skill of skillsSelected) {
      data.courses[i].skills.push(skill.skill_name)
    }

  }

 //console.log("getPublishTrainingCourses: "+JSON.stringify(data))
  return data
}

const uploadCourseImage = async (filename, courseId) => {
  if (filename) {

    const randomImageName = utils.randomRame(10)
    try {
      let imageExtension = filename.split('.')
      imageExtension = imageExtension[imageExtension.length - 1]

      const oldPath = path.join(__dirname, '../../public/tmp/' + filename)
      const newPath = path.join(__dirname, '../../public/images/courses/' + randomImageName + '.' + imageExtension)

      fs.rename(oldPath, newPath, function (error) {
        if (error) console.log(error)
        console.log('Image moved and renamed')
      })

      await db.query('UPDATE courses SET image = ? WHERE course_id = ?', [randomImageName + '.' + imageExtension, courseId])
      const data = {};
      data.filename = randomImageName + '.' + imageExtension;
      return data;
    } catch (error) {
      throw new Error('Error updating image: ' + error.message)
    }
  }else{
    // removing image
    try {
    // await db.query('UPDATE courses SET image = ? WHERE course_id = ?', [null, courseId])

      /// esta borrando
      const images = await db.query('SELECT image FROM courses WHERE course_id = ?',[courseId])
      for (const image of images) {
        const newPath = path.join(__dirname, '../../public/images/courses/' + image.image)
        console.log("Eliminar imagen ",newPath)
        if(fs.existsSync(newPath)) {
          fs.unlinkSync(newPath);
        }else{
          console.log("No existe imagen ",newPath)
        }
      }
      await db.query('UPDATE courses SET image = ? WHERE course_id = ?', [null, courseId])
      const data = {
        image_deleted: true
      };
      return data;
    } catch (error) {
      throw new Error('Error removing image: ' + error.message)
    }
  }
}

const updateCourses = async (userId, course) => {

  //console.log("EL CURSO ES: " + JSON.stringify(course))

  if (!course.title) {
    throw new Error('Please set a title')
  }

  if (!course.partner) {
    throw new Error('Please set the partner name')
  }

  if (!course.keywords) {
    throw new Error('Please set the course keywords')
  }

  if(!course.init_date)
    course.init_date = null
  if(!course.end_date)
    course.end_date = null

  if (course.expired === undefined){
    course.expired = false;
  }
  if (course.manual_publish === undefined){
    course.manual_publish = false;
  }
  if (course.publish_ini === undefined){
    course.publish_ini = null;
  }
  if (course.publish_end === undefined){
    course.publish_end = null;
  }

  // if (!course.category) {
  //   throw new Error('Please set the course category')
  // }

  if (!course.language) {
    throw new Error('Please set the course language')
  }

  let partnerId
  if (course.partner) {
    const partnerIdSearch = await db.query('SELECT course_partner_id FROM courses_partners WHERE partner = ?', course.partner)
    if (partnerIdSearch.length > 0) {
      partnerId = partnerIdSearch[0].course_partner_id
    } else {
      const insertResponse = await db.query('INSERT INTO courses_partners (partner) VALUES(?)', course.partner)
      partnerId = insertResponse.insertId
    }
  }

  let categoryId
  if (course.category) {
    const categoryIdSearch = await db.query('SELECT course_category_id FROM courses_categories WHERE category = ?', course.category)
    if (categoryIdSearch.length > 0) {
      categoryId = categoryIdSearch[0].course_category_id
    } else {
      const insertResponse = await db.query('INSERT INTO courses_categories (category) VALUES(?)', course.category)
      categoryId = insertResponse.insertId
    }
  }

  let licenseTypeId
  if (course.license) {
    const licenseTypeIdSearch = await db.query('SELECT course_license_id FROM courses_licenses WHERE license = ?', course.license)
    if (licenseTypeIdSearch.length > 0) {
      licenseTypeId = licenseTypeIdSearch[0].course_license_id
    } else {
      const insertResponse = await db.query('INSERT INTO courses_licenses (license) VALUES(?)', course.license)
      licenseTypeId = insertResponse.insertId
    }
  }

  let languageId = []
  if (course.language) {
    let i = 0
    for (lang in course.language){
      const languageIdSearch = await db.query('SELECT course_language_id FROM courses_languages WHERE language = ?', lang)
      if (languageIdSearch.length > 0) {
        languageId[i] = languageIdSearch[0].course_language_id
      } else {
        //const insertResponse = await db.query('INSERT INTO courses_languages (language) VALUES(?)', lang)
        //languageId[i] = insertResponse.insertId
      }
      i++
    }
  }

  // let relevanceId
  // if (course.relevance) {
  //   const relevanceIdSearch = await db.query('SELECT course_relevance_id FROM courses_relevances WHERE relevance = ?', course.relevance)
  //   if (relevanceIdSearch.length > 0) {
  //     relevanceId = relevanceIdSearch[0].course_relevance_id
  //   } else {
  //     const insertResponse = await db.query('INSERT INTO courses_relevances (relevance) VALUES(?)', course.relevance)
  //     relevanceId = insertResponse.insertId
  //   }
  // }

  let typeId
  if (course.type) {
    const typeIdSearch = await db.query('SELECT course_type_id FROM courses_types WHERE type = ?', course.type)
    if (typeIdSearch.length > 0) {
      typeId = typeIdSearch[0].course_type_id
    } else {
      const insertResponse = await db.query('INSERT INTO courses_types (type) VALUES(?)', course.type)
      typeId = insertResponse.insertId
    }
  }

  let courseId;
  if (course.manual_publish === undefined){
    course.manual_publish = false;
  }
  if (course.publish_ini === undefined){
    course.publish_ini = null;
  }
  if (course.publish_end === undefined){
    course.publish_end = null;
  }
  if(course.image && course.image.indexOf("http") !== -1){
    course.image = course.image.substring(course.image.lastIndexOf("/")+1);
  }
  if (course.course_id) {
    // update
    const userRole = await db.query('SELECT role_id FROM users WHERE user_id = ?', userId)
    // The user email must have been saved in the db
    if (userRole.length === 0) throw new Error('User not found')

    if (userRole[0].role_id === 7) {
      let res = await db.query("UPDATE courses SET image = ?,expired = ?, manual_publish = ?, publish_ini = ?, publish_end = ?, init_date = ?, end_date = ?,title = ?, duration = ?, description = ?, contact_mail = ?, weblink = ?, eqf_nqf_level = ?, ecvet_points = ?, learning_outcomes = ?, media = ?, institution = ?, " +
          "keywords = ?, access = ?, archive_name = ?, created_at = ?, partner_id = ?, type_id = ?, category_id = ?, license_id = ?, languages = ?, receive_candidates_recommendations = ? " +
          "WHERE course_id = ? ", [course.image,course.expired,course.manual_publish,course.publish_ini,course.publish_end,course.init_date, course.end_date,course.title,course.duration, course.description, course.contact_mail, course.weblink, course.eqf_nqf_level, course.ecvet_points, course.learning_outcomes, course.media, course.institution,
        course.keywords, course.access, course.archive_name, course.created_at, partnerId, typeId, categoryId, licenseTypeId, JSON.stringify(course.language), course.receive_candidates_recommendations || false, course.course_id])
console.log("update course ",res,JSON.stringify(course))
    }else{
      await db.query("UPDATE courses SET image = ?,expired = ?, manual_publish = ?, publish_ini = ?, publish_end = ?, init_date = ?, end_date = ?,title = ?, duration = ?, description = ?, contact_mail = ?, weblink = ?, eqf_nqf_level = ?, ecvet_points = ?, learning_outcomes = ?, media = ?, institution = ?, " +
          "keywords = ?, access = ?, archive_name = ?, created_at = ?, partner_id = ?, type_id = ?, category_id = ?, license_id = ?, languages = ?, receive_candidates_recommendations = ? " +
          "WHERE course_id = ? AND user_id = ?", [course.image,course.expired,course.manual_publish,course.publish_ini,course.publish_end,course.init_date, course.end_date,course.title,course.duration, course.description, course.contact_mail, course.weblink, course.eqf_nqf_level, course.ecvet_points, course.learning_outcomes, course.media, course.institution,
        course.keywords, course.access, course.archive_name, course.created_at, partnerId, typeId, categoryId, licenseTypeId, JSON.stringify(course.language), course.receive_candidates_recommendations || false, course.course_id, userId])
    }

    courseId = course.course_id;
  }else{
    // new
    // Course not created. Insert new course
    const insertResponse = await db.query("INSERT INTO courses (expired,manual_publish, publish_ini , publish_end, init_date,end_date,title,duration, description, contact_mail, weblink, eqf_nqf_level, ecvet_points, learning_outcomes, media, institution, keywords, access, archive_name, " +
        "created_at, partner_id, type_id, category_id, license_id, languages, receive_candidates_recommendations, user_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
        [course.expired,course.manual_publish,course.publish_ini,course.publish_end, course.init_date, course.end_date,course.title,course.duration, course.description, course.contact_mail, course.weblink, course.eqf_nqf_level, course.ecvet_points, course.learning_outcomes, course.media, course.institution,
          course.keywords, course.access, course.archive_name, course.created_at, partnerId, typeId, categoryId, licenseTypeId, JSON.stringify(course.language), course.receive_candidates_recommendations || false, userId])

    courseId = insertResponse.insertId
  }


  // Update topic stuff
  // Delete topics
  await db.query('DELETE FROM courses_topics WHERE course_id = ?', courseId)
  // Insert new topics
  if(course.topicsSelected) {
    for (const topic of course.topicsSelected) {
      const topicIdSearch = await db.query('SELECT topic_id FROM topics WHERE topic = ?', topic)
      await db.query('INSERT INTO courses_topics (course_id, topic_id) VALUES (?,?)', [courseId, topicIdSearch[0].topic_id])
    }
  }

  if (course.skills){
    //console.log("LAS SKILLS SELECCIONADAS SON")
    //console.log(course.skills)
    await db.query("DELETE FROM courses_skills WHERE course_id = ?",courseId)
    if(course.skills.length > 0) {
      for(let i = 0; i < course.skills.length; i++){
        console.log("INSERT INTO courses_skills (course_id, skill_id) VALUES (" + courseId + ", (SELECT skill_id FROM skills WHERE skill_name = \"" + course.skills[i] + "\" LIMIT 1))")
        await db.query("INSERT INTO courses_skills (course_id, skill_id) VALUES (" + courseId + ", (SELECT skill_id FROM skills WHERE skill_name = ? LIMIT 1))",course.skills[i])
      }
    }
  }


  if (course.competences){
    //console.log(course.competences)
    await db.query("DELETE FROM courses_competences WHERE course_id = ?" ,courseId)
    for(let i = 0; i < course.competences.length; i++){
      console.log("INSERT INTO courses_competences (course_id, skill_id) VALUES (" + courseId + ", (SELECT skill_id FROM skills WHERE skill_name = \"" + course.competences[i] + "\" LIMIT 1))")
      await db.query("INSERT INTO courses_competences (course_id, skill_id) VALUES (" + courseId + ", (SELECT skill_id FROM skills WHERE skill_name = ? LIMIT 1))", course.competences[i])
    }
  }

  return { "courseId" : courseId };
}

const deleteCourses = async (userId, courseId) => {
  await db.query('DELETE FROM courses WHERE user_id = ? AND course_id = ?', [userId, courseId])
  await db.query('DELETE FROM courses_topics WHERE course_id = ?', courseId)
}

module.exports = {
  getPublishTrainingCourses,
  uploadCourseImage,
  updateCourses,
  deleteCourses
}
