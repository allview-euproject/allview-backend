const db = require('../../db')

const getHome = async (userId) => {
  const data = {}

  // Using the IMDB weighted average method, wich is a true Bayesian estimate
  /*
   weighted rating (WR) = (v ÷ (v+m)) × R + (m ÷ (v+m)) × C
   R = average rating
   v = number of votes for the movie = (votes)
   m = minimum votes required to be listed in the Top
   C = the mean vote across the whole data
  */
  const m = 1 // We can increase the value if the number of ratings increase
  const ratingGlobalAvgData = await db.query('SELECT AVG(rating) AS rating_global_avg FROM users_courses WHERE users_courses.deleted_at IS NULL')
  const C = ratingGlobalAvgData[0].rating_global_avg

  // Get the rating average and vote count for each course
  const ratedCoursesAgg = await db.query('SELECT course_id, AVG(rating) AS rating_avg, COUNT(course_id) AS rated_times ' +
    'FROM users_courses WHERE users_courses.deleted_at IS NULL GROUP BY course_id')

  const weightedRating = []
  for (const ratedCourseAgg of ratedCoursesAgg) {
    // Skip courses with less than m votes
    if (ratedCourseAgg.rated_times < m) continue
    const R = ratedCourseAgg.rating_avg
    const v = ratedCourseAgg.rated_times
    const WR = (v / (v + m)) * R + (m / (v + m)) * C

    // Store all the valid weighted ratings
    weightedRating.push({
      course_id: ratedCourseAgg.course_id,
      weighted_rating: WR
    })
  }

  // Sort the top courses by weighted rating
  weightedRating.sort((a, b) => b.weighted_rating - a.weighted_rating)

  // Save the top four courses in the data array
  data.topRatedCourses = []
  let counter = 0
  for (const topCourse of weightedRating) {
    const course = await db.query('SELECT course_id, title, partner, image FROM courses ' +
      'INNER JOIN courses_partners ON courses.partner_id = courses_partners.course_partner_id WHERE course_id = ?', topCourse.course_id)
    data.topRatedCourses.push(course[0])
    counter++
    if (counter >= 4) break
  }

  data.recentCourses = await db.query('SELECT course_id, title, partner, image FROM courses INNER JOIN ' +
    'courses_partners ON courses.partner_id = courses_partners.course_partner_id ORDER BY created_at DESC LIMIT 4')

  /* Version alowing user duplicate
  console.log('SELECT candidate_recommendation_id, users.*, courses.title AS course_title FROM candidates_recommendations ' +
      'INNER JOIN courses ON courses.course_id = candidates_recommendations.course_id ' +
      'INNER JOIN users ON users.user_id = candidates_recommendations.candidate_id ' +
      'WHERE candidates_recommendations.course_id IN (SELECT course_id FROM courses WHERE user_id = ?) ' +
      'AND courses.receive_candidates_recommendations = 1 ' +
      'ORDER BY candidates_recommendations.score DESC LIMIT 4')

  data.recommendedCandidates = await db.query('SELECT candidate_recommendation_id, users.*, courses.title AS course_title FROM candidates_recommendations ' +
    'INNER JOIN courses ON courses.course_id = candidates_recommendations.course_id ' +
    'INNER JOIN users ON users.user_id = candidates_recommendations.candidate_id ' +
    'WHERE candidates_recommendations.course_id IN (SELECT course_id FROM courses WHERE user_id = ?) ' +
    'AND courses.receive_candidates_recommendations = 1 ' +
    'ORDER BY candidates_recommendations.score DESC LIMIT 4', userId)
*/

  console.log('SELECT\n' +
      '    DISTINCT (users.user_id) as dist,users.*,candidates_recommendations.score\n' +
      'FROM\n' +
      '    candidates_recommendations\n' +
      '    INNER JOIN courses ON courses.course_id = candidates_recommendations.course_id\n' +
      '    INNER JOIN users ON users.user_id = candidates_recommendations.candidate_id\n' +
      'WHERE\n' +
      '    candidates_recommendations.course_id IN (SELECT course_id FROM courses WHERE user_id = ?)\n' +
      '    AND courses.receive_candidates_recommendations = 1\n' +
      'ORDER BY\n' +
      '    candidates_recommendations.score DESC\n' +
      'LIMIT 4')

  data.recommendedCandidates = await db.query('SELECT\n' +
      '    DISTINCT (users.user_id) as dist,users.*,candidates_recommendations.score\n' +
      'FROM\n' +
      '    candidates_recommendations\n' +
      '    INNER JOIN courses ON courses.course_id = candidates_recommendations.course_id\n' +
      '    INNER JOIN users ON users.user_id = candidates_recommendations.candidate_id\n' +
      'WHERE\n' +
      '    candidates_recommendations.course_id IN (SELECT course_id FROM courses WHERE user_id = ?)\n' +
      '    AND courses.receive_candidates_recommendations = 1\n' +
      'ORDER BY\n' +
      '    candidates_recommendations.score DESC\n' +
      'LIMIT 4', userId)
  return data
}

module.exports = {
  getHome
}
