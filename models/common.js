require('dotenv')
const bcrypt = require('bcrypt')
const db = require('../db')
const utils = require('../utils')
const fs = require('fs')
const path = require('path')
const userProfile = require('./people/user-profile')
const offerJobs = require('./company/offer-jobs')
const publishTrainingCourses = require('./training-provider/publish-training-courses')

const searchSkillsByName = async (name) => {
  // TODO only last level
  const skillSearch = await db.query('SELECT skill_name FROM skills WHERE skill_name LIKE "%' + name + '%" ORDER BY skill_name')
  const skills = []
  for (const skill of skillSearch) {
    // We are interested only in the name
    skills.push(skill.skill_name)
  }
  return skills
}

const searchCompetencesByName = async (name) => {
  const competencesSearch = await db.query('SELECT skill_name FROM skills WHERE skill_type_id = 2 AND skill_level = 4 AND skill_name LIKE "%' + name + '%" ORDER BY skill_name')
  const competences = []
  // We are interested only in the name
  for (const competence of competencesSearch) {
    competences.push(competence.skill_name)
  }
  return competences
}

const searchExperiencesByName = async (name) => {
  const experiencesSearch = await db.query('SELECT occupation_name, occupation_level FROM occupations WHERE occupation_is_last_level = 1 AND occupation_name LIKE "%' + name + '%" ORDER BY occupation_name')
  const experiences = []
  // We are interested only in the name
  for (const competence of experiencesSearch) {
    experiences.push(competence.occupation_name)
  }
  return experiences
}

const searchChallengesByName = async (name) => {
  const challengesSearch = await db.query('SELECT occupation_name FROM occupations WHERE occupation_is_last_level = 1 AND occupation_name LIKE "%' + name + '%" ORDER BY occupation_name')
  const challenges = []
  // We are interested only in the name
  for (const challenge of challengesSearch) {
    challenges.push(challenge.occupation_name)
  }
  return challenges
}


const searchCoursesByName = async (name,limit,offset) => {
  let query = 'SELECT title FROM courses WHERE  title LIKE "%' + name + '%" ORDER BY title';


  if(limit !== undefined)
    query +=" LIMIT "+limit+" "

  if(offset !== undefined)
    query +=" OFFSET "+offset+" "

  const coursesSearch = await db.query(query)
  const courses = []
  // We are interested only in the name
  for (const course of coursesSearch) {
    courses.push(course.title)
  }
  return courses
}


const getCountries = async () => {
  const countriesSearch = await db.query('SELECT nicename FROM countries ORDER BY nicename')
  const countries = []
  // We are interested only in the name
  for (const country of countriesSearch) {
    countries.push(country.nicename)
  }
  return countries
}

const getSearchCourses = async (userId,limit,offset) => {
  const types = await db.query('SELECT * FROM courses_types ORDER BY type')
  const topics = await db.query('SELECT * FROM topics  ORDER BY topic')
  const partners = await db.query('SELECT * FROM courses_partners  ORDER BY partner')
  const languages = await db.query('SELECT * FROM courses_languages  ORDER BY language')
  let query = 'SELECT  courses.course_id, courses.init_date,courses.end_date, category_id, category ,title,description,learning_outcomes, keywords, ' +
      'courses_types.course_type_id as course_type_id,courses_types.type as course_type_name, access,' +
      'license_id,institution,contact_mail, weblink,language_id,languages,created_at,relevance_id,eqf_nqf_level,' +
      'ecvet_points,archive_name,media,courses.user_id,partner_id, updated_at,image,cost,rating,duration,' +
      'receive_candidates_recommendations,course_partner_id,partner,license as license_name, '+
      'true as fav_id '+
      'FROM courses '+
      'INNER JOIN courses_partners ON courses_partners.course_partner_id = courses.partner_id '+
      'LEFT JOIN courses_licenses ON courses_licenses.course_license_id = courses.license_id ' +
      'LEFT JOIN courses_categories ON courses_categories.course_category_id = courses.category_id ' +
      'LEFT JOIN courses_types ON courses_types.course_type_id = courses.type_id  '+
      'WHERE courses.course_id IN (SELECT course_id FROM courses_users_favorites WHERE user_id = ?) ';

  if(limit !== undefined)
    query +=" LIMIT "+limit+" "

  if(offset !== undefined)
    query +=" OFFSET "+offset+" "


  //console.log(query)
  const courses = await db.query(
      query, userId)
  console.log(query)
  return {
    types: types,
    topics: topics,
    partners: partners,
    languages: languages,
    courses: courses
  }
}

const searchCourses = async (userId,keywords,eqf_levels,free_or_paid, languages, minduration, maxduration, competences, types, topics, entities, skills, order,limit,offset) => {
  let query = 'SELECT ' +
      ' courses.course_id, courses.init_date,courses.end_date, category_id, category ,title,description,learning_outcomes, keywords, ' +
      ' courses_types.course_type_id as course_type_id,courses_types.type as course_type_name, access,license_id,institution,contact_mail,' +
      ' weblink,language_id,languages,created_at,relevance_id,eqf_nqf_level,ecvet_points,archive_name,media,courses.user_id,partner_id,' +
      ' updated_at,image,cost,rating,duration,receive_candidates_recommendations,course_partner_id,partner,license as license_name' +
      ', courses_users_favorites.course_user_favorite_id as fav_id '+
      ' FROM courses INNER JOIN courses_partners ON courses_partners.course_partner_id = courses.partner_id ' +
      ' LEFT JOIN courses_types ON courses_types.course_type_id = courses.type_id ' +
      ' LEFT JOIN courses_users_favorites ON courses_users_favorites.course_id = courses.course_id and courses_users_favorites.user_id = ? ' +
      ' LEFT JOIN courses_categories ON courses_categories.course_category_id = courses.category_id '+
      ' LEFT JOIN courses_licenses ON courses_licenses.course_license_id = courses.license_id  WHERE '

  query += " (\n courses.deleted_at IS NULL AND " +
      "\t\tcourses.expired = FALSE AND courses.manual_publish = TRUE\n" +
      "\t\tOR ( \n" +
      "    \t\t(courses.publish_ini IS NULL OR CURRENT_TIMESTAMP > courses.publish_ini) \n" +
      "    \t\tAND \n" +
      "    \t\t(courses.publish_end IS NULL OR CURRENT_TIMESTAMP < courses.publish_end) \n" +
      "    \t\tAND \n" +
      "    \t\t(courses.end_date IS NULL OR CURRENT_TIMESTAMP < courses.end_date) \n" +
      "\t\t)) AND ";

  // if (keywords != null) {
  //   query = query + '(keywords LIKE \'%' + keywords + '%\' OR title LIKE \'%' + keywords + '%\') AND '
  // }

  if (keywords != null && keywords.length > 0) {
    console.log("keywords",JSON.stringify(keywords))
    let keywords_words = keywords.toString().split(',')
    console.log("keywords",JSON.stringify(keywords_words))
    keywords_words = keywords_words.map(s => s.trim());

    const keywords_query = keywords_words.map(s => `( LOWER(keywords) LIKE LOWER('%${s}%') OR LOWER(title) LIKE LOWER('%${s}%'))`).join(" OR ");
    query = query+"("+ keywords_query +") AND ";
  }

  if(eqf_levels != null && eqf_levels.length > 0){
    const eqf_query = eqf_levels.map(s => `( LOWER(eqf_nqf_level) LIKE LOWER('%${s}%') OR LOWER(eqf_nqf_level) LIKE LOWER('%${s}%'))`).join(" OR ");
    query = query+"("+ eqf_query +") AND ";
  }

  if(free_or_paid != null){
    if(free_or_paid.toUpperCase() === 'FREE')
      query = query + '(access = "Free") AND '
    else if(free_or_paid.toUpperCase() === 'PAID')
      query = query + '(access = "Paid") AND '
  }

  if (competences != null && competences.length !== 0) {
    const competences_query = competences.map(s => `'${s}'`).join(", ");
    query = query + ' courses.course_id IN (SELECT course_id FROM courses_competences WHERE skill_id IN (SELECT skill_id FROM skills WHERE skill_name IN ( ' + competences_query + ' ))) AND '
  }

  if (types != null && types.length !== 0) {
    const types_query = types.map(s => `'${s}'`).join(", ");
    query = query + 'type_id IN (SELECT course_type_id FROM courses_types WHERE type IN('+types_query+ ')) AND '
  }

  if (topics != null && topics.length !== 0) {
    const topics_query = topics.map(s => `'${s}'`).join(", ");
    query = query + 'courses.course_id IN ( SELECT course_id FROM courses_topics WHERE topic_id IN ( SELECT topic_id FROM topics WHERE topic IN ('+topics_query+ '))) AND '
  }

  if (languages != null && languages.length !== 0) {
    const languages_query = languages.map(s => `LOWER(courses.languages) LIKE LOWER('%${s}%')`).join(" OR ");
    query = query+"("+ languages_query +") AND ";
  }

  if (entities != null && entities.length !== 0) {
    const entities_query = entities.map(s => `'${s}'`).join(", ");
    query = query + 'partner_id IN (SELECT course_partner_id FROM courses_partners WHERE partner IN('+entities_query+')) AND '
  }

  if (skills != null && skills.length > 0) {
    let skills_query = skills.map(s => `'${s}'`).join(", ");
    query = query + 'courses.course_id IN (SELECT course_id FROM courses_skills WHERE skill_id IN (SELECT skill_id FROM skills WHERE skill_name IN (' + skills_query + '))) AND '
  }

  if (minduration != null && maxduration != null) {
    query = query + 'duration BETWEEN ' + minduration + ' AND ' + maxduration + ' AND'
  }

  query = query + ' 1 ORDER BY ' + order
  if(limit !== undefined)
    query +=" LIMIT "+limit+" "

  if(offset !== undefined)
    query +=" OFFSET "+offset+" "

  // console.log("limit",limit)
  // console.log("offset",offset)
   console.log("courses",query)
  const courses = await db.query(query,userId)
  return courses
}

const deleteFavCourses = async (courseId, userId) => {
  await db.query('DELETE FROM courses_users_favorites WHERE course_id = ? AND user_id = ?', [courseId, userId])
}

const addFavCourses = async (courseId, userId) => {
  try {
    await db.query('INSERT INTO courses_users_favorites (user_id, course_id) VALUES(?,?)', [userId, courseId])
  } catch (error) {
    if (error.message.includes('ER_DUP_ENTRY')) {
      throw new Error('alreadySaved')
    } else {
      throw new Error(error.message)
    }
  }
}

const deleteFavJobs = async (jobId, userId) => {
  await db.query('DELETE FROM jobs_users_favorites WHERE job_id = ? AND user_id = ?', [jobId, userId])
}

const addFavJobs = async (jobId, userId) => {
  try {
    await db.query('INSERT INTO jobs_users_favorites (user_id, job_id) VALUES(?,?)', [userId, jobId])
  } catch (error) {
    if (error.message.includes('ER_DUP_ENTRY')) {
      throw new Error('alreadySaved')
    } else {
      throw new Error(error.message)
    }
  }
}

const getRateCourses = async (userId) => {
  const courses = await db.query('SELECT course_id, title, image FROM courses ORDER BY title')
  const coursesBeingRated = await db.query('SELECT c.title, c.image, u.* FROM courses c, users_courses u WHERE u.deleted_at IS NULL AND c.course_id = u.course_id AND u.user_id = ? ORDER BY c.title', userId)
  for (const courseBeingRated of coursesBeingRated) {
    for (const i in courses) {
      if (courseBeingRated.course_id === courses[i].course_id) {
        courses[i].status_id = courseBeingRated.status_id
      }
    }
  }
  return courses
}

const rateCourse = async (userId, courseId, rating, comment, reason) => {
  // Check if the course was already rated by the same user
  const alreadyRated = await db.query('SELECT users_courses_id FROM users_courses WHERE users_courses.deleted_at IS NULL AND user_id = ? AND course_id = ?', [userId, courseId])
  if (alreadyRated.length > 0) {
    throw new Error('The course was already rated')
  }

  // Save null if there is no comment/reason since they are optional fields
  await db.query('INSERT INTO users_courses (user_id, course_id, rating, comment, reason) VALUES (?,?,?,?,?)', [userId, courseId, rating, comment, reason])
}

const getCourse = async (userId,courseId) => {
  //let course = await db.query('SELECT * FROM courses WHERE course_id = ?', courseId)

  let query = 'SELECT courses.course_id,courses.init_date,courses.end_date, category_id, category ,title,description,learning_outcomes, keywords, ' +
      ' courses_types.course_type_id as course_type_id,courses_types.type as course_type_name, access,license_id,institution,contact_mail,' +
      ' weblink,language_id,languages,created_at,relevance_id,eqf_nqf_level,ecvet_points,archive_name,media,courses.user_id,partner_id,' +
      ' updated_at,image,cost,rating,duration,receive_candidates_recommendations,course_partner_id,partner,license as license_name' +
      ' , manual_publish,publish_ini,publish_end' +
      (userId ?', courses_users_favorites.course_user_favorite_id as fav_id ':'')+
      ' FROM courses INNER JOIN courses_partners ON courses_partners.course_partner_id = courses.partner_id ' +
      ' LEFT JOIN courses_types ON courses_types.course_type_id = courses.type_id ' +
      ' LEFT JOIN courses_categories ON courses_categories.course_category_id = courses.category_id ' +
      (userId ? ' LEFT JOIN courses_users_favorites ON courses_users_favorites.course_id = courses.course_id and courses_users_favorites.user_id = ? ':'')+
      ' LEFT JOIN courses_licenses ON courses_licenses.course_license_id = courses.license_id  WHERE courses.deleted_at is null AND courses.course_id = ? AND (courses.expired = FALSE AND courses.manual_publish = TRUE ' +
      'OR ( ' +
      '    (publish_ini IS NULL OR CURRENT_TIMESTAMP > publish_ini) \n' +
      '    AND \n' +
      '    (publish_end IS NULL OR CURRENT_TIMESTAMP < publish_end) \n' +
      '    AND \n' +
      '    (end_date IS NULL OR CURRENT_TIMESTAMP < end_date) \n' +
      '))'

  //console.log(query)
  let course = await db.query(query, userId?[userId,courseId]:[courseId])

  course = course[0]
  course.test = "test"

  //console.log("course",course)
  // if(!userId){
  //
  // }
  if(course.deleted_at != null){
    throw new Error('This course has been removed.')
  }
  if(!shouldDisplayByPublish(course)){
    throw new Error('You can\'t see this course at this moment.')
  }else {

    let skills = await db.query('SELECT * FROM skills WHERE skill_id IN (SELECT courses_skills.skill_id FROM courses_skills WHERE course_id = ?)', courseId)
    course.skills = skills[0]

    let competences = await db.query('SELECT * FROM skills WHERE skill_id IN (SELECT courses_competences.skill_id FROM courses_competences WHERE course_id = ?)', courseId)
    course.competences = competences[0]
    // Rating
    let rating = await db.query('SELECT AVG(rating) AS rating FROM users_courses WHERE users_courses.deleted_at IS NULL AND course_id = ?', courseId)
    rating = rating[0].rating
    // Override default rating
    course.rating = rating

    if (course.Category) {
      course = {...course, ...(await db.query('SELECT * FROM courses_categories WHERE category_id = ?', course.Category))[0]}
    }

    // Topics are now in a different table
    const coursesTopicsArray = []
    const topics = await db.query('SELECT * FROM courses_topics INNER JOIN topics ON topics.topic_id = courses_topics.topic_id WHERE course_id = ?', courseId)
    for (const topic of topics) {
      coursesTopicsArray.push(topic.topic)
    }
    if (coursesTopicsArray.length > 0) {
      course.courses_topic = coursesTopicsArray.join(', ')
    }

    if (course.license_id) {
      course = {...course, ...(await db.query('SELECT * FROM courses_licenses WHERE course_license_id = ?', course.license_id))[0]}
    }

    if (course.language_id) {
      course = {...course, ...(await db.query('SELECT * FROM courses_languages WHERE course_language_id = ?', course.language_id))[0]}
    }

    if (course.relevance_id) {
      course = {...course, ...(await db.query('SELECT * FROM courses_relevances WHERE course_relevance_id = ?', course.relevance_id))[0]}
    }

    if (course.partner_id) {
      course = {...course, ...(await db.query('SELECT * FROM courses_partners WHERE course_partner_id = ?', course.partner_id))[0]}
    }

    if (course.type_id) {
      course = {...course, ...(await db.query('SELECT * FROM courses_types WHERE course_type_id = ?', course.type_id))[0]}
    }

    //console.log("EL CURSO QUE RECIBO ES: " + JSON.stringify(course))

    return course
  }
}

const updatePassword = async (email, newmail, password, passwordCheck) => {
  // Check if all the fields are present
  if (!password || !passwordCheck) {
    throw new Error('Missing fields')
  }

  // Check if both passwords are the same
  if (password !== passwordCheck) {
    throw new Error('Passwords do not match')
  }

  // Check if the password has at least 8 characters with numbers and letters
  utils.isPasswordSecure(password)

  // Hash with the Bcrypt algorithm using a bcrypt cost of 12 (2^12 salting rounds)
  // Bcrypt cost should be modified to take 250ms
  console.time('hash')
  const passwordHash = await bcrypt.hash(password, 12)
  console.timeEnd('hash')

  // Update the new password
  await db.query('UPDATE users SET password_hash = ?, email=? WHERE email = ?', [passwordHash, newmail, email])
}

// Delete user account with all its saved data
const deleteAccount = async (userId) => {
  // TODO Set cascade relationships in all the main tables
  await db.query('DELETE FROM users WHERE user_id = ?', userId)
  await db.query('DELETE FROM courses WHERE user_id = ?', userId)

  // Delete all the jobs published by the user
  await offerJobs.updateJobs(userId, [])

  // Delete all the courses published by the user
  const courses = await db.query('SELECT course_id FROM courses WHERE user_id = ?', userId)
  for (const course of courses) {
    await publishTrainingCourses.deleteCourses(userId, course.course_id)
  }

  // Delete all user data
  await db.query('DELETE FROM users_occupations WHERE user_id = ?', userId)
  await db.query('DELETE FROM users_competences WHERE user_id = ?', userId)
  await db.query('DELETE FROM users_courses WHERE user_id = ?', userId)
  await db.query('DELETE FROM users_educations WHERE user_id = ?', userId)
  await db.query('DELETE FROM users_experiences WHERE user_id = ?', userId)
  await db.query('DELETE FROM users_skills WHERE user_id = ?', userId)
  // TODO Remove all its files as well (cv, images, etc)
}

  const shouldDisplayByPublish = (job_course) => {
  if(job_course.expired)
    return false
  const now = new Date(); // Fecha y hora actuales

   // console.log("shouldDisplayJob>", job_course.manual_publish)
  // Condición de publicación manual
  if (job_course.manual_publish === 1) {
    //console.log("shouldDisplayJob response>", true)
    return true;
  }else if(job_course.end_date != null && now > new Date(job_course.end_date)){
    // caducado por end_date
    return false;
  }
  else if( job_course.publish_ini === null && job_course.publish_end === null){
    // ni a mano ni hay fechas
    return false
  }

  // Condición de fechas
  const publishIniPassed = job_course.publish_ini === null || now > new Date(job_course.publish_ini);
  const publishEndNotPassed = job_course.publish_end === null || now < new Date(job_course.publish_end);
  return publishIniPassed && publishEndNotPassed;
}

const uploadProfileImage = async (filename, userId) => {
  if(filename) {
    const randomImageName = utils.randomRame(10)
    console.log(randomImageName)
    try {
      let imageExtension = filename.split('.')
      imageExtension = imageExtension[imageExtension.length - 1]
      const newImageName = randomImageName + '.' + imageExtension
      const oldPath = path.join(__dirname, '../public/tmp/' + filename)
      const newPath = path.join(__dirname, '../public/images/profiles/' + newImageName)
      fs.rename(oldPath, newPath, function (error) {
        if (error) console.log(error)
        console.log('Image moved and renamed')
      })
      await db.query('UPDATE users SET image = ? WHERE user_id = ?', [newImageName, userId])
      return newImageName
    } catch (error) {
      console.log('Error updating image: ' + error)
    }
  }else{
    try{
      await db.query('UPDATE users SET image = ? WHERE user_id = ?', [null, userId])
    } catch (error) {
      console.log('Error updating image: ' + error)
    }
  }
}

const getJobsByOptions = async (keywords, languages, competences, skills, contracts, companies, order,limit,offset) => {
  var query = 'SELECT jobs.image, jobs.job_id, jobs.name, users.full_name FROM jobs INNER JOIN users ON users.user_id = jobs.company_id WHERE '
  query += " (\n jobs.deleted_at IS NULL AND " +
      "\t\tjobs.expired = FALSE AND jobs.manual_publish = TRUE\n" +
      "\t\tOR ( \n" +
      "    \t\t(jobs.publish_ini IS NULL OR CURRENT_TIMESTAMP > jobs.publish_ini) \n" +
      "    \t\tAND \n" +
      "    \t\t(jobs.publish_end IS NULL OR CURRENT_TIMESTAMP < jobs.publish_end) \n" +
      "    \t\tAND \n" +
      "    \t\t(jobs.end_date IS NULL OR CURRENT_TIMESTAMP < jobs.end_date) \n" +
      "\t\t)) AND ";


  if (keywords != null && keywords.length > 0) {
    console.log("keywords",JSON.stringify(keywords))
    let keywords_words = keywords.toString().split(',')
    keywords_words = keywords_words.map(s => s.trim());
    const keywords_query = keywords_words.map(s => `LOWER(name) LIKE LOWER('%${s}%')`).join(" OR ");
    query = query+"("+ keywords_query +") AND ";
  }
  // if (languages != null && languages.length > 0) {
  //   const languages_query = languages.map(s => `'${s}'`).join(", ");
  //   query = query + ' language_id IN (SELECT job_language_id FROM jobs_languages WHERE language IN('+languages_query+')) AND '
  // }

  if (languages != null && languages.length !== 0) {
    const languages_query = languages.map(s => `LOWER(jobs.languages) LIKE LOWER('%${s}%')`).join(" OR ");
    query = query+"("+ languages_query +") AND ";
  }

  if (companies != null && companies.length > 0) {
    const companies_query = companies.map(s => `'${s}'`).join(", ");
    query = query + ' company_id IN (SELECT user_id FROM users WHERE full_name IN ( '+companies_query+')) AND ';
  }

  if (skills != null && skills.length > 0) {
    let skills_query = skills.map(s => `'${s}'`).join(", ");
    query = query + 'job_id IN (SELECT job_id FROM jobs_skills WHERE skill_id IN (SELECT skill_id FROM skills WHERE skill_name IN (' + skills_query + '))) AND '
  }

  if (contracts != null && contracts.length > 0) {
    let contracts_query = skills.map(s => `'${s}'`).join(", ");
    query = query + 'contract_id IN (SELECT job_contract_id FROM jobs_contracts WHERE contract IN('+contracts_query+ ')) AND '
  }
  query = query + '1 ORDER BY ' + order + ' DESC '
  if(limit !== undefined)
    query +=" LIMIT "+limit+" "

  if(offset !== undefined)
    query +=" OFFSET "+offset+" "

  console.log("jobs",query)
  const jobs = await db.query(query)
  return jobs
}

const getJobById = async (jobId) => {
  let q = "SELECT j.*, d.contract, c.full_name  FROM jobs j LEFT JOIN jobs_contracts d ON d.job_contract_id = j.contract_id  LEFT JOIN users c ON c.user_id = j.company_id   WHERE j.job_id = ?";
    console.log("q",q)

    const jobsRes = await db.query(q, jobId)
  console.log("jobsRes",jobsRes)
  if(jobsRes.length == 0){
    throw new Error('Job not found ')
  }else{

    const job = jobsRes[0]
    console.log("job",job)
    if(job.deleted_at != null){
      throw new Error('This offer has been removed.')
    }
    if(!shouldDisplayByPublish(job)){
      throw new Error('You can\'t see this offer in this moment.')
    }else{
      job.experiencesSelected = ""
      job.experiencesSelected = job.job_requirements

      if(!job.languages && job.language_id){
        job.language = (await db.query('SELECT language FROM jobs_languages WHERE job_language_id = ?', job.language_id))[0].language
        if(!job.languages){
          job.languages = JSON.stringify([job.language])
        }
      }
      console.log("1")
      //job.contract = (await db.query('SELECT contract FROM jobs_contracts WHERE job_contract_id = ?', job.contract_id))[0].contract
      console.log("111")
      const sectors = await db.query('SELECT IFNULL(t2.occupation_name, t1.occupation_name) AS occupation_name\n' +
          'FROM jobs_occupations AS t1\n' +
          'LEFT JOIN occupations AS t2 ON t1.occupation_id = t2.occupation_id \n' +
          'WHERE t1.job_id = ?', job.job_id);

      job.sector = []
      for (const sector of sectors) {
        job.sector.push(sector.occupation_name)
      }
      console.log("2z")
      const essentialSkillsSearch = await db.query('SELECT skill_name FROM jobs_skills WHERE job_id = ? AND essential = 1', job.job_id)
      job.essentialSkillsSelected = []
      for (const essentialSkill of essentialSkillsSearch) {
        job.essentialSkillsSelected.push(essentialSkill.skill_name)
      }
      console.log("3")
      const optionalSkillsSearch = await db.query('SELECT skill_name FROM jobs_skills WHERE job_id = ? AND essential = 0', job.job_id)
      job.optionalSkillsSelected = []
      for (const optionalSkill of optionalSkillsSearch) {
        job.optionalSkillsSelected.push(optionalSkill.skill_name)
      }
      console.log("4")
      const experiencesSSearch = await db.query('SELECT occupation_name FROM jobs_occupations WHERE job_id = ?', job.job_id)
      // job.experiencesSelected = []
      // for (const experience of experiencesSSearch) {
      //   job.experiencesSelected.push(experience.occupation_name)
      // }

      console.log("5")
      console.log("job",job)

      return job
    }

  }


}

const getJobById_ = async (jobId) => {
  var job = await db.query('SELECT j.*,  d.contract, c.full_name FROM jobs j,  jobs_contracts d, users c WHERE ' +
    'd.job_contract_id = j.contract_id AND c.user_id = j.company_id AND j.job_id = ?', jobId)
  const skills = await db.query('SELECT skill_name FROM skills WHERE skill_id IN (SELECT skill_id FROM jobs_skills WHERE job_id = ?)', jobId)
  const occupations = await db.query('SELECT occupation_name FROM occupations WHERE occupation_id IN(SELECT occupation_id FROM jobs_occupations WHERE job_id = ?)', jobId)
  job.forEach(async j => {
    j.skills = ''
    j.occupations = ''
    skills.forEach(skill => {
      j.skills = j.skills + skill.skill_name + ', '
    })
    j.skills = j.skills.substr(0, j.skills.length - 2)
    occupations.forEach(occupation => {
      j.occupations = j.occupations + occupation.occupation_name + ', '
    })
    j.occupations = j.occupations.substr(0, j.occupations.length - 2)
  })

  if(!job[0].languages && job[0].language_id){
    const langQ = await db.query('SELECT language FROM jobs_languages WHERE job_language_id  = ?', job[0].language_id)
    const lgs = []
    langQ.forEach(lan =>{
      lgs.push(lan.language)
    })
    job[0].languages = JSON.stringify(lgs)
  }
  return job[0]
}

const getSearchJobs = async (userId) => {
  const contracts = await db.query('SELECT * FROM jobs_contracts ORDER BY contract')
  const languages = await db.query('SELECT * FROM jobs_languages ORDER BY language')
  //const companies = await db.query('SELECT full_name FROM users WHERE role_id = 3 ORDER BY full_name')
  const companies = await db.query('SELECT full_name FROM users WHERE role_id = 3 and user_id IN (SELECT DISTINCT(company_id) from jobs) ORDER BY full_name')


  let qq = 'SELECT * FROM jobs INNER JOIN users ON users.user_id = jobs.company_id WHERE deleted_at IS NULL AND job_id IN (SELECT job_id FROM jobs_users_favorites WHERE user_id = ?) ' +
      'AND ( jobs.manual_publish = TRUE\n' +
      'OR ( \n' +
      '    (jobs.publish_ini IS NULL OR CURRENT_TIMESTAMP > jobs.publish_ini) \n' +
      '    AND \n' +
      '    (jobs.publish_end IS NULL OR CURRENT_TIMESTAMP < jobs.publish_end) \n' +
      '    AND \n' +
      '    (jobs.end_date IS NULL OR CURRENT_TIMESTAMP < jobs.end_date) \n' +
      ')) ORDER BY name';

  //console.log("job",qq)
  const jobs = await db.query(qq, userId)

  return {
    contracts: contracts,
    languages: languages,
    companies: companies,
    jobs: jobs
  }
}

// Search for users that have one or more of the selected skills AND one or more of the selected competences
// AND one or more of the selected experiences
const searchCandidates = async (skills, competences, experiences,courses) => {
  let query = 'SELECT user_id, email, full_name, description, city, state, country, phone, image, cv FROM users WHERE '
  if (skills != null && skills.length !== 0) {
    query = query + 'user_id IN (SELECT user_id FROM users_skills WHERE skill_id IN (SELECT skill_id FROM skills WHERE skill_name IN('
    skills.forEach(skill => {
      query = query + '"' + skill + '",'
    })
    query = query.substr(0, query.length - 1) + '))) AND '
  }
  if (competences != null && competences.length !== 0) {
    query = query + 'user_id IN (SELECT user_id FROM users_competences WHERE skill_id IN (SELECT skill_id FROM skills WHERE skill_name IN('
    competences.forEach(competence => {
      query = query + '"' + competence + '",'
    })
    query = query.substr(0, query.length - 1) + '))) AND '
  }
  if (experiences != null && experiences.length !== 0) { 
    query = query + 'user_id IN (SELECT user_id FROM users_experiences WHERE occupation_id IN (SELECT occupation_id FROM occupations WHERE occupation_name IN('
    experiences.forEach(experience => {
      query = query + '"' + experience + '",'
    })
    query = query.substr(0, query.length - 1) + '))) AND '
  }

  if (courses != null && courses.length !== 0) {
    query = query + 'user_id IN (SELECT user_id FROM courses_users_favorites WHERE course_id IN (SELECT course_id FROM courses WHERE title IN('

    courses.forEach(course => {
      query = query + '"' + course + '",'
    })
    query = query.substr(0, query.length - 1) + '))) AND '
  }

  query = query + ' role_id = 1 ORDER BY full_name'
  console.log(query)
  const candidatesSearch = await db.query(query)
  // Get the full information of the user
  for (const i in candidatesSearch) {
    candidatesSearch[i].userProfile = await userProfile.getUserProfile(candidatesSearch[i].user_id)
  }

  return candidatesSearch
}

/// external_resources

const deleteExternalResource = async (resource_id,userId) => {
  const user = await db.query('SELECT role_id FROM users WHERE user_id = ? ',userId)
  if(user[0]){
    if(user.role_id == 7){
      // es admin
      return await db.query('DELETE FROM external_resources WHERE id = ?', [resource_id])
    }
  }
  return await db.query('DELETE FROM external_resources WHERE id = ? AND owner = ?', [resource_id, userId])
}

const addExternalResource = async (type,name,desc,entityTable,entityId,userId,file,uri) => {
  console.log("addExternalResource",file)
  console.log("uri",uri)
  try {

    console.log("file",file)
    console.log("type",type)
    if(file != undefined && type === "File") {
      const randomImageName = utils.randomRame(10)
      console.log(randomImageName)
      try {
        let imageExtension = file.split('.')
        imageExtension = imageExtension[imageExtension.length - 1]
        const newImageName = randomImageName + '.' + imageExtension
        const oldPath = path.join(__dirname, '../public/tmp/' + file)
        const newPath = path.join(__dirname, '../public/images/resources/' + newImageName)
        fs.rename(oldPath, newPath, function (error) {
          if (error) console.log(error)
          console.log('Image moved and renamed')
        })

        uri = newImageName

      } catch (error) {
        console.log('Error updating image: ' + error)
      }
    }

    console.log("params",JSON.stringify([
      type,
      name,
      desc ? desc : null,
      entityId,
      entityTable,
      userId,
      uri
    ]))
    let val = await db.query('INSERT INTO external_resources (external_resources.type, external_resources.name,external_resources.desc,relation_id,relation_table,owner,content) VALUES(?,?,?,?,?,?,?)',
        [type,
          name,
          desc ? desc : null,
          entityId,
          entityTable,
          userId,
          uri
        ])
    return {
      id: val.inserId,
      type: type,
      name: name,
      desc: desc,
      entityId: entityId,
      entityTable: entityTable,
      userId: userId,
      uri: uri
    }
  } catch (error) {
    if (error.message.includes('ER_DUP_ENTRY')) {
      throw new Error('alreadySaved')
    } else {
      throw new Error(error.message)
    }
  }
}

const getExternalResource = async (entityTable,entityId) => {
  return await db.query('SELECT * FROM external_resources WHERE relation_table = ? AND relation_id = ? ORDER BY external_resources.type',[entityTable,entityId])
}
module.exports = {
  deleteExternalResource,
  addExternalResource,
  getExternalResource,
  searchSkillsByName,
  searchCompetencesByName,
  searchExperiencesByName,
  searchChallengesByName,
  searchCoursesByName,
  getCountries,
  getSearchCourses,
  searchCourses,
  deleteFavCourses,
  addFavCourses,
  getRateCourses,
  rateCourse,
  getCourse,
  deleteFavJobs,
  addFavJobs,
  updatePassword,
  deleteAccount,
  uploadProfileImage,
  getJobsByOptions,
  getJobById,
  getSearchJobs,
  searchCandidates
}
