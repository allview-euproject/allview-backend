const db = require('../db')
const bcrypt = require('bcrypt')
const utils = require('../utils')
const fs = require("fs");

const getRoleid = async (userId) => {
  
  const userRole = await db.query('SELECT role_id FROM users WHERE user_id = ?', userId)
  // The user email must have been saved in the db
  if (userRole.length === 0) throw new Error('User not found')

  return userRole
}

const getAllUsers = async (userId) => {
  const userRole = await getRoleid(userId)
  let users
  if (userRole[0].role_id === 7) {
    users = await db.query('SELECT * FROM users')
    //console.log(users)
    return users
  }
}


const getAllCourses = async (userId) => {
  // console.log(userId)
  const userRole = await getRoleid(userId)
   console.log(userRole[0].role_id)

if(userRole[0].role_id !== 7)
  throw new Error('Not admin R: '+userRole[0].role_id )

  const data = {}

  // Data used in the selectors
  const partners = await db.query('SELECT partner FROM courses_partners')
  data.partners = []
  for (const partner of partners) {
    data.partners.push(partner.partner)
  }

  const topics = await db.query('SELECT topic FROM topics')
  data.topics = []
  for (const topic of topics) {
    data.topics.push(topic.topic)
  }

  const types = await db.query('SELECT type FROM courses_types')
  data.types = []
  for (const type of types) {
    data.types.push(type.type)
  }

  const languages = await db.query('SELECT language FROM courses_languages')
  data.languages = []
  for (const language of languages) {
    data.languages.push(language.language)
  }

  const licenses = await db.query('SELECT license FROM courses_licenses')
  data.licenses = []
  for (const license of licenses) {
    data.licenses.push(license.license)
  }

  const categories = await db.query('SELECT category FROM courses_categories')
  data.categories = []
  for (const category of categories) {
    data.categories.push(category.category)
  }

  const relevances = await db.query('SELECT relevance FROM courses_relevances')
  data.relevances = []
  for (const relevance of relevances) {
    data.relevances.push(relevance.relevance)
  }

  const skills = await db.query('SELECT distinct(skill_name) FROM skills WHERE skill_type_id = 1 ORDER BY skill_name ')
  data.skills = []
  for (const skill of skills) {
    data.skills.push(skill.skill_name)
  }

  const competences = await db.query('SELECT distinct(skill_name) FROM skills WHERE skill_type_id = 2 ORDER BY skill_name')
  data.competences = []
  for (const competence of competences) {
    data.competences.push(competence.skill_name)
  }


  // Saved course data
  data.courses = await db.query('SELECT * FROM courses ' +
      'LEFT JOIN courses_languages ON language_id = course_language_id ' +
      'LEFT JOIN courses_categories ON course_category_id = category_id ' +
      'LEFT JOIN courses_partners ON partner_id = course_partner_id ' +
      'LEFT JOIN courses_licenses ON license_id = course_license_id ' +
      'LEFT JOIN courses_types ON type_id = course_type_id WHERE courses.deleted_at IS NULL'  )

  //'LEFT JOIN courses_relevances ON relevance_id = course_relevance_id ' +


  for (const i in data.courses) {
    data.courses[i].topicsSelected = []
    const topicsSelected = await db.query('SELECT * FROM courses_topics INNER JOIN topics ON topics.topic_id = courses_topics.topic_id WHERE course_id = ?', data.courses[i].course_id)
    for (const topic of topicsSelected) {
      data.courses[i].topicsSelected.push(topic.topic)
    }
  }


  for (const i in data.courses) {
    data.courses[i].competences = []
    //console.log("SELECT skill_name FROM skills WHERE skill_id = (SELECT courses_competences.skill_id FROM courses_competences WHERE course_id = " + data.courses[i].course_id + ") ")
    const competencesSelected = await db.query("SELECT skill_name FROM skills WHERE skill_id IN (SELECT courses_competences.skill_id FROM courses_competences WHERE course_id = " + data.courses[i].course_id + ") ORDER BY skill_name")
    // console.log(competencesSelected)
    data.courses[i].competences = []
    for (const compentence of competencesSelected) {
      data.courses[i].competences.push(compentence.skill_name)
    }

  }

  for (const i in data.courses) {
    data.courses[i].skills = []
    // console.log("SELECT skill_name FROM skills WHERE skill_id = (SELECT courses_skills.skill_id FROM courses_skills WHERE course_id = " + data.courses[i].course_id + ")")
    const skillsSelected = await db.query("SELECT skill_name FROM skills WHERE skill_id IN (SELECT courses_skills.skill_id FROM courses_skills WHERE course_id = " + data.courses[i].course_id + ") ORDER BY skill_name")
    data.courses[i].skills = []
    for (const skill of skillsSelected) {
      data.courses[i].skills.push(skill.skill_name)
    }

    const ratings = await db.query("SELECT uc.users_courses_id,uc.user_id,uc.status_id,uc.rating, uc.comment,uc.reason, u.full_name,u.email,uc.deleted_at \n" +
        "\tFROM users_courses uc\n" +
        "\tLEFT JOIN users u ON u.user_id = uc.user_id\n" +
        "\tWHERE deleted_at IS NULL AND course_id = ?",data.courses[i].course_id)

    data.courses[i].ratings = []
    for (const rating of ratings) {
      data.courses[i].ratings.push({
        id:rating.users_courses_id,
        user_id:rating.user_id,
        status_id:rating.status_id,
        rating:rating.rating,
        comment:rating.comment,
        reason:rating.reason,
        full_name:rating.full_name,
        email:rating.email,
        deleted_at:rating.deleted_at
      })
    }

  }
  //console.log("getPublishTrainingCourses: "+JSON.stringify(data))
  return data

}

const getAllJobs = async (email) => {


  const userRole = await getRoleid(email)
  let data = {}
  if (userRole[0].role_id === 7) {

    const sectors = await db.query('SELECT occupation_name FROM occupations WHERE occupation_level = 2')
    data.sectors = []
    for (const sector of sectors) {
      data.sectors.push(sector.occupation_name)
    }

    const languages = await db.query('SELECT language FROM jobs_languages')
    data.languages = []
    for (const language of languages) {
      data.languages.push(language.language)
    }

    const contracts = await db.query('SELECT contract FROM jobs_contracts')
    data.contracts = []
    for (const contract of contracts) {
      data.contracts.push(contract.contract)
    }

    const jobs = await db.query('SELECT us.full_name as  company_name ,j.* FROM jobs as j LEFT JOIN users us ON us.user_id = company_id WHERE j.deleted_at IS NULL')

    data.jobs = []
    for (const job of jobs) {
      job.experiencesSelected = ""
      job.experiencesSelected = job.job_requirements

      if(!job.languages && job.language_id){
        job.language = (await db.query('SELECT language FROM jobs_languages WHERE job_language_id = ?', job.language_id))[0].language
        if(!job.languages){
          job.languages = JSON.stringify([job.language])
        }
      }
      if(job.contract_id) {
        job.contract = (await db.query('SELECT contract FROM jobs_contracts WHERE job_contract_id = ?', job.contract_id))[0].contract
      }
      //const sectors = await db.query('SELECT occupation_name FROM jobs_occupations WHERE job_id = ?', job.job_id)
      const sectors = await db.query('SELECT IFNULL(t2.occupation_name, t1.occupation_name) AS occupation_name\n' +
          'FROM jobs_occupations AS t1\n' +
          'LEFT JOIN occupations AS t2 ON t1.occupation_id = t2.occupation_id \n' +
          'WHERE t1.job_id = ?', job.job_id);

      job.sector = []
      for (const sector of sectors) {
        job.sector.push(sector.occupation_name)
      }

      const essentialSkillsSearch = await db.query('SELECT skill_name FROM jobs_skills WHERE job_id = ? AND essential = 1', job.job_id)
      job.essentialSkillsSelected = []
      for (const essentialSkill of essentialSkillsSearch) {
        job.essentialSkillsSelected.push(essentialSkill.skill_name)
      }
      job.essentialSkillsSelectedAutocomplete = []

      const optionalSkillsSearch = await db.query('SELECT skill_name FROM jobs_skills WHERE job_id = ? AND essential = 0', job.job_id)
      job.optionalSkillsSelected = []
      for (const optionalSkill of optionalSkillsSearch) {
        job.optionalSkillsSelected.push(optionalSkill.skill_name)
      }
      job.optionalSkillsSelectedAutocomplete = []

      //const experiencesSSearch = await db.query('SELECT occupation_name FROM jobs_occupations WHERE job_id = ?', job.job_id)
      // job.experiencesSelected = []
      // for (const experience of experiencesSSearch) {
      //   job.experiencesSelected.push(experience.occupation_name)
      // }

      job.experiencesSelectedAutocomplete = []

      data.jobs.push(job)
    }

    return data
  }
}

const updatePersonalInformation = async (userId, email, fullName, role, password, passwordCheck, city, state, country, phone, description,
  companyIdentification, street, contactPerson, website, mainProduct, sector, topic) => {
    if (fullName) {
      try {
        await db.query('UPDATE users SET full_name = ? WHERE user_id = ?', [fullName, userId])
      } catch (error) {
        throw new Error('Error updating full name: ' + error.message)
      }
    }
    if (password) {
      // Check if both passwords are the same
      if (password !== passwordCheck) {
        throw new Error('Passwords do not match')
      }
      
      // Check if the password has at least 8 characters with numbers and letters
      utils.isPasswordSecure(password)
      
      // Hash with the Bcrypt algorithm using a bcrypt cost of 12 (2^12 salting rounds)
      // Bcrypt cost should be modified to take 250ms
      console.time('hash')
      const passwordHash = await bcrypt.hash(password, 12)
      console.timeEnd('hash')
      await db.query('UPDATE users SET password_hash= ? WHERE user_id= ?', [passwordHash, userId])
    }
    if (email) {
      try {
        await db.query('UPDATE users SET email = ? WHERE user_id = ?', [email, userId])
      } catch (error) {
        throw new Error('Error updating email: ' + error.message)
      }
    }
    if (role) {
      try {
        await db.query('UPDATE users SET role_id = (SELECT role_id FROM roles WHERE role = ? ) WHERE user_id = ?', [role, userId])
      } catch (error) {
        throw new Error('Error updating role: ' + error.message)
      }
    }
    if (city) {
      try {
        await db.query('UPDATE users SET city = ? WHERE user_id = ?', [city, userId])
      } catch (error) {
        throw new Error('Error updating city: ' + error.message)
      }
    }
    if (state) {
      try {
        await db.query('UPDATE users SET state = ? WHERE user_id = ?', [state, userId])
      } catch (error) {
        throw new Error('Error updating state: ' + error.message)
      }
    }
    if (country) {
      try {
        await db.query('UPDATE users SET country = ? WHERE user_id = ?', [country, userId])
      } catch (error) {
        throw new Error('Error updating country: ' + error.message)
      }
    }
    if (phone) {
      //SEGÚN LA ITU E.164 LA LONGITUD MÁXIMA DE UN NÚMERO DE TELÉFONO ES DE 15 CARACTERES
      // A phone number should have 15 digits max
      if (phone.match(/\d/g).length > 15) throw new Error('Error updating phone: the number is too long')
      // A phone number should have 8 digits min
      if (phone.match(/\d/g).length < 8) throw new Error('Error updating phone: the number is too short')
      try {
        await db.query('UPDATE users SET phone = ? WHERE user_id = ?', [phone, userId])
      } catch (error) {
        throw new Error('Error updating phone: ' + error.message)
      }
    }
    if (description) {
      try {
        await db.query('UPDATE users SET description = ? WHERE user_id = ?', [description, userId])
      } catch (error) {
        throw new Error('Error updating description: ' + error.message)
      }
    }
    if (company_identification) {
      try {
        await db.query('UPDATE users SET company_identification = ? WHERE user_id = ?', [companyIdentification, userId])
      } catch (error) {
        throw new Error('Error updating company identification: ' + error.message)
      }
    }
    if (street) {
      try {
        await db.query('UPDATE users SET street = ? WHERE user_id = ?', [street, userId])
      } catch (error) {
        throw new Error('Error updating street: ' + error.message)
      }
    }
    if (contactPerson) {
      try {
        await db.query('UPDATE users SET contact_person = ? WHERE user_id = ?', [contactPerson, userId])
      } catch (error) {
        throw new Error('Error updating contact person: ' + error.message)
      }
    }
    if (website) {
      try {
        await db.query('UPDATE users SET website = ? WHERE user_id = ?', [website, userId])
      } catch (error) {
        throw new Error('Error updating website: ' + error.message)
      }
    }
    if (mainProduct) {
      try {
        await db.query('UPDATE users SET main_product = ? WHERE user_id = ?', [mainProduct, userId])
      } catch (error) {
        throw new Error('Error updating main product: ' + error.message)
      }
    }
    if (sector) {
      try {
        await db.query('UPDATE users SET sector_id = (SELECT occupation_id FROM occupations WHERE occupation_level=2 AND occupation_name= ? ) WHERE user_id = ?', [sector, userId])
      } catch (error) {
        throw new Error('Error updating sector: ' + error.message)
      }
    }
    if (topic) {
      try {
        await db.query('UPDATE users SET topic = (SELECT courses_topic_id FROM courses_topic WHERE courses_topic_name= ? ) WHERE user_id = ?', [topic, userId])
      } catch (error) {
        throw new Error('Error updating topic: ' + error.message)
      }
    }
  }

  //EL UPDATE JOB TENEMOS QUE REVISARLO. NO ESTOY SEGURO DE QUE DESDEDE BACKOFFICE PODAMOS MODIFICAR ALGO MÁS QUE ACTIVAR O DESACTIVAR....
  const updateJobInformation = async (jobj, userId) => {
    console.log(JSON.stringify(jobj))
    const userRole = await getRoleid(userId)
    /*
    console.log("UPDATE jobs SET image = '" + jobj.image 
    + "', description = '" + jobj.description
    + "', name = '" + jobj.name 
    + "', contact_details = '" + jobj.contact_details 
    + "', salary = " + jobj.salary 
    + ", language_id = " + jobj.language_id 
    + ", contract_id = " + jobj.contract_id 
    + " WHERE job_id = " + jobj.job_id)

     */
    if (userRole[0].role_id === 7) {
      //
      const query = "UPDATE jobs SET  description = ?, name = ?, contact_details = ?, salary = ?, language_id = ?, contract_id = ?, receive_candidates_recommendations = ? WHERE job_id = ?"
      await db.query(query,[jobj.description,jobj.name,jobj.contact_details,jobj.salary,jobj.language_id,jobj.contract_id,jobj.receive_candidates_recommendations,jobj.job_id])
    }
  }

  const updateCourseInformation = async (course, userId) => {
    const userRole = await getRoleid(userId)


    if (userRole[0].role_id === 7) {
      console.log("updateCourseInformation:" + JSON.stringify(course))
/*
      const query = "UPDATE courses SET category_id  = " +  course.category_id + ", "
          + "title = '" + course.title + "', "
          + "description  = '" +  course.description + "', "
          + "learning_outcomes  = '" +  course.learning_outcomes + "', "
          + "keywords  = '" +  course.keywords + "', "
          + "type_id  = " +  course.type_id + ", "
          + "license_id  = " +  course.license_id + ", "
          + "institution  = '" +  course.institution + "', "
          + "weblink  = '" +  course.weblink + "', "
          + "language_id  = " +  course.language_id + ", "
          + "relevance_id  = " +  course.relevance_id + ", "
          + "eqf_nqf_level  = '" +  course.eqf_nqf_level + "', "
          + "ecvet_points  = '" +  course.ecvet_points + "', "
          + "archive_name  = '" +  course.archive_name + "', "
          + "media  = '" +  course.media + "', "
          + "user_id  = " +  course.user_id + ", "
          + "partner_id  = " +  course.partner_id + ", "
          + "updated_at  = NOW(), "
          + "image  = '" +  course.image + "', "
          + "cost  = " +  course.cost + ", "
          + "rating  = " +  course.rating + ", "
          + "duration  = '" +  course.duration + "', "
          + "receive_candidates_recommendations  = '" +  course.receive_candidates_recommendations + "'"
          + " WHERE course_id = " + course.course_id
*/

      const query = "UPDATE courses SET category_id  = " +  course.category_id + ", "
          + "title = ?, "
          + "description  = ?, "
          + "learning_outcomes  = ?, "
          + "keywords  = ?, "
          + "type_id  = ?, "
          + "license_id  = ?, "
          + "institution  = ?, "
          + "weblink  = ?, "
          + "languages  = ?, "
          + "relevance_id  = ?, "
          //+ "eqf_nqf_level  = '" +  course.eqf_nqf_level + "', "
          //+ "ecvet_points  = '" +  course.ecvet_points + "', "
          + "archive_name  = ?, "
          //+ "media  = '" +  course.media + "', "
          //+ "user_id  = " +  course.user_id + ", "
          + "partner_id  = ?, "
          + "updated_at  = NOW(), "
          //+ "image  = '" +  course.image + "', "
          //+ "cost  = " +  course.cost + ", "
          //+ "rating  = " +  course.rating + ", "
          + "duration  = ?, "
          + "receive_candidates_recommendations  = ?"
          + " WHERE course_id = " + course.course_id

      console.log("\nquery:" + JSON.stringify(query))
      let res = await db.query(query,[course.title,course.description,course.learning_outcomes,course.keywords,course.type_id,course.license.id,course.institution,course.weblink,
        course.languages,course.relevance_id,course.archive_name,course.partner_id,course.duration,course.receive_recommendations])

      console.log("res: "+res)
    }
  }
  
  const deleteUser = async (userId, usersToDelete) => {
    const userRole = await getRoleid(userId)
    var usersInArr = []
    
    if (userRole[0].role_id == 7) {
      usersToDelete.forEach(async function(obj){
        usersInArr.push (obj.id)
        // console.log(JSON.stringify(usersToDelete))
        // console.log(usersInArr)
        // console.log(userRole[0].role_id)
        
        // console.log('DELETE FROM users WHERE user_id IN (' + usersInArr + ')')
        await db.query('DELETE FROM users WHERE user_id = ' + obj.id)
        await db.query('DELETE FROM jobs WHERE company_id = ' + obj.id)
        await db.query('DELETE FROM courses WHERE user_id  = ' + obj.id)
      })
    }
    //return response
  }

  const deleteJob = async (userId, jobId) => {
    const userRole = await getRoleid(userId)
    if (userRole[0].role_id === 7) {
      //await db.query('DELETE FROM jobs WHERE job_id = ? ', jobId)
      await db.query('UPDATE jobs SET deleted_at = NOW() WHERE job_id = ? ', jobId)
    }
    // return response
  }

  const deleteCourse = async (userId, courseId) => {
    const userRole = await getRoleid (userId)
    if (userRole[0].role_id === 7) {
      // console.log("DELETE FROM courses WHERE course_id = " + courseId)
      // await db.query('DELETE FROM courses WHERE course_id = ' + courseId)
      let res = await db.query('UPDATE courses SET deleted_at = NOW() WHERE course_id = ?', courseId)
      console.log("updated ddelted at ",res)
    }else{
      throw new Error('not admin')
    }
    // return response
  }
  
  const getAllRoles = async (email) => {
    const userRole = await getRoleid(email)
    var roles
    if (userRole[0].role_id === 7) { roles = await db.query('SELECT role FROM roles') }
    return roles
  }


const getCourseRatings = async (courseId,show_deleted) => {
  if(courseId === undefined){
    return  await db.query(!show_deleted ? 'SELECT * FROM users_courses WHERE deleted_at IS NULL ' :'SELECT * FROM users_courses')
  }else{
    return  await db.query(!show_deleted ? 'SELECT * FROM users_courses WHERE deleted_at IS NULL AND course_id = ?' : 'SELECT * FROM users_courses WHERE course_id = ?', courseId)
  }
}

const removeCourseRating = async (rating_id,final_delete) => {
  if(final_delete === true){
    return  await db.query('DELETE FROM WHERE users_courses_id = ?', rating_id)
  }else{
    return  await db.query('UPDATE users_courses SET deleted_at = NOW() WHERE users_courses_id = ?', rating_id)
  }
}

const updateRate = async (rating_id,status_id) => {
  const coursesRatesAccepted = await db.query('UPDATE users_courses SET status_id = ? WHERE users_courses_id = ?', [status_id,rating_id])
  return coursesRatesAccepted
}

const backHome = async (email) => {


  const userRole = await getRoleid(email)
  let data = {}
  if (userRole[0].role_id === 7) {

    const users = (await db.query('SELECT COUNT(user_id) as total_users FROM users'))[0].total_users
    const jobs = (await db.query('SELECT COUNT(job_id) as total_jobs FROM jobs WHERE deleted_at IS NULL AND (expired = false AND manual_publish = TRUE ' +
        'OR ( ' +
        '    (publish_ini IS NULL OR CURRENT_TIMESTAMP > publish_ini) \n' +
        '    AND \n' +
        '    (publish_end IS NULL OR CURRENT_TIMESTAMP < publish_end) \n' +
        '    AND \n' +
        '    (end_date IS NULL OR CURRENT_TIMESTAMP < end_date) \n' +
        '))'))[0].total_jobs
    const deleted_jobs = (await db.query('SELECT COUNT(job_id) as total_jobs FROM jobs WHERE deleted_at IS NOT NULL'))[0].total_jobs
    const expired_jobs = (await db.query('SELECT COUNT(job_id) as total_jobs \n' +
        '\tFROM jobs \n' +
        '\tWHERE \n' +
        '\t\tdeleted_at IS NULL AND\n' +
        '\t\t(\n' +
        '\t\t\texpired = TRUE OR\n' +
        '\t\t\t(publish_end IS NOT NULL AND CURRENT_TIMESTAMP < publish_end) OR\n' +
        '\t\t\t(end_date IS NOT NULL AND CURRENT_TIMESTAMP < end_date)\n' +
        '\t\t)'))[0].total_jobs

    const waiting_publish_date_jobs = (await db.query('SELECT COUNT(job_id) as total_jobs \n' +
        '\tFROM jobs \n' +
        '\tWHERE \n' +
        '\t\tdeleted_at IS NULL AND expired = FALSE AND manual_publish = false and  publish_ini is not null  and CURRENT_TIMESTAMP < publish_ini\n' +
        '\t'))[0].total_jobs

    // const deleted_jobs = (await db.query('SELECT COUNT(job_id) as total_jobs FROM jobs WHERE deleted_at IS NOT NULL'))[0].total_jobs
    const courses = (await db.query('SELECT COUNT(course_id) as total_courses FROM courses WHERE deleted_at IS NULL AND (expired = false AND manual_publish = TRUE ' +
        'OR ( ' +
        '    (publish_ini IS NULL OR CURRENT_TIMESTAMP > publish_ini) \n' +
        '    AND \n' +
        '    (publish_end IS NULL OR CURRENT_TIMESTAMP < publish_end) \n' +
        '    AND \n' +
        '    (end_date IS NULL OR CURRENT_TIMESTAMP < end_date) \n' +
        '))'))[0].total_courses

    const deleted_courses = (await db.query('SELECT COUNT(course_id) as total_courses FROM courses WHERE deleted_at IS NOT NULL'))[0].total_courses
    const expired_courses = (await db.query('SELECT COUNT(course_id) as total_courses \n' +
        '\tFROM courses \n' +
        '\tWHERE \n' +
        '\t\tdeleted_at IS NULL AND\n' +
        '\t\t(\n' +
        '\t\t\texpired = TRUE OR\n' +
        '\t\t\t(publish_end IS NOT NULL AND CURRENT_TIMESTAMP < publish_end) OR\n' +
        '\t\t\t(end_date IS NOT NULL AND CURRENT_TIMESTAMP < end_date)\n' +
        '\t\t)'))[0].total_courses

    const waiting_publish_date_courses = (await db.query('SELECT COUNT(course_id) as total_courses \n' +
        '\tFROM courses \n' +
        '\tWHERE \n' +
        '\t\tdeleted_at IS NULL AND expired = FALSE AND manual_publish = false and  publish_ini is not null  and CURRENT_TIMESTAMP < publish_ini\n' +
        '\t'))[0].total_courses

    let rutaArchivo = "./crons/SEF_cron_result.txt";
    let cron = null ;
    if(fs.existsSync(rutaArchivo)){
      cron = fs.readFileSync(rutaArchivo, 'utf8');
    }else{
      cron = "No Cron Result File"
    }

    return {
      users: users,
      jobs: jobs,
      deleted_jobs: deleted_jobs,
      expired_jobs: expired_jobs,
      waiting_publish_date_jobs: waiting_publish_date_jobs,
      //   deleted_jobs:deleted_jobs,
      courses: courses,
      deleted_courses: deleted_courses,
      expired_courses: expired_courses,
      waiting_publish_date_courses: waiting_publish_date_courses,
      cron:cron,
    }
  }
}
  
  module.exports = {
    getRoleid,
    getAllUsers,
    getAllCourses,
    getAllJobs,
    updatePersonalInformation,
    updateJobInformation,
    updateCourseInformation,
    deleteUser,
    deleteJob,
    deleteCourse,
    getAllRoles,
    getCourseRatings,
    removeCourseRating,
    updateRate,
    backHome
  }
  