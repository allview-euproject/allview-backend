const userProfile = require('../models/training-provider/user-profile')
const demandedSkills = require('../models/training-provider/demanded-skills')
const validateCourses = require('../models/training-provider/validate-courses')
const publishTrainingCourses = require('../models/training-provider/publish-training-courses')
const home = require('../models/training-provider/home')

const getUserProfile = async (request, response) => {
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await userProfile.getUserProfile(userId)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const updatePersonalInformation = async (request, response) => {
  const userId = request.payload.userId
  const fullName = request.body.fullName
  const city = request.body.city
  const state = request.body.state
  const country = request.body.country
  const phone = request.body.phone
  const description = request.body.description
  const companIdentification = request.body.company_identification
  const street = request.body.street
  const contactPerson = request.body.contact_person
  const contactMail = request.body.contact_mail
  const website = request.body.website
  const sector = request.body.topic

  try {
    response.status(200).json({
      success: true,
      result: await userProfile.updatePersonalInformation(userId, fullName, city, state, country, phone, description, companIdentification, street, contactPerson, contactMail, website, sector)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const getDemandedSkills = async (request, response) => {
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await demandedSkills.getDemandedSkills(userId)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const getDemandedProfesionalExperience = async (request, response) => {
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await demandedSkills.getDemandedProfesionalExperience(userId)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const getDemandedCompetences = async (request, response) => {
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await demandedSkills.getDemandedCompetences(userId)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const getValidateCourses = async (request, response) => {
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await validateCourses.getValidateCourses(userId)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const acceptRate = async (request, response) => {
  const rateid = request.body.rateid
  try {
    response.status(200).json({
      success: true,
      result: await validateCourses.acceptRate(rateid)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const rejectRate = async (request, response) => {
  const rateid = request.body.rateid
  try {
    response.status(200).json({
      success: true,
      result: await validateCourses.rejectRate(rateid)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const getPublishTrainingCourses = async (request, response) => {
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await publishTrainingCourses.getPublishTrainingCourses(userId)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const uploadCourseImage = async (request, response) => {
  const imageName = request.file ? request.file.filename : null
  const courseId = request.body.course_id
  // const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await publishTrainingCourses.uploadCourseImage(imageName, courseId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const updateCourses = async (request, response) => {
  const userId = request.payload.userId
  const course = request.body.course
  try {
    response.status(200).json({
      success: true,
      result: await publishTrainingCourses.updateCourses(userId, course)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const deleteCourses = async (request, response) => {
  const userId = request.payload.userId
  const courseId = request.params.course_id
  try {
    response.status(200).json({
      success: true,
      result: await publishTrainingCourses.deleteCourses(userId, courseId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const getHome = async (request, response) => {
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await home.getHome(userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

module.exports = {
  getUserProfile,
  updatePersonalInformation,
  getDemandedSkills,
  getDemandedProfesionalExperience,
  getDemandedCompetences,
  getValidateCourses,
  acceptRate,
  rejectRate,
  getPublishTrainingCourses,
  uploadCourseImage,
  updateCourses,
  deleteCourses,
  getHome
}
