const userProfile = require('../models/people/user-profile')
const learningPaths = require('../models/people/learning-paths')
const home = require('../models/people/home')

const getUserProfile = async (request, response) => {
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await userProfile.getUserProfile(userId)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const updatePersonalInformation = async (request, response) => {
  const userId = request.payload.userId
  const fullName = request.body.fullName
  const city = request.body.city
  const state = request.body.state
  const country = request.body.country
  const phone = request.body.phone
  const description = request.body.description
  const companIdentification = request.body.company_identification
  const street = request.body.street
  const contactPerson = request.body.contact_person
  const website = request.body.website
  const mainProduct = request.body.main_product
  const sectorId = request.body.sector_id

  try {
    response.status(200).json({
      success: true,
      result: await userProfile.updatePersonalInformation(userId, fullName, city, state, country, phone, description, companIdentification, street, contactPerson, website, mainProduct, sectorId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const updateInformationTrainingProvider = async (request, response) => {
  const userId = request.payload.userId
  const contactPerson = request.body.contactPerson
  const fullName = request.body.fullName
  const companyIdentification = request.body.companyIdentification
  const description = request.body.description
  const website = request.body.website
  const location = request.body.location
  const topic = request.body.topic

  try {
    response.status(200).json({
      success: true,
      result: await userProfile.updateInformationTrainingProvider(fullName, contactPerson, description, companyIdentification, website, location, topic, userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const updateUserSkills = async (request, response) => {
  const userId = request.payload.userId
  const skills = request.body.skills
  try {
    response.status(200).json({
      success: true,
      result: await userProfile.updateUserSkills(userId, skills)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const updateUserCompetences = async (request, response) => {
  const userId = request.payload.userId
  const skills = request.body.skills
  try {
    response.status(200).json({
      success: true,
      result: await userProfile.updateUserCompetences(userId, skills)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const updateUserExperiences = async (request, response) => {
  const userId = request.payload.userId
  const experiences = request.body.experiences
  try {
    response.status(200).json({
      success: true,
      result: await userProfile.updateUserExperiences(userId, experiences)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const updateUserChallenges = async (request, response) => {
  const userId = request.payload.userId
  const challenges = request.body.challenges
  try {
    response.status(200).json({
      success: true,
      result: await userProfile.updateUserChallenges(userId, challenges)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const updateUserEducations = async (request, response) => {
  const userId = request.payload.userId
  const educations = request.body.educations
  try {
    response.status(200).json({
      success: true,
      result: await userProfile.updateUserEducations(userId, educations)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const getLearningPaths = async (request, response) => {
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await learningPaths.getLearningPaths(userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const uploadCv = async (request, response) => {
  const cvName = request.file.filename
  const userId = request.body.userId
  try {
    response.status(200).json({
      success: true,
      result: await userProfile.uploadCv(cvName, userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const deleteCv = async (request, response) => {
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await userProfile.deleteCv(userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const getHome = async (request, response) => {
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await home.getHome(userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

module.exports = {
  getUserProfile,
  updatePersonalInformation,
  updateInformationTrainingProvider,
  updateUserSkills,
  updateUserCompetences,
  updateUserExperiences,
  updateUserChallenges,
  updateUserEducations,
  getLearningPaths,
  uploadCv,
  deleteCv,
  getHome
}
