const backoffice = require('../models/backoffice')

const getRoleId = async (request, response) => {
  response.setHeader('Access-Control-Allow-Origin', '*');
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await backoffice.getRoleid(userId)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}
const getAllUsers = async (request, response) => {
  //console.log(request.payload)
  try {
    const userId = request.payload.userId
    response.status(200).json({
      success: true,
      result: await backoffice.getAllUsers(userId)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const getAllCourses = async (request, response) => {
  //console.log(request.payload)
  try {
    const userId = request.payload.userId
    response.status(200).json({
      success: true,
      result: await backoffice.getAllCourses(userId)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const backHome = async (request, response) => {
  try {
    const userId = request.payload.userId
    response.status(200).json({
      success: true,
      result: await backoffice.backHome(userId)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const getAllJobs = async (request, response) => {
  try {
    const userId = request.payload.userId
    response.status(200).json({
      success: true,
      result: await backoffice.getAllJobs(userId)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const updatePersonalInformation = async (request, response) => {

  const id = request.body.userId
  const email = request.body.email
  const fullName = request.body.fullName
  const role = request.body.role
  const password = request.body.password
  const passwordCheck = request.body.password_check
  const city = request.body.city
  const state = request.body.state
  const country = request.body.country
  const phone = request.body.phone
  const description = request.body.description
  const companyIdentification = request.body.company_identification
  const street = request.body.street
  const contactPerson = request.body.contact_person
  const website = request.body.website
  const mainProduct = request.body.main_product
  const sector = request.body.sector_id
  const topic = request.body.topic

  try {
    response.status(200).json({
      success: true,
      result: await backoffice.updatePersonalInformation(id, email, fullName, role, password, passwordCheck, city, state,
        country, phone, description, companyIdentification, street, contactPerson, website, mainProduct, sector, topic)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const updateJobInformation = async (request, response) => {

  const userId = request.payload.userId
  // const id = request.body.jobId
  // const name = request.body.name
  // const description = request.body.description
  // const salary = request.body.salary
  // const contact_details = request.body.contact_details
  // const image = request.body.image
  // const language_id = request.body.language_id
  // const contract_id = request.body.contract_id
  // const duration = request.body.duration
  // const receive_candidates_recommendations = request.body.receive_candidates_recommendations

  // const jobj = {"job_id": id,"name": name, "description": description, "salary": salary, 
  // "contact_details": contact_details, "language_id": language_id, "contract_id": contract_id, "duration": duration,
  // "receive_candidates_recommendations": receive_candidates_recommendations, "image": image}

  jobj = request.body.job_to_update

  try {
    response.status(200).json({
      success: true,
    //   result: await backoffice.updatePersonalInformation(id, email, fullName, role, password, passwordCheck, city, state,
    //     country, phone, description, companyIdentification, street, contactPerson, website, mainProduct, sector, topic)
    // })
      result: await backoffice.updateJobInformation(jobj, userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const updateCourseInformation = async (request, response) => {

  const userId = request.payload.userId
  const course = request.body.course_to_update

  try {
    response.status(200).json({
      success: true,
    //   result: await backoffice.updatePersonalInformation(id, email, fullName, role, password, passwordCheck, city, state,
    //     country, phone, description, companyIdentification, street, contactPerson, website, mainProduct, sector, topic)
    // })
      result: await backoffice.updateCourseInformation(course, userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const deleteUser = async (request, response) => {
  // console.log(request)
  // console.log(request.params)
  // console.log(request.body)
  try {
    const userId = request.payload.userId
    const usersToDelete = request.body.users_to_delete
    console.log(JSON.stringify(usersToDelete))
    response.status(200).json({
      success: true,
      result: await backoffice.deleteUser(userId, usersToDelete)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const deleteJob = async (request, response) => {
  try {
    const userId = request.payload.userId
    const jobId = request.body.job_to_delete
    response.status(200).json({
      success: true,
      result: await backoffice.deleteJob(userId, jobId)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const deleteCourse = async (request, response) => {
  console.log(JSON.stringify(request.body))
  try {
    const userId = request.payload.userId
    console.log("EL USUARIO ES " + userId)
    const courseId = request.body.course_to_delete
    response.status(200).json({
      success: true,
      result: await backoffice.deleteCourse(userId, courseId)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const  getCourseRatings = async (request, response) => {
  try {
    const courseId = request.body.courseId ? request.body.courseId : undefined;
    const show_deleted = request.body.show_deleted ? request.body.show_deleted : false
    response.status(200).json({
      success: true,
      result: await backoffice.getCourseRatings(courseId,show_deleted)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const removeCourseRating = async (request, response) => {
  try {
    const rating_id = request.body.rating_id ;
    const final_delete = request.body.final_delete ? request.body.final_delete : false
    response.status(200).json({
      success: true,
      result: await backoffice.removeCourseRating(rating_id,final_delete)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}


const updateRate = async (request, response) => {
  try {
    const rating_id = request.body.rating_id ;
    const status_id = request.body.status_id ;
    response.status(200).json({
      success: true,
      result: await backoffice.updateRate(rating_id,status_id)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const getAllRoles = async (request, response) => {
  try {
    const email = request.payload.email
    response.status(200).json({
      success: true,
      result: await backoffice.getAllRoles(email)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

module.exports = {
  getRoleId,
  getAllUsers,
  getAllJobs,
  getAllCourses,
  updatePersonalInformation,
  updateJobInformation,
  updateCourseInformation,
  deleteUser,
  deleteJob,
  deleteCourse,
  getAllRoles,
  getCourseRatings,
  removeCourseRating,
  updateRate,
  backHome
}
