const common = require('../models/common')
const path = require('path')

const updatePassword = async (request, response) => {
  const email = request.payload.email
  const newmail = request.payload.newmail
  const password = request.body.password
  const passwordCheck = request.body.passwordCheck
  try {
    response.status(200).json({
      success: true,
      result: await common.updatePassword(email, newmail, password, passwordCheck)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const deleteAccount = async (request, response) => {
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await common.deleteAccount(userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const searchSkillsByName = async (request, response) => {
  const name = request.body.name
  try {
    response.status(200).json({
      success: true,
      result: await common.searchSkillsByName(name)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const searchCompetencesByName = async (request, response) => {
  const name = request.body.name
  try {
    response.status(200).json({
      success: true,
      result: await common.searchCompetencesByName(name)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const searchExperiencesByName = async (request, response) => {
  const name = request.body.name
  const level = request.query.level
  try {
    response.status(200).json({
      success: true,
      result: await common.searchExperiencesByName(name, level)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const searchChallengesByName = async (request, response) => {
  const name = request.body.name
  try {
    response.status(200).json({
      success: true,
      result: await common.searchChallengesByName(name)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const searchCoursesByName = async (request, response) => {
  const name = request.body.name
  const limit = request.body.limit
  const offset = request.body.limit
  try {
    response.status(200).json({
      success: true,
      result: await common.searchCoursesByName(name,limit,offset)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const getCountries = async (request, response) => {
  try {
    response.status(200).json({
      success: true,
      result: await common.getCountries()
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const getImage = async (request, response) => {
  response.sendFile(path.resolve(__dirname, '../public/images/' + request.params.folder + '/' + request.params.image))
}

const getCv = async (request, response) => {
  response.sendFile(path.resolve(__dirname, '../public/cvs/' + request.params.cv))
}

const getSearchCourses = async (request, response) => {
  const userId = request.payload.userId
    const limit = request.body.limit
    const offset = request.body.offset
  try {
    response.status(200).json({
      success: true,
      result: await common.getSearchCourses(userId,limit,offset)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const searchCourses = async (request, response) => {
  const userId = request.payload.userId
  const keyWords = request.body.keywords
  const eqf_levels = request.body.eqf_levels
  const free_or_paid = request.body.free_or_paid
  const competences = request.body.competences
  const languages = request.body.languages
  const skills = request.body.skills
  const minduration = request.body.minduration
  const maxduration = request.body.maxduration
  const order = request.body.order
  const types = request.body.types
  const topics = request.body.topics
  const partners = request.body.partners
  const limit = request.body.limit
  const offset = request.body.offset

  try {
    response.status(200).json({
      success: true,
      result: await common.searchCourses(userId,keyWords,eqf_levels,free_or_paid, languages, minduration, maxduration, competences, types, topics, partners, skills, order,limit,offset)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const deleteFavCourses = async (request, response) => {
  const courseId = request.body.course_id
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await common.deleteFavCourses(courseId, userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const addFavCourses = async (request, response) => {
  const courseId = request.body.course_id
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await common.addFavCourses(courseId, userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const deleteFavJobs = async (request, response) => {
  const jobId = request.body.job_id
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await common.deleteFavJobs(jobId, userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const addFavJobs = async (request, response) => {
  const jobId = request.body.job_id
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await common.addFavJobs(jobId, userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}


const getRateCourses = async (request, response) => {
  const userId = request.payload.userId
  const nameFile = await common.getRateCourses(userId)
  console.log(JSON.stringify(request.payload))
  try {
    response.status(200).json({
      success: true,
      result: nameFile
    })

    // resp = {"nameFile": nameFile}
    // response.send(JSON.stringify(resp))
    
  } catch (error) {
    console.log("getRateCourses Error",error)
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const rateCourse = async (request, response) => {
  const userId = request.payload.userId
  const course = request.body.course
  const rating = request.body.rating
  const comment = request.body.comment
  const reason = request.body.reason
  try {
    response.status(200).json({
      success: true,
      result: await common.rateCourse(userId, course, rating, comment, reason)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const getCourse = async (request, response) => {
  const userId = request.payload.userId
  const idcourse = request.params.course_id
  try {
    response.status(200).json({
      success: true,
      result: await common.getCourse(userId,idcourse)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const getCourseNoUID = async (request, response) => {

  const idcourse = request.params.course_id
  try {
    response.status(200).json({
      success: true,
      result: await common.getCourse(0,idcourse)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const uploadProfileImage = async (request, response) => {
  const imageName = request.file ? request.file.filename : null
  const userId = request.body.userId
  //console.log(request)
  try {
    response.status(200).json({
      success: true,
      result: await common.uploadProfileImage(imageName, userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const getSearchJobs = async (request, response) => {
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await common.getSearchJobs(userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const getJobsByOptions = async (request, response) => {
  const keywords = request.body.keywords
  const languages = request.body.languages
  const competences = request.body.competences
  const skills = request.body.skills
  const contracts = request.body.contracts
  const order = request.body.order
  const companies = request.body.companies
  const limit = request.body.limit
  const offset = request.body.offset
  try {
    response.status(200).json({
      success: true,
      result: await common.getJobsByOptions(keywords, languages, competences, skills, contracts, companies, order,limit,offset)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const getJobById = async (request, response) => {
  const jobId = request.params.job_id
  try {
    response.status(200).json({
      success: true,
      result: await common.getJobById(jobId)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const searchCandidates = async (request, response) => {
  const skills = request.body.skills
  const competences = request.body.competences
  const experiences = request.body.experiences
  const courses = request.body.courses
  try {
    response.status(200).json({
      success: true,
      result: await common.searchCandidates(skills, competences, experiences,courses)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}


const deleteExternalResource = async (request, response) => {
  const resource_id = request.body.resource_id
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await common.deleteExternalResource(resource_id, userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const addExternalResource = async (request, response) => {
  const file = request.file ? request.file.filename : null
  const userId = request.payload.userId;
  const type = request.body.res_type
  const name = request.body.name
  const url = request.body.url
  const desc = request.body.desc
  const entityTable = request.body.entityTable
  const entityId = request.body.entityId

  try {
    response.status(200).json({
      success: true,
      result: await common.addExternalResource(type,name,desc,entityTable,entityId,userId,file,url)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const getExternalResource = async (request, response) => {

  const userId = request.payload.userId
  const entityTable = request.body.entityTable
  const entityId = request.body.entityId

  try {
    response.status(200).json({
      success: true,
      result: await common.getExternalResource(entityTable,entityId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}


module.exports = {
  getExternalResource,
  addExternalResource,
  deleteExternalResource,
  searchSkillsByName,
  searchCompetencesByName,
  searchExperiencesByName,
  searchChallengesByName,
  searchCoursesByName,
  getImage,
  getCountries,
  getSearchCourses,
  searchCourses,
  deleteFavCourses,
  addFavCourses,
  getRateCourses,
  rateCourse,
  getCourse,
  deleteFavJobs,
  addFavJobs,
  updatePassword,
  deleteAccount,
  uploadProfileImage,
  getCv,
  getSearchJobs,
  getJobsByOptions,
  getJobById,
  searchCandidates,
  getCourseNoUID
}
