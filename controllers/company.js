const userProfile = require('../models/company/user-profile')
const offerJobs = require('../models/company/offer-jobs')
const home = require('../models/company/home')
const candidates = require('../models/company/candidates')

const getUserProfile = async (request, response) => {
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await userProfile.getUserProfile(userId)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const updatePersonalInformation = async (request, response) => {
  const userId = request.payload.userId
  const fullName = request.body.fullName
  const city = request.body.city
  const state = request.body.state
  const country = request.body.country
  const phone = request.body.phone
  const description = request.body.description
  const companIdentification = request.body.company_identification
  const street = request.body.street
  const contactPerson = request.body.contact_person
  const contactMail = request.body.contact_mail
  const website = request.body.website
  const sector = request.body.sector
  console.log(JSON.stringify(request.body))

  try {
    response.status(200).json({
      success: true,
      result: await userProfile.updatePersonalInformation(userId, fullName, city, state, country, phone, description, companIdentification, street, contactPerson, contactMail, website, sector)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const updateJobs = async (request, response) => {
  const userId = request.payload.userId
  const jobs = request.body.jobs
  try {
    response.status(200).json({
      success: true,
      result: await offerJobs.updateJobs(userId, jobs)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const updateJob = async (request, response) => {
  const userId = request.payload.userId
  const jobs = request.body.jobs
  try {
    response.status(200).json({
      success: true,
      result: await offerJobs.updateJob(userId, jobs)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const deleteJob = async (request, response) => {
  const userId = request.payload.userId
  const job = request.body.job
  try {
    response.status(200).json({
      success: true,
      result: await offerJobs.deleteJob(userId, job)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const uploadJobImage = async (request, response) => {
  const imageName = request.body.delete_image ? undefined : request.file.filename;
  const jobId = request.body.job_id
  // const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await offerJobs.uploadJobImage(imageName, jobId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const getOfferJobs = async (request, response) => {
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await offerJobs.getOfferJobs(userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const getHome = async (request, response) => {
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await home.getHome(userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const postFavCandidate = async (request, response) => {
  const userId = request.payload.userId
  const candidateId = request.body.candidateId
  console.log("LOS USUARIOS SON: " + userId + " ---- " + candidateId)
  try {
    response.status(200).json({
      success: true,
      result: await candidates.doFavCandidate(userId, candidateId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const deleteFavCandidate = async (request, response) => {
  const userId = request.payload.userId
  const candidateId = request.body.candidateId
  console.log(userId, candidateId)
  try {
    response.status(200).json({
      success: true,
      result: await candidates.deleteFavCandidate(userId, candidateId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const getCandidates = async(request, response) =>{
  const userId = request.payload.userId
  try {
    response.status(200).json({
      success: true,
      result: await candidates.getCandidates(userId)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

module.exports = {
  getUserProfile,
  updatePersonalInformation,
  updateJobs,
  updateJob,
  getOfferJobs,
  uploadJobImage,
  getHome,
  getCandidates,
  postFavCandidate,
  deleteFavCandidate,
  deleteJob
}
