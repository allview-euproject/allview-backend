const home = require('../models/home')

const login = async (request, response) => {
  const email = request.body.email
  const password = request.body.password
  try {
    response.status(200).json({
      success: true,
      result: await home.login(email, password)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const register = async (request, response) => {
  const name = request.body.name
  const email = request.body.email
  const role = request.body.role
  const password = request.body.password
  const passwordCheck = request.body.password_check
  try {
    response.status(200).json({
      success: true,
      result: await home.register(name, email, role, password, passwordCheck)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const sendVerifyAccountEmail = async (request, response) => {
  const email = request.body.email
  try {
    response.status(200).json({
      success: true,
      result: await home.sendVerifyAccountEmail(email)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const googleLogin = async (request, response) => {
  const googleToken = request.body.googleToken
  try {
    response.status(200).json({
      success: true,
      result: await home.googleLogin(googleToken)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const verify = async (request, response) => {
  const token = request.params.token
  try {
    response.status(200).json({
      success: true,
      result: await home.verify(token)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const recoverPasswordEmail = async (request, response) => {
  const email = request.body.email
  try {
    response.status(200).json({
      success: true,
      result: await home.recoverPasswordEmail(email)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const recoverPassword = async (request, response) => {
  const token = request.params.token
  const password = request.body.password
  const passwordCheck = request.body.passwordCheck
  try {
    response.status(200).json({
      success: true,
      result: await home.recoverPassword(token, password, passwordCheck)
    })
  } catch (error) {
    response.status(404).json({
      success: false,
      message: error.message
    })
  }
}

const setRole = async (request, response) => {
  const userId = request.payload.userId
  const role = request.params.role
  try {
    response.status(200).json({
      success: true,
      result: await home.setRole(userId, role)
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const landing = async (request, response) => {
  try {
    response.status(200).json({
      success: true,
      result: await home.landing()
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

const landing_courses_jobs = async (request, response) => {
  try {
    response.status(200).json({
      success: true,
      result: await home.landing_courses_jobs()
    })
  } catch (error) {
    response.status(500).json({
      success: false,
      message: error.message
    })
  }
}

module.exports = {
  login,
  register,
  verify,
  recoverPasswordEmail,
  recoverPassword,
  googleLogin,
  setRole,
  landing,
  landing_courses_jobs,
  sendVerifyAccountEmail
}
