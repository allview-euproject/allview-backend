-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Dec 23, 2021 at 11:27 AM
-- Server version: 8.0.22
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `allview_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `candidates_recommendations`
--

CREATE TABLE `candidates_recommendations` (
  `candidate_recommendation_id` int NOT NULL,
  `user_id` int NOT NULL,
  `candidate_id` int NOT NULL,
  `course_id` int NOT NULL,
  `score` float NOT NULL,
  `type` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` int NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint DEFAULT NULL,
  `phonecode` int NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `course_id` int NOT NULL,
  `category_id` int DEFAULT NULL,
  `title` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `description` varchar(6000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `learning_outcomes` varchar(3500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `keywords` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `type_id` int DEFAULT NULL,
  `access` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `license_id` int DEFAULT NULL,
  `institution` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `weblink` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `language_id` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `relevance_id` int DEFAULT NULL,
  `eqf_nqf_level` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `ecvet_points` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `archive_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `media` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `partner_id` int DEFAULT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `cost` float DEFAULT NULL,
  `rating` int DEFAULT '0',
  `duration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `receive_candidates_recommendations` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses_categories`
--

CREATE TABLE `courses_categories` (
  `course_category_id` int NOT NULL,
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses_competences`
--

CREATE TABLE `courses_competences` (
  `courses_competences_id` int NOT NULL,
  `course_id` int NOT NULL,
  `skill_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses_languages`
--

CREATE TABLE `courses_languages` (
  `course_language_id` int NOT NULL,
  `language` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses_licenses`
--

CREATE TABLE `courses_licenses` (
  `course_license_id` int NOT NULL,
  `license` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses_occupations`
--

CREATE TABLE `courses_occupations` (
  `course_occupation_id` int NOT NULL,
  `course_id` int NOT NULL,
  `occupation_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses_partners`
--

CREATE TABLE `courses_partners` (
  `course_partner_id` int NOT NULL,
  `partner` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses_relevances`
--

CREATE TABLE `courses_relevances` (
  `course_relevance_id` int NOT NULL,
  `relevance` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses_skills`
--

CREATE TABLE `courses_skills` (
  `course_skill_id` int NOT NULL,
  `course_id` int NOT NULL,
  `skill_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses_topics`
--

CREATE TABLE `courses_topics` (
  `course_topic_id` int NOT NULL,
  `course_id` int NOT NULL,
  `topic_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses_types`
--

CREATE TABLE `courses_types` (
  `course_type_id` int NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses_users_favorites`
--

CREATE TABLE `courses_users_favorites` (
  `course_user_favorite_id` int NOT NULL,
  `course_id` int NOT NULL,
  `user_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `job_id` int NOT NULL,
  `name` varchar(60) NOT NULL,
  `description` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `sector_id` int NOT NULL,
  `salary` decimal(10,0) NOT NULL,
  `contact_details` varchar(30) NOT NULL,
  `company_id` int NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `rating` decimal(10,0) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `language_id` int NOT NULL,
  `contract_id` int NOT NULL,
  `duration` int DEFAULT NULL,
  `receive_candidates_recommendations` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs_contracts`
--

CREATE TABLE `jobs_contracts` (
  `job_contract_id` int NOT NULL,
  `contract` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs_languages`
--

CREATE TABLE `jobs_languages` (
  `job_language_id` int NOT NULL,
  `language` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs_occupations`
--

CREATE TABLE `jobs_occupations` (
  `job_occupation_id` int NOT NULL,
  `job_id` int NOT NULL,
  `occupation_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs_skills`
--

CREATE TABLE `jobs_skills` (
  `job_skill_id` int NOT NULL,
  `job_id` int NOT NULL,
  `skill_id` int NOT NULL,
  `essential` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs_users_favorites`
--

CREATE TABLE `jobs_users_favorites` (
  `job_user_favorite_id` int NOT NULL,
  `job_id` int NOT NULL,
  `user_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `occupations`
--

CREATE TABLE `occupations` (
  `occupation_id` int NOT NULL,
  `occupation_name` varchar(100) NOT NULL,
  `occupation_description` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `occupation_code` varchar(10) NOT NULL,
  `occupation_level` int NOT NULL,
  `occupation_parent` int DEFAULT NULL,
  `occupation_uri` varchar(100) NOT NULL,
  `occupation_href` varchar(200) NOT NULL,
  `occupation_is_last_level` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int NOT NULL,
  `role` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `skill_id` int NOT NULL,
  `skill_name` varchar(100) NOT NULL,
  `skill_description` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `skill_type_id` int NOT NULL,
  `skill_level` int NOT NULL,
  `skill_code` varchar(10) DEFAULT NULL,
  `skill_parent` int DEFAULT NULL,
  `skill_uri` varchar(100) NOT NULL,
  `skill_href` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `skills_occupations_essential_relations`
--

CREATE TABLE `skills_occupations_essential_relations` (
  `skill_id` int NOT NULL,
  `occupation_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `skills_occupations_optional_relations`
--

CREATE TABLE `skills_occupations_optional_relations` (
  `skill_id` int NOT NULL,
  `occupation_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `skills_types`
--

CREATE TABLE `skills_types` (
  `skill_type_id` int NOT NULL,
  `skill_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'Skills son skills\r\nAttitudes and Values son competences'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `topic_id` int NOT NULL,
  `topic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `google_sub` varchar(50) DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `description` varchar(240) DEFAULT NULL,
  `company_identification` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `street` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `website` varchar(30) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `contact_person` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `sector_id` int DEFAULT NULL,
  `main_product` int DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `verify_token` bigint DEFAULT NULL,
  `topic` int DEFAULT NULL,
  `cv` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_competences`
--

CREATE TABLE `users_competences` (
  `user_competence_id` int NOT NULL,
  `user_id` int NOT NULL,
  `skill_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_courses`
--

CREATE TABLE `users_courses` (
  `users_courses_id` int NOT NULL,
  `user_id` int NOT NULL,
  `course_id` int NOT NULL,
  `status_id` int DEFAULT '1',
  `rating` decimal(2,1) NOT NULL,
  `comment` varchar(480) DEFAULT NULL,
  `reason` varchar(5000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_courses_recommendations`
--

CREATE TABLE `users_courses_recommendations` (
  `user_course_recommendation` int NOT NULL,
  `user_id` int NOT NULL,
  `course_id` int NOT NULL,
  `predicted_rating` float NOT NULL,
  `type` int NOT NULL COMMENT '0: collaborative\r\n1: content-based job challenges\r\n2: content-based rated courses'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_courses_status`
--

CREATE TABLE `users_courses_status` (
  `status_id` int NOT NULL,
  `status` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_educations`
--

CREATE TABLE `users_educations` (
  `users_educations_id` int NOT NULL,
  `user_id` int NOT NULL,
  `university` varchar(100) NOT NULL,
  `level` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `start_year` int NOT NULL,
  `end_year` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_experiences`
--

CREATE TABLE `users_experiences` (
  `users_experiences_id` int NOT NULL,
  `user_id` int NOT NULL,
  `occupation_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_jobs_recommendations`
--

CREATE TABLE `users_jobs_recommendations` (
  `user_job_recommendation_id` int NOT NULL,
  `user_id` int NOT NULL,
  `job_id` int NOT NULL,
  `score` float NOT NULL COMMENT 'From 0 to 1',
  `type` int NOT NULL COMMENT '0: content-based job-user skills'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_occupations`
--

CREATE TABLE `users_occupations` (
  `user_occupation_id` int NOT NULL,
  `user_id` int NOT NULL,
  `occupation_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_skills`
--

CREATE TABLE `users_skills` (
  `user_skill_id` int NOT NULL,
  `user_id` int NOT NULL,
  `skill_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `candidates_recommendations`
--
ALTER TABLE `candidates_recommendations`
  ADD PRIMARY KEY (`candidate_recommendation_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`course_id`),
  ADD KEY `courses_entity_constraint` (`partner_id`),
  ADD KEY `courses_language_constraint` (`language_id`),
  ADD KEY `courses_category_constraint` (`category_id`),
  ADD KEY `courses_licensetype_constraint` (`license_id`),
  ADD KEY `courses_relevance_constraint` (`relevance_id`),
  ADD KEY `courses_type_constraint` (`type_id`),
  ADD KEY `courses_user_constraint` (`user_id`);

--
-- Indexes for table `courses_categories`
--
ALTER TABLE `courses_categories`
  ADD PRIMARY KEY (`course_category_id`);

--
-- Indexes for table `courses_competences`
--
ALTER TABLE `courses_competences`
  ADD PRIMARY KEY (`courses_competences_id`),
  ADD KEY `course_id` (`course_id`),
  ADD KEY `skill_id` (`skill_id`);

--
-- Indexes for table `courses_languages`
--
ALTER TABLE `courses_languages`
  ADD PRIMARY KEY (`course_language_id`);

--
-- Indexes for table `courses_licenses`
--
ALTER TABLE `courses_licenses`
  ADD PRIMARY KEY (`course_license_id`);

--
-- Indexes for table `courses_occupations`
--
ALTER TABLE `courses_occupations`
  ADD PRIMARY KEY (`course_occupation_id`),
  ADD KEY `courses_occupations_ibfk_1` (`course_id`),
  ADD KEY `courses_occupations_ibfk_2` (`occupation_id`);

--
-- Indexes for table `courses_partners`
--
ALTER TABLE `courses_partners`
  ADD PRIMARY KEY (`course_partner_id`);

--
-- Indexes for table `courses_relevances`
--
ALTER TABLE `courses_relevances`
  ADD PRIMARY KEY (`course_relevance_id`);

--
-- Indexes for table `courses_skills`
--
ALTER TABLE `courses_skills`
  ADD PRIMARY KEY (`course_skill_id`),
  ADD KEY `courses_skills_ibfk_1` (`course_id`),
  ADD KEY `courses_skills_ibfk_2` (`skill_id`);

--
-- Indexes for table `courses_topics`
--
ALTER TABLE `courses_topics`
  ADD PRIMARY KEY (`course_topic_id`),
  ADD KEY `course_id` (`course_id`);

--
-- Indexes for table `courses_types`
--
ALTER TABLE `courses_types`
  ADD PRIMARY KEY (`course_type_id`);

--
-- Indexes for table `courses_users_favorites`
--
ALTER TABLE `courses_users_favorites`
  ADD PRIMARY KEY (`course_user_favorite_id`),
  ADD UNIQUE KEY `course_id_2` (`course_id`,`user_id`),
  ADD KEY `course_id` (`course_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`job_id`),
  ADD KEY `JB_DU_FK` (`contract_id`),
  ADD KEY `JB_LG_FK` (`language_id`),
  ADD KEY `JB_CP_FK` (`company_id`);

--
-- Indexes for table `jobs_contracts`
--
ALTER TABLE `jobs_contracts`
  ADD PRIMARY KEY (`job_contract_id`) USING BTREE;

--
-- Indexes for table `jobs_languages`
--
ALTER TABLE `jobs_languages`
  ADD PRIMARY KEY (`job_language_id`);

--
-- Indexes for table `jobs_occupations`
--
ALTER TABLE `jobs_occupations`
  ADD PRIMARY KEY (`job_occupation_id`),
  ADD KEY `JB_JO_FK` (`job_id`),
  ADD KEY `OC_JO_FK` (`occupation_id`);

--
-- Indexes for table `jobs_skills`
--
ALTER TABLE `jobs_skills`
  ADD PRIMARY KEY (`job_skill_id`),
  ADD KEY `JS_JB_FK` (`job_id`),
  ADD KEY `JS_SK_FK` (`skill_id`);

--
-- Indexes for table `jobs_users_favorites`
--
ALTER TABLE `jobs_users_favorites`
  ADD PRIMARY KEY (`job_user_favorite_id`),
  ADD UNIQUE KEY `jobs_id` (`job_id`,`user_id`),
  ADD KEY `US_JU_FK` (`user_id`);

--
-- Indexes for table `occupations`
--
ALTER TABLE `occupations`
  ADD PRIMARY KEY (`occupation_id`),
  ADD UNIQUE KEY `occupation_href` (`occupation_href`),
  ADD UNIQUE KEY `occupation_uri` (`occupation_uri`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`skill_id`),
  ADD UNIQUE KEY `skill_uri` (`skill_uri`),
  ADD UNIQUE KEY `skill_href` (`skill_href`),
  ADD KEY `skill_type_constraint` (`skill_type_id`);

--
-- Indexes for table `skills_occupations_essential_relations`
--
ALTER TABLE `skills_occupations_essential_relations`
  ADD UNIQUE KEY `skill_id` (`skill_id`,`occupation_id`),
  ADD KEY `occupation_constraint_essential` (`occupation_id`);

--
-- Indexes for table `skills_occupations_optional_relations`
--
ALTER TABLE `skills_occupations_optional_relations`
  ADD UNIQUE KEY `skill_id` (`skill_id`,`occupation_id`),
  ADD KEY `occupation_constraint_optional` (`occupation_id`);

--
-- Indexes for table `skills_types`
--
ALTER TABLE `skills_types`
  ADD PRIMARY KEY (`skill_type_id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`topic_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `google_sub` (`google_sub`),
  ADD KEY `user_role_constraint` (`role_id`),
  ADD KEY `US_OC_FK` (`sector_id`),
  ADD KEY `user_topic_constraint` (`topic`);

--
-- Indexes for table `users_competences`
--
ALTER TABLE `users_competences`
  ADD PRIMARY KEY (`user_competence_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `skill_id` (`skill_id`);

--
-- Indexes for table `users_courses`
--
ALTER TABLE `users_courses`
  ADD PRIMARY KEY (`users_courses_id`),
  ADD KEY `US_UC_FK` (`user_id`),
  ADD KEY `CR_UC_FK` (`course_id`),
  ADD KEY `CS_UC_FK` (`status_id`);

--
-- Indexes for table `users_courses_recommendations`
--
ALTER TABLE `users_courses_recommendations`
  ADD PRIMARY KEY (`user_course_recommendation`),
  ADD KEY `course_id` (`course_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users_courses_status`
--
ALTER TABLE `users_courses_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `users_educations`
--
ALTER TABLE `users_educations`
  ADD PRIMARY KEY (`users_educations_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users_experiences`
--
ALTER TABLE `users_experiences`
  ADD PRIMARY KEY (`users_experiences_id`),
  ADD KEY `users_experiences_ibfk_1` (`user_id`);

--
-- Indexes for table `users_jobs_recommendations`
--
ALTER TABLE `users_jobs_recommendations`
  ADD PRIMARY KEY (`user_job_recommendation_id`);

--
-- Indexes for table `users_occupations`
--
ALTER TABLE `users_occupations`
  ADD PRIMARY KEY (`user_occupation_id`);

--
-- Indexes for table `users_skills`
--
ALTER TABLE `users_skills`
  ADD PRIMARY KEY (`user_skill_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `skill_id` (`skill_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `candidates_recommendations`
--
ALTER TABLE `candidates_recommendations`
  MODIFY `candidate_recommendation_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `course_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses_categories`
--
ALTER TABLE `courses_categories`
  MODIFY `course_category_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses_competences`
--
ALTER TABLE `courses_competences`
  MODIFY `courses_competences_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses_languages`
--
ALTER TABLE `courses_languages`
  MODIFY `course_language_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses_licenses`
--
ALTER TABLE `courses_licenses`
  MODIFY `course_license_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses_occupations`
--
ALTER TABLE `courses_occupations`
  MODIFY `course_occupation_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses_partners`
--
ALTER TABLE `courses_partners`
  MODIFY `course_partner_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses_relevances`
--
ALTER TABLE `courses_relevances`
  MODIFY `course_relevance_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses_skills`
--
ALTER TABLE `courses_skills`
  MODIFY `course_skill_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses_topics`
--
ALTER TABLE `courses_topics`
  MODIFY `course_topic_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses_types`
--
ALTER TABLE `courses_types`
  MODIFY `course_type_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses_users_favorites`
--
ALTER TABLE `courses_users_favorites`
  MODIFY `course_user_favorite_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `job_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs_contracts`
--
ALTER TABLE `jobs_contracts`
  MODIFY `job_contract_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs_languages`
--
ALTER TABLE `jobs_languages`
  MODIFY `job_language_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs_occupations`
--
ALTER TABLE `jobs_occupations`
  MODIFY `job_occupation_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs_skills`
--
ALTER TABLE `jobs_skills`
  MODIFY `job_skill_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs_users_favorites`
--
ALTER TABLE `jobs_users_favorites`
  MODIFY `job_user_favorite_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `occupations`
--
ALTER TABLE `occupations`
  MODIFY `occupation_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `skill_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skills_types`
--
ALTER TABLE `skills_types`
  MODIFY `skill_type_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `topic_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_competences`
--
ALTER TABLE `users_competences`
  MODIFY `user_competence_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_courses`
--
ALTER TABLE `users_courses`
  MODIFY `users_courses_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_courses_recommendations`
--
ALTER TABLE `users_courses_recommendations`
  MODIFY `user_course_recommendation` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_educations`
--
ALTER TABLE `users_educations`
  MODIFY `users_educations_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_experiences`
--
ALTER TABLE `users_experiences`
  MODIFY `users_experiences_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_jobs_recommendations`
--
ALTER TABLE `users_jobs_recommendations`
  MODIFY `user_job_recommendation_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_occupations`
--
ALTER TABLE `users_occupations`
  MODIFY `user_occupation_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_skills`
--
ALTER TABLE `users_skills`
  MODIFY `user_skill_id` int NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_category_constraint` FOREIGN KEY (`category_id`) REFERENCES `courses_categories` (`course_category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `courses_entity_constraint` FOREIGN KEY (`partner_id`) REFERENCES `courses_partners` (`course_partner_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `courses_language_constraint` FOREIGN KEY (`language_id`) REFERENCES `courses_languages` (`course_language_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `courses_licensetype_constraint` FOREIGN KEY (`license_id`) REFERENCES `courses_licenses` (`course_license_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `courses_relevance_constraint` FOREIGN KEY (`relevance_id`) REFERENCES `courses_relevances` (`course_relevance_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `courses_type_constraint` FOREIGN KEY (`type_id`) REFERENCES `courses_types` (`course_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `courses_user_constraint` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `courses_competences`
--
ALTER TABLE `courses_competences`
  ADD CONSTRAINT `courses_competences_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `courses_competences_ibfk_2` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`skill_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `courses_occupations`
--
ALTER TABLE `courses_occupations`
  ADD CONSTRAINT `courses_occupations_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `courses_occupations_ibfk_2` FOREIGN KEY (`occupation_id`) REFERENCES `occupations` (`occupation_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `courses_skills`
--
ALTER TABLE `courses_skills`
  ADD CONSTRAINT `courses_skills_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `courses_skills_ibfk_2` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`skill_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `courses_users_favorites`
--
ALTER TABLE `courses_users_favorites`
  ADD CONSTRAINT `courses_users_favorites_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `courses_users_favorites_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jobs`
--
ALTER TABLE `jobs`
  ADD CONSTRAINT `JB_CP_FK` FOREIGN KEY (`company_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `JB_DU_FK` FOREIGN KEY (`contract_id`) REFERENCES `jobs_contracts` (`job_contract_id`),
  ADD CONSTRAINT `JB_LG_FK` FOREIGN KEY (`language_id`) REFERENCES `jobs_languages` (`job_language_id`);

--
-- Constraints for table `jobs_occupations`
--
ALTER TABLE `jobs_occupations`
  ADD CONSTRAINT `JB_JO_FK` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`job_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `OC_JO_FK` FOREIGN KEY (`occupation_id`) REFERENCES `occupations` (`occupation_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jobs_skills`
--
ALTER TABLE `jobs_skills`
  ADD CONSTRAINT `JS_JB_FK` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`job_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `JS_SK_FK` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`skill_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jobs_users_favorites`
--
ALTER TABLE `jobs_users_favorites`
  ADD CONSTRAINT `JB_JU_FK` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`job_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `US_JU_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `skills`
--
ALTER TABLE `skills`
  ADD CONSTRAINT `skill_type_constraint` FOREIGN KEY (`skill_type_id`) REFERENCES `skills_types` (`skill_type_id`) ON DELETE RESTRICT;

--
-- Constraints for table `skills_occupations_essential_relations`
--
ALTER TABLE `skills_occupations_essential_relations`
  ADD CONSTRAINT `occupation_constraint_essential` FOREIGN KEY (`occupation_id`) REFERENCES `occupations` (`occupation_id`) ON DELETE RESTRICT,
  ADD CONSTRAINT `skill_constraint_essential` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`skill_id`) ON DELETE RESTRICT;

--
-- Constraints for table `skills_occupations_optional_relations`
--
ALTER TABLE `skills_occupations_optional_relations`
  ADD CONSTRAINT `occupation_constraint_optional` FOREIGN KEY (`occupation_id`) REFERENCES `occupations` (`occupation_id`) ON DELETE RESTRICT,
  ADD CONSTRAINT `skill_constraint_optional` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`skill_id`) ON DELETE RESTRICT;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `US_OC_FK` FOREIGN KEY (`sector_id`) REFERENCES `occupations` (`occupation_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `user_role_constraint` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `user_topic_constraint` FOREIGN KEY (`topic`) REFERENCES `topics` (`topic_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `users_competences`
--
ALTER TABLE `users_competences`
  ADD CONSTRAINT `users_competences_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD CONSTRAINT `users_competences_ibfk_2` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`skill_id`) ON DELETE CASCADE ON UPDATE RESTRICT;

--
-- Constraints for table `users_courses`
--
ALTER TABLE `users_courses`
  ADD CONSTRAINT `CR_UC_FK` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `CS_UC_FK` FOREIGN KEY (`status_id`) REFERENCES `users_courses_status` (`status_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `US_UC_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_educations`
--
ALTER TABLE `users_educations`
  ADD CONSTRAINT `users_educations_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE RESTRICT;

--
-- Constraints for table `users_experiences`
--
ALTER TABLE `users_experiences`
  ADD CONSTRAINT `users_experiences_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_skills`
--
ALTER TABLE `users_skills`
  ADD CONSTRAINT `users_skills_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_skills_ibfk_2` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`skill_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
------------------

ALTER TABLE courses ADD COLUMN sef_id INT NULL;

------------------
ALTER TABLE jobs
    ADD COLUMN manual_publish BOOL DEFAULT TRUE,
    ADD COLUMN publish_ini TIMESTAMP NULL,
    ADD COLUMN publish_end TIMESTAMP NULL;

ALTER TABLE courses
    ADD COLUMN manual_publish BOOL DEFAULT TRUE,
    ADD COLUMN publish_ini TIMESTAMP NULL,
    ADD COLUMN publish_end TIMESTAMP NULL;


ALTER TABLE jobs
    ADD COLUMN expired BOOL DEFAULT FALSE;

ALTER TABLE courses
    ADD COLUMN expired BOOL DEFAULT FALSE;

ALTER TABLE jobs
    ADD COLUMN deleted_at TIMESTAMP NULL;

ALTER TABLE courses
    ADD COLUMN deleted_at TIMESTAMP NULL;

ALTER TABLE users_courses
    ADD COLUMN deleted_at TIMESTAMP NULL;


ALTER TABLE courses CHANGE created_at old_created_at INT;

ALTER TABLE courses ADD COLUMN created_at TIMESTAMP NULL;

