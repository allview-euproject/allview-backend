# ALLVIEW BACKEND
### This repo contains the REST API of the ALLVIEW Platform, written in JavaScript using Nodejs and Expressjs

<br>

## Project overview
- server.js: REST API entry point
- utils/: utility scripts
- crons/: ESCO data extractor 
- routes/: API routes grouped by role
- middlewares/: JWT authenticator and file downloader
- controllers/: handles request data and errors 
- models/: handles server and database logic for every page
- public/: public files like profile images

<br>

## Installation

Nodejs 12 or higher with NPM must be installed. You can download the latest stable release for your operating system [here](https://nodejs.org/en/download/).

Install all the project dependencies
```
npm install
```

<br>

## Configuration
Copy the `env.example` file to `.env` and set your custom environment variables.
```
cp env.example .env
nano .env
```

The MySQL database must be configured with the schema present in this repository.


<br>

## Run
Start the REST API
```
node server.js
```

Run crons
```
node crons/ESCO.js
```

<br>

## Run with PM2
PM2 must be globally installed in your system
```
sudo npm install -g pm2
```

Start and save
```
pm2 start server.js --name allview-backend
```

Configure PM2 as a system service
```
pm2 startup
# Copy and paste the command it gives
pm2 save
```
