FROM node:16

# Create app directory
WORKDIR /usr/src/app

# Copy package.json and package-lock.json
COPY package*.json ./

# Build code for production
RUN npm ci --only=production

# Bundle app source
COPY . .

# TODO: ENV

# Runtime
CMD [ "node", "server.js" ]