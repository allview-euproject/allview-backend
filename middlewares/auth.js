const jwt = require('jwt-simple')
const db = require('../db')

const jwtCheck = async (request, response, next) => {
  // Extract token from request header
  let token = request.headers['auth-token']
  const cookies = request.headers.cookie

  if (!token && cookies) {
    const tokenMatch = cookies.match(/jwt_token=(.+?)(;|$)/)
    if (tokenMatch) {
      token = tokenMatch[1]
    }
  }

  if (!token) {
    return response.json({
      success: false,
      message: 'Authentication failed: header Auth-Token not found.'
    })
  }

  // Token validation and data decryption
  let payload
  try {
    payload = jwt.decode(token, process.env.JWT_TOKEN)
  } catch (error) {
    return response.json({
      success: false,
      message: 'Authentication failed: wrong Auth-Token header value.'
    })
  }

  // Checks that the token has not expired
  if (new Date().getTime() > payload.expiresAt) {
    return response.json({
      success: false,
      message: 'Authentication failed: expired Auth-Token header value.'
    })
  }

  // Checks if the user that is making the request is in its correct user role
  let requestedUserRole
  if (request.path.includes('/people/')) {
    requestedUserRole = 'people'
  } else if (request.path.includes('/company/')) {
    requestedUserRole = 'company'
  } else if (request.path.includes('/training-provider/')) {
    requestedUserRole = 'training-provider'
  } else if (request.path.includes('/admin/')) {
    requestedUserRole = 'admin'
  }
  if (requestedUserRole) {
    const testUserRole = await db.query('SELECT user_id FROM users INNER JOIN roles ON users.role_id = roles.role_id WHERE user_id = ? AND (role = ? OR role = ?)', [payload.userId, requestedUserRole,"admin"])
    if (testUserRole.length === 0) {
      return response.json({
        success: false,
        message: 'Authentication failed: wrong user role.'
      })
    }
  }

  // Includes the decrypted payload in the request
  request.payload = payload
  next()
}

module.exports = {
  jwtCheck
}
