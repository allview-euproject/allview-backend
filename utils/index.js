const crypto = require('crypto')

module.exports = {
  validateEmail: (email) => {
    if (email.match(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/)) {
      return true
    } else {
      return false
    }
  },
  isPasswordSecure: (password) => {
    if (password.length >= 8) {
      let securityLevel = 0
      if (password.match(/\d/)) securityLevel++ // Numbers
      if (password.match(/[A-Za-z]/)) securityLevel++ // Letters
      if (password.match(/[-!$%^&*()_+|~=`{}\[\]:";'<>?,./]/)) securityLevel++ // Symbols

      if (securityLevel >= 2) return true // If the password has number & symbols, or number & letters, or letters & symbols
    }
    throw new Error('Password is not secure: it must contain at least 8 characters with number & letters, or numbers & symbols, or letters & symbols')
  },
  sleep: (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  },
  capitalize: (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
  },
  randomRame: (len) => {
    return crypto.randomBytes(len).toString('hex')
  },
  isNumeric: (value) => {
    return /^-?\d+$/.test(value)
  }
}
