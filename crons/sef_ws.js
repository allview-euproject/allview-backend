

const dotenv = require("dotenv");
dotenv.config({path:"../.env"});
const fs = require("fs");
const https = require("https");
const zlib = require("zlib");

const db = require('../db')
const utils = require('../utils')
const {errorFunc} = require("express-fileupload/lib/utilities");

class CourseItem{
    /*
    VEXPECURSO: string;
    VDENOMINA: string;
    DFECINICIO: string;
    DFECFIN: string;
    MUNICIPIOTXT: string;
    PROGRAMATXT: string;
    VDENMOFOR: string;
    NDURACION: string;
    NALUMNOS: string;
    VESPECIA: string;
    VNIVELPROTEC: string;
    VNIVELFISICO: string;
    VHORARIO: string;
    FAMILIAPROFTXT: string;
    ENTIDADTXT: string;
    SECTORTXT: string;
    NIVELACADEMICOTXT: string;
    AREAPROFTXT: string;
    AGRUPACIONSECTOR: string;
*/


    constructor(values) {
        this.course_id = 0;
        this.sef_id = values.id;// int(11) DEFAULT NULL,

        //this.course_id = MIRA TABLA;// int(11) NOT NULL AUTO_INCREMENT, //

//        this.title = ;// varchar(4000) DEFAULT NULL,

        this.VDENOMINA = typeof values[1] === 'object' ? '' : values[1] || '';
        this.name = typeof values[1] === 'object' ? '' : values[1] || '';

        this.VESPECIA = typeof values[9] === 'object' ? '' : values[9] || '';
        this.description = typeof values[9] === 'object' ? '' : values[2] || '';

        // values.familiaProfesional ? values.familiaProfesional.denominacion

        this.duration = values.duracion;// varchar(255) DEFAULT NULL
        this.init_date =  values.fechaInicio;// datetime DEFAULT NULL,
        this.end_date =  values.fechaFin;// datetime DEFAULT NULL,



        this.VEXPECURSO = typeof values[0] === 'object' ? '' : values[0] || '';

        this.DFECFIN = typeof values[3] === 'object' ? '' : values[3] || '';
        this.MUNICIPIOTXT = typeof values[4] === 'object' ? '' : values[4] || '';
        this.PROGRAMATXT = typeof values[5] === 'object' ? '' : values[5] || '';
        this.VDENMOFOR = typeof values[6] === 'object' ? '' : values[6] || '';
        this.NDURACION = typeof values[7] === 'object' ? '' : values[7] || '';
        this.NALUMNOS = typeof values[8] === 'object' ? '' : values[8] || '';

        this.VNIVELPROTEC = typeof values[10] === 'object' ? '' : values[10] || '';
        this.VNIVELFISICO = typeof values[11] === 'object' ? '' : values[11] || '';
        this.VHORARIO = typeof values[12] === 'object' ? '' : values[12] || '';
        this.FAMILIAPROFTXT = typeof values[13] === 'object' ? '' : values[13] || '';
        this.ENTIDADTXT = typeof values[14] === 'object' ? '' : values[14] || '';
        this.SECTORTXT = typeof values[15] === 'object' ? '' : values[15] || '';
        this.NIVELACADEMICOTXT = typeof values[16] === 'object' ? '' : values[16] || '';
        this.AREAPROFTXT = typeof values[17] === 'object' ? '' : values[17] || '';
        this.AGRUPACIONSECTOR = typeof values[18] === 'object' ? '' : values[18] || '';
    }

}


const sef_websrvice = () => {
    const soap = require("soap");


// URL del servicio web SOAP
    const url = 'https://jad-inter.carm.es/jAD/webservice/EU_SEF/wsCursosFormacion?wsdl';

    const certPath = './external_cert/carm.es.cer';
    const caCert = fs.readFileSync(certPath);
    console.log('La ruta absoluta del directorio actual es:', process.cwd());
// Crear un agente HTTPS con el certificado
    const agent = new https.Agent({
        ca: caCert,
        rejectUnauthorized: false // Añade esta línea si quieres permitir certificados no autorizados
    });

    const options = {
        agent: agent
    };

    console.log('1');
    soap.createClientAsync(url, {})
        .then((client) => {
            //console.log('Cliente creado',client);
            // Ahora puedes usar el cliente SOAP como lo harías normalmente

            client.wsCursosFormacion.wsCursosFormacionPort.wsCursosFormacion({}, (err, result, rawResponse, soapHeader, rawRequest) => {
                if (err) {
                    console.error('Error al llamar a la operación del servicio SOAP:', err);
                } else {
                    // Aquí puedes acceder a los datos de la respuesta
                    const responseData = result.return;
                    // console.log('Respuesta del servicio:', responseData);
                    // console.log('Resultado:', result);
                    // console.log('Resultado -- :', result.return.valores[0]);
                    // console.log('Resultado:', responseData.valores[1]);
                    // console.log('Respuesta SOAP sin procesar:', rawResponse);
                    // console.log('Encabezado SOAP:', soapHeader);
                    // console.log('Solicitud SOAP sin procesar:', rawRequest);

                    //Column0 -> VEXPECURSO, Column1 -> VDENOMINA, Column2 -> DFECINICIO , etc. Los campos son:
                    // VEXPECURSO, VDENOMINA, DFECINICIO, DFECFIN, MUNICIPIOTXT, PROGRAMATXT, VDENMOFOR,
                    // NDURACION, NALUMNOS, VESPECIA, VNIVELPROTEC, VNIVELFISICO, VHORARIO, FAMILIAPROFTXT,
                    // ENTIDADTXT, SECTORTXT, NIVELACADEMICOTXT, AREAPROFTXT, AGRUPACIONSECTOR.


                    const parseString = require('xml2js').parseString;

                    parseString(rawResponse, function (err, result) {
                        const headers = result['soap:Envelope']['soap:Body'][0]['ns2:wsCursosFormacionResponse'][0]['return'][0]['cabeceras'];
                        const body = result['soap:Envelope']['soap:Body'][0]['ns2:wsCursosFormacionResponse'][0]['return'][0]['valores'];
                        //console.dir(body)
                        //console.log(JSON.stringify());
                        let resultCourse = [];
                        body.forEach((json_item) => {
                            const course = new CourseItem(json_item.item);
                            resultCourse.push(course)
                        });

                        console.log(resultCourse[0]);
                        //console.log(resultCourse[30]);

                    });
                }
            });
        }).catch((err) => {
        console.error('2', err);
    });
    console.log('3');
}

// sef_websrvice();



class CourseItemRest{

    constructor(values) {
        this.job_id = 0;
        this.sef_id = values.id;

    }

}

const sep_rest_api = (raw_equivs,sef_info) => {
    console.log("Iniciando consulta al sep_rest_api");

    var https = require('follow-redirects').https;


    var options = {
        'method': 'GET',
        'hostname': 'sefapps.carm.es',
        'path': '/sefApps/rest/cursos/cursos',
        'headers': {
            'Accept-Encoding': 'gzip, deflate, br',

        },
        'maxRedirects': 20
    };

    var req = https.request(options,  (res) => {
        var chunks = [];

        res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        res.on("end",  (chunk) => {
            let buffer = Buffer.concat(chunks);

            console.log("Consulta recibida.. descomprimiendo gzip");//,res.headers)
            // Verifica si la respuesta está codificada con gzip
            zlib.gunzip(buffer, (err, dezipped) => {
                if (err) {
                    console.error("Error descomprimiendo la respuesta:", err);
                    writeLogFile("Error descomprimiendo la respuesta:"+ JSON.stringify(err)+"\n\n"+buffer);
                    process.exit(-6);
                }

                // Intenta parsear el JSON descomprimido
                try {
                    //let json = JSON.parse(dezipped.toString());
                    //console.log(JSON.stringify(json[0], null, 2)); // Imprime de forma legible

                    console.log("Descomprimido con éxito ");

                    traduce_cursos(raw_equivs,dezipped.toString(),sef_info).then(
                        (ok)=>{
                            console.log("> END");
                            process.exit();
                        },
                        (error)=>{
                            console.log("traduce_cursos Error",error);
                            writeLogFile("traduce_cursos Error"+JSON.stringify(error));
                            process.exit(-8);
                        }
                    );


                } catch (e) {
                    console.error("Error al parsear JSON:", e);
                    writeLogFile("Error al parsear JSON:"+ JSON.stringify(e));
                    process.exit(-7);
                }
            });
        });

        res.on("error", function (error) {
            console.error(error);
        });
    });

    req.end();

}


const traduce_cursos = async (raw_json_equivs, raw_json_courses, sef_info) => {
    console.log("-- Comenzamos el parseo..");
    const json_equivs = JSON.parse(raw_json_equivs);
    const json_courses = JSON.parse(raw_json_courses);
    if (json_equivs.terms && json_equivs.terms.length > 0) {
        console.log("Preparando equivalencias: " + json_equivs.terms.length);

        const sefHashMap = {};
        // Usar un ciclo `for` en lugar de `forEach` para iterar sobre `data.terms`
        for (let i = 0; i < json_equivs.terms.length; i++) {
            const term = json_equivs.terms[i];
            if(!term.skill_competences_ids){
                for (let t of term.skill_competences){
                    t.title = t.trim()
                }
                const query = 'SELECT skill_name,skill_id FROM skills WHERE skill_name IN (?)';
                const res = await db.query(query,[term.skill_competences])
                if(res.length > 0){
                    term.skill_competences_ids = {};
                    term.skill_competences_ids_array = []
                    for(const row of res){
                        term.skill_competences_ids[row['skill_name']] = row['skill_id']
                        term.skill_competences_ids_array.push(row['skill_id'])
                    }
                    //console.log(term)
                }else{
                    term.skill_competences_ids = {};
                    term.skill_competences_ids_array = []
                }
            }
            // Otro ciclo `for` para iterar sobre los elementos `sef` dentro de cada término
            for (let j = 0; j < term.sef.length; j++) {
                const sefItem = term.sef[j];
                let top_ids = [];
                for (let o = 0; o < term.topics.length; o++) {
                    let topic = term.topics[o].toUpperCase();
                    if (sef_info.topic_hash[topic])
                        top_ids.push(sef_info.topic_hash[topic])
                }

                sefHashMap[sefItem.toUpperCase()] = {
                    topics: term.topics,
                    topics_ids: top_ids,
                    keywords: term.keywords.join(', '),
                    skill_competences: term.skill_competences,
                    skill_competences_ids: term.skill_competences_ids,
                    skill_competences_ids_array: term.skill_competences_ids_array
                };
            }
        }
        console.log('Indexado de equivalencias terminado...');

        console.log("Intentando traducir: " + json_courses.length + " cursos");
        let insert_count = 0;
        let without_familiaProfesional = 0;
        let unknown_familiaProfesional = 0;
        let already_inserted = 0;
        let denominaciones_desconocidas = [];
        //let types = {}
        for (let i = 0; i < json_courses.length; i++) {
        //for (let i = 0; i < 20; i++) {
            let sef_course = json_courses[i];
                // try {
                //     types[sef_course.modalidad.id] = sef_course.modalidad.denominacion;
                // } catch (e) {
                //     console.error(e);
                //     console.log("course", JSON.stringify(sef_course))
                //     console.log("---------------------\n")
                // }

            if (sef_info.courses.includes(sef_course.id)) {
              //  console.log("Curso ya insertado " + sef_course.id);
                already_inserted++
                // Encuentra el índice de sef_course.id en el array
                let index = sef_info.courses.indexOf(sef_course.id);
                if (index !== -1) {
                    sef_info.courses.splice(index, 1);
                }
            } else if (sef_course.familiaProfesional == undefined || sef_course.familiaProfesional.denominacion == undefined) {
                // console.error("curso SIN denominacion de familia profesional", JSON.stringify(sef_course))
                without_familiaProfesional++

            } else {
                let equi = sefHashMap[sef_course.familiaProfesional.denominacion.toUpperCase()]
                if (equi == undefined) {
                    // console.error("curso con denominacion de familia profesional DESCONOCIDA", JSON.stringify(sef_course))
                    //console.error("curso con denominacion de familia profesional DESCONOCIDA", sef_course.familiaProfesional.denominacion)
                    if(!denominaciones_desconocidas.includes(sef_course.familiaProfesional.denominacion)){
                        denominaciones_desconocidas.push(sef_course.familiaProfesional.denominacion)
                    }
                    unknown_familiaProfesional++;
                } else {
                    //console.log("sef", JSON.stringify(sef_course));
                    let cur = {
                        "sef_id": sef_course.id,
                        //"course_id":null,
                        "category_id":null,//"category_id": equi.categoria[0],
                        "title": sef_course.denominacion,
                        "description": (sef_course.expediente ? "Exp: " + sef_course.expediente+"</br>":"") + (sef_course.areaTxt ? "Área: " + sef_course.areaTxt + "</br>":"") + (sef_course.nivelAcademicoTxt? "Nivel académico requerido: </br>" + sef_course.nivelAcademicoTxt + "</br>":"") + ( sef_course.especialidadTxt ? sef_course.especialidadTxt + "</br>":"")+(sef_course.horario ? "</br></br><hr></br>" + sef_course.horario : ""),
                        //"skill_competences": equi.skill_competences,
                        "keywords": equi.keywords,

                        "topics": equi.topics,
                        "topics_ids": equi.topics_ids,

                        "type_id": sef_course.modalidad ? sef_info.typesHashMap[sef_course.modalidad.id] : null,
                        "institution": sef_course.entidadTxt,
                        "languages": ["Spanish"],
                        "created_at": parseFechaMilisCreatedAt(Date.now()),
                        //{"partner_id":sef_partner,"user_id":sef_account.user_id,"courses":courses}
                        "user_id": sef_info.user_id,
                        "partner_id": sef_info.partner_id,
                        "image": "sef-logo.png",
                        "license_id": sef_info.license_id,
                        "access": "Free",
                        "duration": sef_course.duration,
                        "contact_mail": "sef.necesidadesformativas@carm.es",
                        //"weblink":"https://www.sefcarm.es/web/pagina?IDCONTENIDO=70740&IDTIPO=100&RASTRO=c$m70716",
                        "weblink": "https://sefapps.carm.es/sefApps/html5/index.html#!/detalle-curso/" + sef_course.id,
                        "init_date": sef_course.fechaInicio ? parseFecha(sef_course.fechaInicio): null,
                        "end_date": sef_course.fechaFin ? parseFecha(sef_course.fechaFin): null,
                        "skills_ids": equi.skill_competences_ids,
                        "skills_ids_array": equi.skill_competences_ids_array
                    }

                    // console.log("\n\n Allview:",JSON.stringify(allv_course));

                    const insertResponse = await db.query(
                        "INSERT INTO courses (sef_id,init_date,end_date,title,duration, description, contact_mail, weblink, institution, keywords, access," +
                        "created_at, partner_id, type_id, category_id, license_id, languages, receive_candidates_recommendations, user_id,image) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                        [cur.sef_id, cur.init_date, cur.end_date, cur.title, cur.duration, cur.description, cur.contact_mail, cur.weblink, cur.institution,cur.keywords, cur.access,
                        cur.created_at, cur.partner_id, cur.type_id, cur.category_id, cur.license_id, JSON.stringify(cur.languages), false, cur.user_id,cur.image])

                    let courseId = insertResponse.insertId
                    // console.log("courseId",courseId)
                    // console.log("cur.topics_ids",cur.topics_ids)
                    if (cur.topics_ids.length > 0){
                        let courses_topics = "INSERT INTO courses_topics (course_id,topic_id) VALUES ?";
                        let values = [];
                        for(let p = 0 ; p < cur.topics_ids.length; p++){
                           values.push([courseId,cur.topics_ids[p]])
                        }
                        //console.log("topics",values)
                        await db.query(courses_topics,[values]);
                    }

                    if (cur.skills_ids_array.length > 0){
                        let courses_skills = "INSERT INTO courses_skills (course_id,skill_id) VALUES ?";
                        let values = [];
                        for(let p = 0 ; p < cur.skills_ids_array.length; p++){
                            values.push([courseId,cur.skills_ids_array[p]])
                        }
                        //console.log("skills",values)
                        await db.query(courses_skills,[values]);
                    }

                    // category

                    insert_count++;
                }
            }
        }

        if(sef_info.courses.length > 0) {
            // console.log("obsoletos",JSON.stringify(sef_info.courses))
            const softDelete = await db.query("UPDATE courses SET deleted_at = NOW() WHERE sef_id IN (?)", [sef_info.courses])
            // console.log("deleted",softDelete)
        }

        const soft_deleted_sef = await db.query("SELECT COUNT(*) as c FROM courses WHERE deleted_at IS NOT NULL AND sef_id IS NOT NULL", [sef_info.courses])

        //console.log("soft_deleted_sef",JSON.stringify(soft_deleted_sef))


        let string = "--------------- RESULTADOS ---------------\n"
        string += json_courses.length+"\t- total de cursos descargados \n";
        string += already_inserted+"\t- already_inserted\n";
        string += insert_count+"\t- insertados\n";
        string += without_familiaProfesional +"\t- without_familiaProfesional\n";
        string += unknown_familiaProfesional +"\t- unknown_familiaProfesional\n";

        string += "\n------ denominaciones_desconocidas ------\n";
        string += denominaciones_desconocidas+"\n";

        string += "\n------ cursos que ya no aparecen --> los eliminados ------\n";
        string += "cursos obsoletos en esta sincronización: "+sef_info.courses.length+"\n";
        string += "cursos obsoletos del sef soft_deleted en bbdd (incluidos los de ahora): "+soft_deleted_sef[0]['c']+"\n";
        string += "--> FIN IMPORTACION <--\n";

        console.log(string);
        writeLogFile(string);
        // courses 209
//1484
    } else {
        console.error("Error al cargar la tabla de equivalencias, hay 0 Equivalencias")
        writeLogFile("Error al cargar la tabla de equivalencias, hay 0 Equivalencias");
        process.exit(-10);
    }
}

const writeLogFile = (string) =>{
    try {
        let rutaArchivo = "./SEF_cron_result.txt";
        if (fs.existsSync(rutaArchivo)) {
            const stats = fs.statSync(rutaArchivo);
            const creacion = new Date(stats.birthtimeMs);

            console.log("creacion",creacion)
            let nueva = rutaArchivo.replace(".txt","_"+creacion.getDate()+".txt")
            fs.renameSync(rutaArchivo, nueva);
        }
        const now = new Date();
        string = "SEF CRON:  Última ejecución: "+now.toLocaleString('es-ES')+"\n\n"+string;
        console.log("string",rutaArchivo,string)
        fs.writeFileSync(rutaArchivo, string);
    }catch (e){
        console.error("Error al escribir en el archivo de resultados:", e);
    }
}

const parseFecha = (fechaStr) =>{
    const partes = fechaStr.split('/');
// Reordenar las partes para cumplir con el formato MM/DD/YYYY
    const fechaReformateada = `${partes[1]}/${partes[0]}/${partes[2]}`;

// Crear un objeto de fecha a partir de la cadena reformateada
    const fecha = new Date(fechaReformateada);

// Obtener el timestamp
    return fecha.toISOString().slice(0, 19).replace('T', ' ');
}

const parseFechaMilisCreatedAt = (fechaMilis) =>{
// Crear un objeto de fecha a partir de la cadena reformateada
    const fecha = new Date(fechaMilis);

// Obtener el timestamp
    return fecha.toISOString().slice(0, 19).replace('T', ' ');
}

const load_sef_json_equivs = (sef_info ) => {
    // Ruta al archivo JSON que deseas cargar
    const filePath = './sef_equivalences.json';

    console.log("Cargando archivo de equivalencias_sef: "+filePath);

// Lee el archivo de manera asíncrona
    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            console.error('Error al leer el archivo:', err);
            writeLogFile('Error al leer el archivo:'+ JSON.stringify(err));
            process.exit(-4);
        }
        // Parsea la cadena JSON a un objeto JavaScript
        try {
            //console.log(jsonData); // Aquí puedes trabajar con el objeto JSON
            console.log("Fichero cargado correctamente")

            sep_rest_api(data,sef_info);
        } catch (parseErr) {
            console.error('Error al parsear JSON:', parseErr);
            writeLogFile('Error al parsear JSON:'+ JSON.stringify(parseErr));
            process.exit(-5);
        }
    });
}

console.log("iniciando proceso sincronización con sef")




const check_db = async () => {

    try{
        // chequeamos que existe el campo y los que ya existen
        let courses = await db.query('SELECT sef_id FROM courses WHERE sef_id IS NOT NULL AND deleted_at IS NULL');
        courses =  courses.map(packet => packet.sef_id);

        // comprobamos que existe al menos un usuaio de seb al que asignarle los cursos
        let sef_account_id = await db.query('SELECT * FROM users WHERE full_name like "SEF%" AND role_id = 2 limit 1');
        if(sef_account_id.length == 0){
            throw ("Tiene que haber dado de alta al menos un usuario con full_name SEF para poder realizar esta operación")
        }else{
            sef_account_id = sef_account_id[0].user_id;
        }
        let sef_partner_id = await db.query('SELECT * FROM courses_partners WHERE partner like "SEF%" limit 1');
        if(sef_partner_id.length == 0){
            let id = await db.query('INSERT INTO courses_partners (partner) VALUES ("SEF - Servicio de Empleo y Formación")');
            sef_partner_id = id.insertId;
            console.log(sef_partner_id)
        }else{
            sef_partner_id = sef_partner_id[0].course_partner_id;
        }

        let license_id = await db.query('SELECT course_license_id FROM courses_licenses WHERE license = "Admission requirements (more details on web page)" limit 1');
        if(license_id.length == 0){
            let id = await db.query('INSERT INTO courses_licenses (license) VALUES ("Admission requirements (more details on web page)")');
            license_id = id.insertId;
            console.log(license_id)
        }else{
            license_id = license_id[0].course_license_id;
        }

        if (fs.existsSync("../public/images/courses/sef-logo.png")) {
            console.log('El logo de sef existe.');
        } else {
            console.error('El  logo de sef no existe. lo copiamos');
            try {
                // Copiar el archivo
                fs.copyFileSync("./sef-logo.png", "../public/images/courses/sef-logo.png");
                console.log('Archivo copiado exitosamente.');
            } catch (error) {
                throw ('Error al copiar el archivo:'+e.toString())
            }
        }

        let topicsHashMap = {};
        let topics = await db.query('SELECT UPPER(topic) as topic,topic_id FROM topics');
        for (let j = 0; j < topics.length; j++) {
            const top = topics[j];
            topicsHashMap[top.topic] = top.topic_id;
        }

        // let course_types = {
        //     TL: 'Teleformación',
        //     PR: 'Presencial',
        //     MX: 'Mixta'
        // }

        let typesHashMap = {};
        let term = await db.query('SELECT topic,topic_id FROM topics WHERE "Complete online course"')
        typesHashMap["TL"]= term.length > 0 ? term[0].topic_id : 1;
        term = await db.query('SELECT topic,topic_id FROM topics WHERE "Onsite"')
        typesHashMap["PR"]= term.length > 0 ? term[0].topic_id : 13;
        term = await db.query('SELECT topic,topic_id FROM topics WHERE "Complete blended learning course"')
        typesHashMap["MX"]= term.length > 0 ? term[0].topic_id : 5;

        let info = {
            "partner_id":sef_partner_id,
            "user_id":sef_account_id,
            "courses":courses,
            "license_id":license_id,
            "topic_hash":topicsHashMap,
            "typesHashMap":typesHashMap
        };

        //console.log("sef_info",info)
        return info;
    }catch (e){
        if(e.sqlMessage === "Unknown column 'sef_id' in 'field list'"){
            try{
                await db.query('ALTER TABLE courses ADD COLUMN sef_id INT NULL');
                console.log("Añadida columna sef_id")
                check_db();
            }catch (e){
                console.error("Error intentando alterar la tabla ",e)
                writeLogFile("Error intentando alterar la tabla "+JSON.stringify(e));
                process.exit(-2);
            }

        }else{
            console.error("Error",e)
            writeLogFile("Error "+JSON.stringify(e));
            process.exit(-1);
        }
    }

}


check_db().then(
    (res)=>{
        //console.log("courses ",JSON.stringify(res))
        load_sef_json_equivs(res)
    },
    (err)=>{
        console.log("error ",err)
        writeLogFile("Error "+JSON.stringify(err));
        process.exit(-3);
    })
/*

UNION ALL SELECT 'Create artwork'
UNION ALL SELECT 'communication, collaboration and creativity'
UNION ALL SELECT 'Designing systems and products'
UNION ALL SELECT 'Processing information, ideas an competences'
UNION ALL SELECT 'Dealing with problems'
UNION ALL SELECT 'Self-management skills and competences'
UNION ALL SELECT 'Create digital content'
UNION ALL SELECT ''
UNION ALL SELECT 'Developing objectives and strategies'
UNION ALL SELECT 'Developing strategies to solve problems'
UNION ALL SELECT 'Plan medium to long term objectives'
UNION ALL SELECT 'Working efficiently'
UNION ALL SELECT 'Planning and organising'
UNION ALL SELECT 'Processing information, ideas and concepts'
UNION ALL SELECT ''
UNION ALL SELECT 'Building and repairing  structures'
UNION ALL SELECT 'Installing structural masonry materials'
UNION ALL SELECT 'Prepare site for construction'
UNION ALL SELECT 'Installing wooden and metal components'
UNION ALL SELECT 'Manipulating and controlling objects and competences'
UNION ALL SELECT 'Planning and organising'
UNION ALL SELECT 'Processing information, ideas and concepts'
UNION ALL SELECT ''
UNION ALL SELECT 'Working with computers'
UNION ALL SELECT 'Programming computer systems'
UNION ALL SELECT 'Installing, maintaining and repairing electrical, electronic and preicision equipment'
UNION ALL SELECT 'Domotic systems'
UNION ALL SELECT 'Working with digital devices and application'
UNION ALL SELECT 'Planning and organising'
UNION ALL SELECT ''
UNION ALL SELECT 'Operating precision industrial equipment'
UNION ALL SELECT 'Use non-destructive testing equiment'
UNION ALL SELECT 'Operating metal, plastic or rubber forming equipment'
UNION ALL SELECT 'Installing plumbing or piping equipment or systems'
UNION ALL SELECT 'Apply thermite welding techniques'
UNION ALL SELECT 'Use equipment, tools or technology with precision'
UNION ALL SELECT 'Manipulating and controlling objects and competences'
UNION ALL SELECT ''
UNION ALL SELECT ''
UNION ALL SELECT 'Setting up computer systems'
UNION ALL SELECT 'Programming computer systems'
UNION ALL SELECT 'Accesing and analysing digital data'
UNION ALL SELECT 'Browsing, searching and filtering digital data'
UNION ALL SELECT 'Information and communication technologies (icts)'
UNION ALL SELECT 'Emergent technologies'
UNION ALL SELECT 'Thinking creatively and innovatively'
UNION ALL SELECT 'Use equipment, tools or technology with precision'
UNION ALL SELECT 'Working with digital devices and application'
UNION ALL SELECT ''
UNION ALL SELECT 'Programming computer systems'
UNION ALL SELECT 'Set up automative robot'
UNION ALL SELECT 'Domotic systems'
UNION ALL SELECT 'Use equipment, tools or technology with precision'
UNION ALL SELECT 'Manipulating and controlling objects and quipment'
UNION ALL SELECT 'Responding to physical circumstances'
UNION ALL SELECT ''
UNION ALL SELECT 'Using hand tools'
UNION ALL SELECT 'Shaping materials to create products'
UNION ALL SELECT 'Manipulate wood'
UNION ALL SELECT 'Manipulating and controlling objects and quipment'
UNION ALL SELECT 'Responding to physical circumstances'
UNION ALL SELECT ''
UNION ALL SELECT 'Solving problem'
UNION ALL SELECT 'Developing solutions'
UNION ALL SELECT 'advising on environmental issues'
UNION ALL SELECT 'advise on nature conservation'
UNION ALL SELECT 'advise on pollution prevention'
UNION ALL SELECT 'Applying general knowledge'
UNION ALL SELECT 'Applaying environmental skills and competences'
UNION ALL SELECT ''
UNION ALL SELECT ''
UNION ALL SELECT 'Operating metal, plastic or rubber forming equipment'
UNION ALL SELECT 'Finish plastic products'
UNION ALL SELECT 'Monitor chemical process condition'
UNION ALL SELECT 'Responding to physical circumstances'
UNION ALL SELECT 'Manipulating and controlling objects and equipment'
UNION ALL SELECT ''
UNION ALL SELECT ''
UNION ALL SELECT ''
UNION ALL SELECT 'Working with computers'
UNION ALL SELECT 'Setting up and protecting computer systems'
UNION ALL SELECT 'Emergent technologies'
UNION ALL SELECT 'Working with digital devices and applications'
UNION ALL SELECT 'Computer use'
UNION ALL SELECT 'Taking a proactive approach'
UNION ALL SELECT 'planing and organising'
UNION ALL SELECT ''
UNION ALL SELECT ''
UNION ALL SELECT 'Working with computers'
UNION ALL SELECT 'Working with digital devices and applications'
UNION ALL SELECT 'Thinking creatively and innovatively'
UNION ALL SELECT 'Working with digital devices and application'
UNION ALL SELECT ''
UNION ALL SELECT 'Developing objectives and strategies'
UNION ALL SELECT 'Developing strategies to solve problems'
UNION ALL SELECT 'Working efficiently'
UNION ALL SELECT 'Planning and organising'

* */
//sep_rest_api();
/*

async function createSoapClient() {
    try {
       const certPath = './external_cert/carm.es.cer';
        const url = 'https://jad-inter.carm.es/jAD/webservice/EU_SEF/wsCursosFormacion?wsdl';

        // Leer el certificado desde el archivo
        const caCert = fs.readFileSync(certPath);

        // Crear un agente HTTPS con el certificado
        const agent = new https.Agent({
            ca: caCert,
            rejectUnauthorized: false // Añade esta línea si quieres permitir certificados no autorizados
        });

        const options = {
            request: {
                agent: agent
            }
        };

        const client = await soap.createClientAsync(url, options);
        console.log('Cliente SOAP creado con éxito');

        // Ahora puedes usar el cliente SOAP como lo harías normalmente
        client.wsCursosFormacion({}, (err, result) => {
            if (err) {
                console.error('Error al llamar a la operación del servicio SOAP:', err);
            } else {
                // Aquí puedes acceder a los datos de la respuesta
                const responseData = result.return;
                console.log('Respuesta del servicio:', responseData);
            }
        });

    } catch (err) {
        console.error('Error al crear el cliente SOAP:', err);
    }
}

createSoapClient();
 */