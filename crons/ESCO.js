const fetch = require('node-fetch')
const dotenv = require("dotenv");
dotenv.config({path:"../.env"});
const db = require('../db')
const csv = require('csvtojson')
const utils = require('../utils')
const fs = require("fs");
const {string} = require("sharp/lib/is");
const {all} = require("express/lib/application");

const SKILL_LIST = 'https://ec.europa.eu/esco/api/resource/skill?isInScheme=http://data.europa.eu/esco/concept-scheme/member-skills&language=en&limit=$limit&offset=$offset'
const OCCUPATION_LIST = 'https://ec.europa.eu/esco/api/resource/occupation?isInScheme=http://data.europa.eu/esco/concept-scheme/isco&language=en&limit=$limit&offset=$offset'


const ESCO_VERSION = "v1.1.2"
const ESCO_FOLDER = "./esco_files_v1_1_2"

// Inserts and updates the occupations table
const updateOccupations = async () => {
  const occupationsHrefs = await getAllOccupationHrefs()
  for (const occupationHref of occupationsHrefs) {
    try {
      const occupation = await getOccupationData(occupationHref)
      console.log('> Inserting/updating', occupation.name)

      // Occupation level
      const occupationLevel = occupation.code.replace('.', '').length - 1

      // Occupation parent
      let occupationParent = null
      if (occupation.parent_uri) {
        const occupationParentSearch = await db.query('SELECT occupation_id FROM occupations WHERE occupation_uri = ?', occupation.parent_uri)
        if (occupationParentSearch.length > 0) {
          occupationParent = occupationParentSearch[0].occupation_id
        }
      }

      // Insert/update
      await db.query('INSERT INTO occupations (occupation_name, occupation_description, occupation_code, occupation_uri, occupation_href, occupation_parent, occupation_level) VALUES(?,?,?,?,?,?,?)' +
        'ON DUPLICATE KEY UPDATE occupation_name = ?, occupation_description = ?, occupation_code = ?, occupation_uri = ?, occupation_href = ?, occupation_parent = ?, occupation_level = ?',
      [occupation.name, occupation.description, occupation.code, occupation.uri, occupation.href, occupationParent, occupationLevel, occupation.name, occupation.description, occupation.code, occupation.uri, occupation.href, occupationParent, occupationLevel])
    } catch (error) {
      console.log('< ' + error.message)
    }
  }
}

// Extracts the important data from a occupation
const getOccupationData = async (href) => {
  let res = await fetch(href)
  res = await res.json()

  // Parent occupation data
  let parentHref = null
  let parentUri = null
  if (res._links.broaderConcept || res._links.broaderHierarchyConcept || res._links.broaderIscoGroup || res._links.broaderOccupation) {
    parentHref = (res._links.broaderConcept || res._links.broaderHierarchyConcept || res._links.broaderIscoGroup || res._links.broaderOccupation)[0].href // TODO one to many?
    parentUri = (res._links.broaderConcept || res._links.broaderHierarchyConcept || res._links.broaderIscoGroup || res._links.broaderOccupation)[0].uri // TODO one to many?
  }

  // Child occupation data
  const childOccupations = []
  if (res._links.narrowerOccupation) {
    for (const childOccupation of res._links.narrowerOccupation) {
      childOccupations.push(childOccupation.href)
    }
  }

  const concept = {
    href: href,
    uri: res.uri,
    name: res.title,
    description: res.description.en.literal,
    code: res.code,
    parent_href: parentHref,
    parent_uri: parentUri,
    childOccupations: childOccupations
  }
  return concept
}

// Gets all available skills, by its href link
const getAllOccupationHrefs = async () => {
  // These vars will be used to iterate through all the data
  const limit = 20
  let offset = 0
  const occupations = []
  console.time('getAllOccupationHref')
  let maxTrials = 40

  // Keep fetching data until all data has been fetched
  while (true) {
    if (maxTrials === 0) break

    // Request to the ESCO API, using a different offset for each iteration
    let res = await fetch(OCCUPATION_LIST.replace('$offset', offset).replace('$limit', limit))
    console.log('Request status:', res.status)
    res = await res.json()

    // Sometimes the API returns an empty response. Rertry and decrease the maxTrials value
    if (res.concepts.length === 0) {
      maxTrials--
      console.log('Retrying', maxTrials)
      continue
    }

    // Update offset. Offset value is counted as blocks of limit values
    offset++

    // Save the occupation hrefs present in the response (XXXX)
    for (const concept of res.concepts) {
      occupations.push(concept.href)
      // The occupation can have children occupations, push them too (XXXX.X)
      const occupationData = await getOccupationData(concept.href)
      for (const childOccupation of occupationData.childOccupations) {
        occupations.push(childOccupation)
        // The child occupations may also have children (XXXX.X.X)
        const occupationData = await getOccupationData(childOccupation)
        for (const childOccupation of occupationData.childOccupations) {
          occupations.push(childOccupation)
        }
      }
    }

    // The end of the occupations list has been reached. Exit
    if (res.concepts.length < limit) break
  }
  console.log('Occupations length', [...new Set(occupations)].length)
  console.timeEnd('getAllOccupationHref')
  // Remove duplicates
  return [...new Set(occupations)]
}

// Sets a 1 in occupation_is_last_level if the occupation has not any child occupations
const updateLastLevelOccupations = async () => {
  const occupations = await db.query('SELECT occupation_id FROM occupations')

  console.time('updateLastLevelOccupations -- end')
  console.log('> Updating last-level occupations ||')

  for (const occupation of occupations) {
    console.log('occupation',occupation)
    const childOccupations = await db.query('SELECT occupation_id FROM occupations WHERE occupation_parent = ?', occupation.occupation_id)
    if (childOccupations.length === 0) {
      await db.query('UPDATE occupations SET occupation_is_last_level = 1 WHERE occupation_id = ?', occupation.occupation_id)

    }

  }
  console.timeEnd('updateLastLevelOccupations -- end')
}

const skills_dict = {};
const relations_dict = {};

const skills_tree = [];

const competences_dict = {};
const competences_tree = [];

const concepts_dict = {};

const hierearchy_dict = {};
const hierearchy_dict_uri = {};
const hierearchy_tree = [];
const relation_dict_uri = {};
const level_3 = []

const updateSkills = async () => {

  try {
    let courses = await db.query('SELECT esco_version FROM skills limit 1');
  }catch (e){
    if(e.sqlMessage === "Unknown column 'esco_version' in 'field list'"){
      try{
        await db.query('ALTER TABLE skills ADD COLUMN esco_version varchar(255) DEFAULT NULL');
        await db.query('ALTER TABLE skills DROP INDEX skill_href');
        await db.query('ALTER TABLE skills DROP INDEX skill_uri');
        console.log("Añadida columna esco_version")
      }catch (e){
        console.error("Error intentando alterar la tabla ",e)
        process.exit(-2);
      }
    }else{
      console.error("Error",e)
      process.exit(-1);
    }
  }

  //await indexFileConcepts("./conceptSchemes_en.csv");
  //await indexFileSkills("./ISCOGroups_en.csv");
  console.time('skills_csv')
  console.log('Loading skills csv files')
  try {
    await indexFileSkills(ESCO_FOLDER + "/greenSkillsCollection_en.csv");
    await indexFileSkills(ESCO_FOLDER + "/occupations_en.csv");
    await indexFileSkills(ESCO_FOLDER + "/skills_en.csv");
    await indexFileSkills(ESCO_FOLDER + "/digCompSkillsCollection_en.csv");
    await indexFileSkills(ESCO_FOLDER + "/languageSkillsCollection_en.csv");
    await indexFileSkills(ESCO_FOLDER + "/researchSkillsCollection_en.csv");
    // await indexFileSkill(ESCO_FOLDER+"/skillsHierarchy_en.csv");
    await indexFileSkills(ESCO_FOLDER + "/transversalSkillsCollection_en.csv");
    await indexFileSkills(ESCO_FOLDER + "/digitalSkillsCollection_en.csv");
    await indexFileSkills(ESCO_FOLDER + "/skillGroups_en.csv");
    //await relations("./skillSkillRelations_en.csv");
    //await relations("./broaderRelationsOccPillar_en.csv");
    await relations(ESCO_FOLDER + "/broaderRelationsSkillPillar_en.csv");
    console.timeEnd('skills_csv')

    console.time('processing_hierarchy')
    console.log('> processing_hierarchy')
    await getParentSkills()
    console.timeEnd('processing_hierarchy')

    console.log('Adding low level skills')
    console.time('low_level_skills')
    await getChildSkills()
    console.timeEnd('low_level_skills')
  }catch (e){
    console.error("ERROR LEYENDO CSVs")
    process.exit(-3);
  }

  // console.log('Exporting tree.json')
  // fs.writeFile('esco_tree.json', JSON.stringify(hierearchy_tree), (err) => {
  //   if (err) {
  //     console.error('There was an error writing the file:', err);
  //   } else {
  //     console.log('File has been written successfully');
  //   }
  // });

  console.log('Start db_operations')
  const transaction = await db.getConnection();
  await transaction.query('START TRANSACTION');
  console.time('db_skills_update')
  try{
    console.error("Actualizando skills db")
    await db_sync(hierearchy_tree,null);
    console.error("Actualización de skills exitosa , haciendo commit")
    await transaction.query('COMMIT');
    await transaction.release()
  }catch (e){
    console.error("Error al realizar la actualización , haciendo rollback")
    await transaction.query('ROLLBACK');
    await transaction.release()
    console.log("ERROR")
    process.exit(-4);
  }finally {

  }

  console.timeEnd('db_skills_update')
  console.log("ALL OK --> END")
  process.exit(0,);
  // process.exit(0);
  //await transaction.query('UPDATE jobs set name = "TEST 1" WHERE job_id = 50');
//  await transaction.query('COMMIT');
  //await transaction.query('UPDATE jobs set name = "TEST 2" WHERE job_id = 50');



  //
  //
  // await db.query('ROLLBACK');

  // console.time('dbInsertion')
  // console.log('> Inserting skills in the db')
  // let insertions = 0
  // console.log(skills[0])
  // for (const skill of skills) {
  //   try {
  //     // Get skill type id
  //     let skillTypeId
  //     if (skill.type === 'skill') {
  //       skillTypeId = 1
  //     } else if (skill.type === 'competence') {
  //       skillTypeId = 2
  //     }
  //
  //     // Search and insert/update skill parent. If it is a main skill, it wont have any parent
  //     let skillParent = null
  //     if (skill.parent_uri) {
  //       const skillParentSearch = await db.query('SELECT skill_id FROM skills WHERE skill_uri = ? LIMIT 1', skill.parent_uri)
  //       if (skillParentSearch.length > 0) {
  //         skillParent = skillParentSearch[0].skill_id
  //       }
  //     }
  //
  //     // Capitalice skill name
  //     const skillName = utils.capitalize(skill.name)
  //
  //     // Insert/update skill
  //     await db.query('INSERT INTO skills (skill_name, skill_description, skill_uri, skill_href, skill_type_id, skill_parent, skill_level, skill_code) VALUES(?,?,?,?,?,?,?,?) ' +
  //       'ON DUPLICATE KEY UPDATE skill_name = ?, skill_description = ?, skill_uri = ?, skill_href = ?, skill_type_id = ?, skill_parent = ?, skill_level = ?, skill_code = ?',
  //     [skillName, skill.description, skill.uri, skill.href, skillTypeId, skillParent, skill.level, skill.code, skillName, skill.description, skill.uri, skill.href, skillTypeId, skillParent, skill.level, skill.code])
  //
  //     insertions++
  //   } catch (error) {
  //     console.log('< ' + error)
  //     console.log(skill)
  //   }
  // }
  // console.log('Insertions', insertions)
  console.timeEnd('dbInsertion')
}

const db_sync = async (skills,parent) => {


  if(parent && !parent.db_id){
    // buscamos parent en bbdd
    const parentName = utils.capitalize(parent.title)
    const skillParentSearch = parent.code ? await db.query('SELECT skill_id FROM skills WHERE skill_code = ? LIMIT 1', parent.code)
        :await db.query('SELECT skill_id FROM skills WHERE skill_name = ? LIMIT 1', parentName)
    if (skillParentSearch.length > 0) {
      parent.db_id = skillParentSearch[0].skill_id
    }else{
      parent.db_id  = -1
    }
    //console.log("parent",parent.db_id,parent)
  }
  if(Array.isArray(skills)){
    for (const skill of skills){
      // chek myself
      // console.log("check level: "+skill.level+ " code: "+(skill.code?skill.code:"")+ " parent: "+(parent && parent.db_id ? parent.db_id : " -- ") +" - "+ (skill.parent_code?skill.parent_code:"")+" - "+skill.title+" uri "+skill.uri);
      const skillName = utils.capitalize(skill.title)
      const href = "https://ec.europa.eu/esco/api/resource/skill?uri="+skill.uri+"&language=en&selectedVersion=v1.1.2"

      const skillParentSearch = skill.code ? await db.query('SELECT skill_id FROM skills WHERE skill_code = ? LIMIT 1', skill.code)
                : await db.query('SELECT skill_id FROM skills WHERE skill_name = ? LIMIT 1', skillName)

      if(skillParentSearch.length > 0){
        // update
        await db.query("UPDATE skills set skill_name = ?, skill_description = ?, skill_uri = ?, skill_href = ?, skill_type_id = ?, skill_parent = ?, skill_level = ?, skill_code = ?,esco_version = ? WHERE skill_id = ?",
            [skillName, skill.desc, skill.uri, href, 1, (parent && parent.db_id > 0 ? parent.db_id : null ), skill.level, skill.code,ESCO_VERSION,skillParentSearch[0].skill_id]);

      }else{
        // insrt
        await db.query('INSERT INTO skills (skill_name, skill_description, skill_uri, skill_href, skill_type_id, skill_parent, skill_level, skill_code,esco_version) VALUES(?,?,?,?,?,?,?,?,?) ',
            [skillName, skill.desc, skill.uri, href, 1, (parent && parent.db_id > 0 ? parent.db_id : null ), skill.level, skill.code,ESCO_VERSION]);
      }

      if(skill.children){
        await db_sync(skill.children,skill)
      }
    }
  }else{
    console.log("No es un array",skills)
  }
}


// Gets all available parent skills
const getParentSkills = async () => {
  // Load the main skills
  let file = ESCO_FOLDER+'/skillsHierarchy_en.csv';
  const parentSkills = await csv().fromFile(file)
  const skills = []
  for (const parentSkill of parentSkills) {

    // console.log("Hierarchy",parentSkill)
    // Filter stuff different than skills or competences
    if (parentSkill['Level 0 code'] === 'K' || parentSkill['Level 0 code'] === 'L'
        ){
    //if (parentSkill['Level 0 code'] !== 'T' && parentSkill['Level 0 code'] !== 'S' ){
      continue
    }
    // if (parentSkill['Level 3 code'] !== undefined ){
    //   continue
    // }

    let item = {}

    for(let i = 7;  i >= 0 ; i--){

      if(parentSkill['Level '+i+' preferred term'] !== undefined && parentSkill['Level '+i+' preferred term'] != ''){

        item.level = i;
        item.code = parentSkill['Level '+i+' code'];
        item.title = parentSkill['Level '+i+' preferred term'];
        item.uri = parentSkill['Level '+i+' URI'];
        item.desc = parentSkill.Description;
        item.parent_code = i > 0 ? parentSkill['Level '+(i-1)+' code'] : null;
        item.children = null;

        if(skills_dict[item.uri]){
          let temp = skills_dict[item.uri];
          let ch_sk = skills_dict[item.uri][ESCO_FOLDER+'/skillGroups_en.csv']
          if(ch_sk.skillType)
            item.esco_type = ch_sk.skillType
          if(ch_sk.conceptType)
            item.esco_concept_type = ch_sk.conceptType
        }

        if(concepts_dict[item.uri]){
          item.concept = concepts_dict[item.uri].preferredLabel;
        }

        //const hierearchy_dict = {};
        // const hierearchy_tree = []

        hierearchy_dict_uri[item.uri] = item;
        hierearchy_dict[item.code] = item;
        if(item.parent_code == null){
          hierearchy_tree.push(item);
        }else{
          if(hierearchy_dict[item.parent_code]){
            if(hierearchy_dict[item.parent_code].children == null){
              hierearchy_dict[item.parent_code].children = []
            }
            hierearchy_dict[item.parent_code].children.push(item);
            // if(item.level === 3){
            //   level_3.push(item)
            // }
            level_3.push(item)
          }
        }
        break
      }
    }


  }
  return skills
}

const indexFileConcepts = async (file) => {
  if (fs.existsSync(file)){
    console.log("file exist",file)
  }else{
    console.log("file NO exist",file)
  }
  try {
    const allSkills = await csv().fromFile(file)
    //console.log(file, "concept", allSkills[0])
    for (const skill of allSkills) {
      // if(JSON.stringify(skill).indexOf('http://data.europa.eu/esco/skill/418a56a2-7cfc-4b6d-8cf5-5c2add99a849')>-1){
      //   console.log("xxxxx",skill)
      // }
      if(skill.hasTopConcept){
        let keys = skill.hasTopConcept.split(',')
            .map(url => url.trim());
        for (const url of keys){
          concepts_dict[url] = skill;
        }
      }
    }
    console.log("concepts loaded")
  }catch (e){
    console.error(file,e)
  }

}

const indexFileSkills = async (file) => {
  if (fs.existsSync(file)){
    console.log("file exist",file)
  }else{
    console.log("file NO exist",file)
  }
  try {
    const allSkills = await csv().fromFile(file)
    //console.log(file, "sample", allSkills[0])
    for (const skill of allSkills) {
      // if(JSON.stringify(skill).indexOf('http://data.europa.eu/esco/skill/418a56a2-7cfc-4b6d-8cf5-5c2add99a849')>-1){
      //   console.log("xxxxx",skill)
      // }
      if(skills_dict[skill.conceptUri] === undefined)
        skills_dict[skill.conceptUri] = {}

      skills_dict[skill.conceptUri][file] = skill
    }
  }catch (e){
    console.error(file,e)
  }

}

const relations = async (file) => {
  if (fs.existsSync(file)){
    console.log("relations_dict file exist",file)
  }else{
    console.log("relations_dict file NO exist",file)
  }
  try {
    const allSkills = await csv().fromFile(file)
    //console.log(file, "relations_dict", allSkills[0])
    for (const skill of allSkills) {
      if(relations_dict[skill.conceptUri] === undefined) {
          relations_dict[skill.conceptUri] = {}
      }
      if(relations_dict[skill.conceptUri]['conceptUri'] === undefined){
        relations_dict[skill.conceptUri]['conceptUri'] = []
      }
      relations_dict[skill.conceptUri]['conceptUri'].push(skill);

      if(relations_dict[skill.broaderUri] === undefined){
        relations_dict[skill.broaderUri] = {}
      }
      if(relations_dict[skill.broaderUri]['broaderUri'] === undefined){
        relations_dict[skill.broaderUri]['broaderUri'] = []
      }
      relations_dict[skill.broaderUri]['broaderUri'].push(skill)

    }
  }catch (e){
    console.error(file,e)
  }

}

// Gets all the children skills by its parents
const getChildSkills = async (parentSkills) => {
  let diff = []
  const skills = []
  //
  // Iterate parent skills
  for (const parentSkill of level_3) {
    // Save allways all the parent skills
    // Only extract data from the level 3 skills
  //  if (parentSkill.level !== 3) continue
  //   if(parentSkill.code === "T4.4"){
  //     console.log("sss")
  //   }

    if (parentSkill.children !== null) continue
    ///console.log("csv_res",JSON.stringify(parentSkill))

    let skill = parentSkill//hierearchy_dict_uri[parentSkill.uri];

    //console.log("\nskill",JSON.stringify(skill))

    let relation = relations_dict[parentSkill.uri]
    if(relation){
      // if(relation['broaderUri'].broaderUri ===parentSkill.uri ){
      //
      // } else{
      //
      // }
    }
   // console.log("\nrelation",JSON.stringify(relation))

    if(relation && relation['broaderUri']){
      for(const child of relation['broaderUri']){
        let ch_sk = skills_dict[child.conceptUri][ESCO_FOLDER+'/skills_en.csv']
        let item = {};
        //   console.log("ch_sk",ch_sk)
        //   console.log("skill",skill)
        // console.log("parentSkill",parentSkill)

        item.level = skill.level + 1;
        //item.code = parentSkill['Level '+i+' code'];
        item.title = ch_sk.preferredLabel;
        item.uri = ch_sk.conceptUri;
        item.desc = ch_sk.description;
        item.parent_code = skill.code;
        item.children = null;
        item.esco_type = ch_sk.skillType;
        item.esco_concept_type = ch_sk.conceptType;
        //const hierearchy_dict = {};
        // const hierearchy_tree = []
        if(concepts_dict[item.uri]){
          item.concept = concepts_dict[item.uri].preferredLabel;
          if(!diff.includes(item.concept)){
            diff.push(item.concept)
          }
        }

          if(skill.children === null)
            skill.children = [];

        skill.children.push(item)

        hierearchy_dict_uri[item.uri] = item;
        if(item.level < 4){
          level_3.push(item)
        }
      }
    }

  }

 // console.log("Concepts ",diff)
 // console.log("end of getChildSkills")
  return skills
}

const updateSkillsOccupationsRelations = async () => {
  console.time('updateSkillsOccupationsRelations')
  console.log('> Updating skills-occupations relations')

  const childSkills = await db.query('SELECT skill_id, skill_href FROM skills WHERE skill_level = 4')
  for (const childSkill of childSkills) {
    // Get skill data, including the occupations relations
    let res = await fetch(childSkill.skill_href)
    res = await res.json()

    // We are interested just in these arrays
    const optionals = res._links.isOptionalForOccupation
    const essentials = res._links.isEssentialForOccupation

    // Update skills_occupations_optional_relations table
    if (optionals && optionals.length > 0) {
      for (const optional of optionals) {
        const ocupationId = await db.query('SELECT occupation_id FROM occupations WHERE occupation_href LIKE "%' + optional.href + '%"')
        if (ocupationId.length > 0) {
          console.log('Optional ok')
          await db.query('INSERT IGNORE INTO skills_occupations_optional_relations (skill_id, occupation_id) VALUES(?,?)', [childSkill.skill_id, ocupationId[0].occupation_id])
        } else {
          console.log('Optional occupation not found:', optional.href)
        }
      }
    }
    // Update skills_occupations_essential_relations table
    if (essentials && essentials.length > 0) {
      for (const essential of essentials) {
        const ocupationId = await db.query('SELECT occupation_id FROM occupations WHERE occupation_href LIKE "%' + essential.href + '%"')
        if (ocupationId.length > 0) {
          console.log('Essential ok')
          await db.query('INSERT IGNORE INTO skills_occupations_essential_relations (skill_id, occupation_id) VALUES(?,?)', [childSkill.skill_id, ocupationId[0].occupation_id])
        } else {
          console.log('Essential occupation not found:', essential.href)
        }
      }
    }
  }
  console.timeEnd('updateSkillsOccupationsRelations')
}

updateSkills()


// updateOccupations()
// updateSkillsOccupationsRelations()
// updateLastLevelOccupations()
//process.exit()